<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all products images
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 */
/* Template Name: Casio TypeForm Template */ 

get_header(); ?>
<style>*{margin:0;padding:0;} html,body,#wrapper{width:100%;height:500px;} iframe{border-radius:0 !important;}</style> </head> <body> 
<?php
    wp_reset_query(); // necessary to reset query
    while ( have_posts() ) : the_post();
        the_content();
    endwhile; // End of the loop.
?>
</body> </html>
<?php

get_footer();
