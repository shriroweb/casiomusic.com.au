<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

	//global $wpdb;
$stock_code = $product->get_sku();
$stock_quantity = get_stock_quantity_pronto($stock_code);
if ($stock_quantity >= MINIMUM_STOCK_QTY) : ?>
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>
	<div class="cartcol65 fleft">
	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

		<?php
		do_action( 'woocommerce_before_add_to_cart_quantity' );

		woocommerce_quantity_input( array(
			'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
			'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $stock_quantity, $product ),
			'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
		) );

		do_action( 'woocommerce_after_add_to_cart_quantity' );
		?>

		<button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>
	<!--where to buy-->
	
			<a href="<?php echo get_bloginfo('url');?>/store-locator">
						
						<!--<span class="button wherebuy">Where To Buy</span>-->
						<button type="button" class="wherebuy button alt" id="wherebuy">Where To Buy</button>
		
					</a>
		
	<!--where to buy-->
	
	</div>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>
	
<?php	
else:
echo '<p class="stock out-of-stock">Out of stock</p>';

//echo do_shortcode('[contact-form-7 id="15589" title="Out Of  Stock Waitlist"]');
endif; ?>
