<?php
/*This file is part of soundtechnology, ecommerce-gem child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function soundtechnology_enqueue_child_styles() {
$parent_style = 'parent-style'; 
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 
		'child-style', 
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version') );
	}
add_action( 'wp_enqueue_scripts', 'soundtechnology_enqueue_child_styles' );

/*Write here your own functions */
function widget($atts) {
    
    global $wp_widget_factory;
    
    extract(shortcode_atts(array(
        'widget_name' => FALSE
    ), $atts));
    
    $widget_name = wp_specialchars($widget_name);
    
    if (!is_a($wp_widget_factory->widgets[$widget_name], 'WP_Widget')):
        $wp_class = 'WP_Widget_'.ucwords(strtolower($class));
        
        if (!is_a($wp_widget_factory->widgets[$wp_class], 'WP_Widget')):
            return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct"),'<strong>'.$class.'</strong>').'</p>';
        else:
            $class = $wp_class;
        endif;
    endif;
    
    ob_start();
    the_widget($widget_name, $instance, array('widget_id'=>'arbitrary-instance-'.$id,
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
    
}
add_shortcode('widget','widget'); 

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
return array(
'width' => 165,
'height' => 94,
'crop' => 0,
);
} );

/*cart button hide*/

/*add_filter( 'woocommerce_loop_add_to_cart_link', 'replacing_add_to_cart_button', 10, 2 );
function replacing_add_to_cart_button( $button, $product  ) {
    $button_text = __("View product", "woocommerce");
    $button = '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';

    return $button;
}

add_filter( 'woocommerce_is_purchasable', '__return_false');

add_filter( 'wc_product_sku_enabled', 'bbloomer_remove_product_page_sku' );
 
function bbloomer_remove_product_page_sku( $enabled ) {
    if ( !is_admin() && is_product() ) {
        return false;
    }
 
    return $enabled;
}*/

/*cart button hide*/


/*hide price from products start*/

/*remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_filter( 'woocommerce_variable_sale_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_get_price_html', 'businessbloomer_remove_prices', 10, 2 );
 
function businessbloomer_remove_prices( $price, $product ) {
if ( ! is_admin() ) $price = '';
return $price;
}*/

/*hide price from products end*/

add_action( 'after_setup_theme', 'my_after_setup_theme' );
function my_after_setup_theme() {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
}

add_role( 'subscriber','subscriber');
    
	
	if(current_user_can('wpsl_store_locator_manager')) {
	remove_role( 'editor');
	remove_role( 'author');
    
	}
	
	function my_login_redirect( $url, $request, $user ){
if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
if( $user->has_cap( 'administrator') or $user->has_cap( 'author')) {
$url = admin_url();
} else {
$url = home_url('/sound_login/');
}
}
return $url;
}add_filter('login_redirect', 'my_login_redirect', 10, 3 );

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
wp_redirect( home_url() );
exit();
}

// Changing "Default Sorting" to "Recommended sorting" on shop and product settings pages
function sip_update_sorting_name( $catalog_orderby ) {
$catalog_orderby = str_replace("Default sorting", "Recommended sorting", $catalog_orderby);
return $catalog_orderby;
}
add_filter( 'woocommerce_catalog_orderby', 'sip_update_sorting_name' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'sip_update_sorting_name' );


/*search limit*/

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('product'));
        $query->set('posts_per_page',4);
    }

return $query;
}

add_filter('pre_get_posts','searchfilter');

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 5; // 3 products per row
	}
}


/*allow mime type usdz*/

function mos_filter_fix_wp_check_filetype_and_ext( $data, $file, $filename, $mimes ) {
	if ( ! empty( $data['ext'] ) && ! empty( $data['type'] ) ) {
		return $data;
	}
	$registered_file_types = ['usdz' => 'model/vnd.usdz+zip|application/octet-stream|model/x-vnd.usdz+zip'];
	$filetype = wp_check_filetype( $filename, $mimes );
	if ( ! isset( $registered_file_types[ $filetype['ext'] ] ) ) {
		return $data;
	}
	return [
		'ext' => $filetype['ext'],
		'type' => $filetype['type'],
		'proper_filename' => $data['proper_filename'],
	];
}

function mos_allow_usdz( $mime_types ) {
	if ( ! in_array( 'usdz', $mime_types ) ) { 
		$mime_types['usdz'] = 'model/vnd.usdz+zip|application/octet-stream|model/x-vnd.usdz+zip';
	}
	return $mime_types;
}
add_filter( 'wp_check_filetype_and_ext', 'mos_filter_fix_wp_check_filetype_and_ext', 10, 4 );
add_filter( 'upload_mimes', 'mos_allow_usdz' );


/*allow mime type:webp*/



add_filter( 'wp_check_filetype_and_ext', 'wpse_file_and_ext_webp', 10, 4 );
function wpse_file_and_ext_webp( $types, $file, $filename, $mimes ) {
    if ( false !== strpos( $filename, '.webp' ) ) {
        $types['ext'] = 'webp';
        $types['type'] = 'image/webp';
    }

    return $types;
}

/**
 * Adds webp filetype to allowed mimes
 * 
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * 
 * @param array $mimes Mime types keyed by the file extension regex corresponding to
 *                     those types. 'swf' and 'exe' removed from full list. 'htm|html' also
 *                     removed depending on '$user' capabilities.
 *
 * @return array
 */
add_filter( 'upload_mimes', 'wpse_mime_types_webp' );
function wpse_mime_types_webp( $mimes ) {
    $mimes['webp'] = 'image/webp';

  return $mimes;
}

/*show SKU on product single page*/

/*add_action( 'woocommerce_single_product_summary', 'dev_designs_show_sku', 5 );
function dev_designs_show_sku(){
    global $product;
    echo 'SKU: ' . $product->get_sku();
}*/
/*show SKU on product single page*/


/*online store integration code*/


/**
 * Force WooCommerce terms and conditions link to open in a new page when clicked on the checkout page
 */
function shriro_woocommerce_checkout_terms_and_conditions() {
  remove_action( 'woocommerce_checkout_terms_and_conditions', 'wc_terms_and_conditions_page_content', 30 );
}
add_action( 'wp', 'shriro_woocommerce_checkout_terms_and_conditions' );


/**
 * Disable WooCommerce Ajax Cart Fragments Everywhere
 */ 
add_action( 'wp_enqueue_scripts', 'shriro_disable_woocommerce_cart_fragments', 11 );  
function shriro_disable_woocommerce_cart_fragments() { 
	 wp_dequeue_script('wc-cart-fragments'); 
}
 
 

/* Change PayPal Icon Start*/

/*function my_new_paypal_icon() {
    return '/wp-content/uploads/2017/04/paypal-icon.png';
}

add_filter( 'woocommerce_paypal_icon', 'my_new_paypal_icon' );*/
 
 /* Change PayPal Icon End */
 
 
 /*DEFAULT SHIP TO DIFFENRENT LOCATION IS UNCHECKED*/
 add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' ); 
 /*DEFAULT SHIP TO DIFFENRENT LOCATION IS UNCHECKED*/
 
 
 /*display product variation in next line*/
 add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );
/*display product variation in next line end*/
 
 /**
 * Exclude products from a particular category on the shop page
 */
function custom_pre_get_posts_query($q) {
	$tax_query = (array) $q->get('tax_query');
	$tax_query[] = array(
		'taxonomy' => 'product_cat',
		'field' => 'slug',
		'terms' => array('discontinued'), // Don't display products in the BBQs category on the shop page.
		'operator' => 'NOT IN',
	);
	$q->set('tax_query', $tax_query);
}
 

 /*copied code from ehb for ecommerce */
 
 function action_woocommerce_new_order($order_id) {
	global $wpdb;
	if (!$order_id) {
		return;
	}
	$order = wc_get_order($order_id);
	if ($order->get_shipping_state() == 'NT' || $order->get_shipping_state() == 'NSW' || $order->get_shipping_state() == 'ACT') {
		$warehouse_code = '1NW';
	} else if ($order->get_shipping_state() == 'VIC' || $order->get_shipping_state() == 'SA' || $order->get_shipping_state() == 'TAS') {
		$warehouse_code = '1NW';
	} else if ($order->get_shipping_state() == 'WA') {
		$warehouse_code = '1WA';
	} else if ($order->get_shipping_state() == 'QLD') {
		$warehouse_code = '1NW';
	}

	$is_couponcode_exists = false;
	if ($order->get_coupon_codes()) {
		$is_couponcode_exists = true;
		$customer_code = 'CASIOEMI';
		foreach ($order->get_used_coupons() as $coupon_code) {
			$coupon_post_obj = get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
			$coupon_id = $coupon_post_obj->ID;
			// Get an instance of WC_Coupon object in an array(necessary to use WC_Coupon methods)
			$coupon = new WC_Coupon($coupon_id);
			/*echo "<pre>";
			$discount_type = 'fixed_cart'; // Type: fixed_cart, percent, fixed_product, percent_product
			print_r($coupon->get_discount_type());
			echo "</pre>";
			exit;*/
			$discount_type = $coupon->get_discount_type();
			$coupon_amount = $coupon->get_amount();
			//$coupon_code = $coupon->get_code();
			$coupon_code = '';
			 ## Filtering with your coupon custom types
	    	if($coupon->is_type( 'fixed_product' ) && $discount_type == 'fixed_product'){
		    	$product_ids = $coupon->get_product_ids();
	        	$coupon_total_amount = $coupon->get_amount();
	        	if(count($product_ids) > 0){
	        		foreach($product_ids as $pid){
	        			$coupon_list['coup_fixed_product'][$pid] = $coupon_total_amount;
	        		}
	        	}
	   	 	}
	   	 	else if($coupon->is_type( 'fixed_cart' ) && $discount_type == 'fixed_cart'){
		    	$product_ids = $coupon->get_product_ids();
	        	$coupon_total_amount = $coupon->get_amount();
	        	if(count($product_ids) > 0){
	        		foreach($product_ids as $pid){
	        			$coupon_list['coup_fixed_cart'][$pid] = $coupon_total_amount;
	        		}
	        	}
	   	 	}
	   	 	 else if($coupon->is_type( 'percent' ) && $discount_type == 'percent'){
		    	$product_ids = $coupon->get_product_ids();
	        	$coupon_total_amount = $coupon->get_amount();
	        	if(count($product_ids) > 0){
	        	foreach($product_ids as $pid){
	        			$coupon_list['coup_percent_product'][$pid] = $coupon_total_amount;
	        		}
	        	}
	   	 	}

		}
	} else {
		$customer_code = 'CASIOEMI';
		$coupon_amount = 0;
	}

	//print_r($coupon_list['coup_percent_product']);exit;

	$customer_note = $order->get_customer_note();
	$customer_note1 = substr($customer_note, 0, 30);
	$customer_note2 = substr($customer_note, 30, 60);
	$customer_note3 = substr($customer_note, 60, 90);

	$csv = 'Customer Code,Warehouse,Customer Reference,Sales Rep Code,Not Before Date,Delivery Name,Delivery Address 1,Delivery Address 2,Delivery Suburb,Delivery State,Delivery Postcode,Delivery instructions 1,Delivery instructions 2,Delivery instructions 3,Terms Request,Additional Email Address,Web Reference,Credit Card No,Expiry Date,Customer Name,Customer Email,Billing Address 1,Billing Address 2,Billing Suburb,Billing State,Billing Postcode,Customer Phone,Flag';
	$csv .= "\n";
	$csv .= $customer_code . ",";
	$csv .= $warehouse_code . ",";
	$csv .= $order->get_order_number() . ",";
	$csv .= ",";
	$csv .= date("d-m-Y", strtotime($order->get_date_created())) . ",";
	$csv .= $order->get_shipping_first_name() . " " . $order->get_shipping_last_name() . ",";
	$csv .= $order->get_shipping_address_1() . ",";
	$csv .= $order->get_shipping_address_2() . ",";
	$csv .= $order->get_shipping_city() . ",";
	$csv .= $order->get_shipping_state() . ",";
	$csv .= $order->get_shipping_postcode() . ",";
	$csv .= $customer_note1 . ",";
	$csv .= $customer_note2 . ",";
	$csv .= $customer_note3 . ",";
	$csv .= ",";
	$csv .= ",";
	$csv .= $order->get_order_number() . ",";
	$csv .= ",";
	$csv .= ",";
	$csv .= $order->get_billing_first_name() . " " . $order->get_billing_last_name() . ",";
	$csv .= $order->get_billing_email() . ",";
	$csv .= $order->get_billing_address_1() . ",";
	$csv .= $order->get_billing_address_2() . ",";
	$csv .= $order->get_billing_city() . ",";
	$csv .= $order->get_billing_state() . ",";
	$csv .= $order->get_billing_postcode() . ",";
	$csv .= $order->get_billing_phone() . ",";
	$csv .= "D,";
	$csv .= "\n";
	$csv .= "Item Code,Order Quantity,Item Price(Zero for list),Discount Percentage,Note";
	$csv .= "\n";

	foreach ($order->get_items() as $item_id => $item_data) {
		$product = $item_data->get_product();
		$product_id = $item_data->get_product_id();
		$product_name = $product->get_sku(); // Get the product name
		$item_quantity = $item_data->get_quantity(); // Get the item quantity
		/*$item_total = $product->get_price();*/ // Get the item line total
		/*Doddy has to sync this table from Pronto*/
		/*Fetch price from pronto table wpms_quote_margin_cost if coupon code exists*/
		$item_total = 0;
		if($is_couponcode_exists){
			//$coupon_amount = 0;
  			$table_name = $wpdb->prefix."quote_margin_cost";
  			/*echo "SELECT * FROM $table_name WHERE `stock-code`='".$product_name."' 
  			AND `whse-code`='".$warehouse_code."'";exit;*/
  			$whse_res = $wpdb->get_results( "SELECT * FROM $table_name WHERE `stock-code`='".$product_name."' 
  			AND `whse-code`='".$warehouse_code."'" );
  			if($whse_res){
	  			foreach($whse_res as $row_whse){
	  				$item_total =	$row_whse->{'whse-avg-cost'};	
	  			}
  			}
  			/*If not able to find that product on quote_margin table*/
  			else{
  				/*echo "Discount type is ".$discount_type;exit;*/
  				$item_total = $product->get_price(); 	
  				/*Discount amount for a fixed product*/
  				if($discount_type == 'fixed_product' && $item_total > 0){
  					if(count($coupon_list['coup_fixed_product']) > 0){
  						if($coupon_list['coup_fixed_product'][$product_id] >= 0){
  							$item_total = $item_total - $coupon_list['coup_fixed_product'][$product_id];
  							if($item_total < 0){
  								$item_total = 0;
  							}
  						}
  					}
				}
				else if($discount_type == 'fixed_cart' && $item_total > 0){
  					if(count($coupon_list['coup_fixed_cart']) > 0){
  						if($coupon_list['coup_fixed_cart'][$product_id] >= 0){
  							$item_total = $item_total - $coupon_list['coup_fixed_cart'][$product_id];
  							if($item_total < 0){
  								$item_total = 0;
  							}
  						}
  					}
				}
				/*Discount amount for a fixed product*/

				/*Discount percentage applied for each item*/
				else if($discount_type == 'percent' && $item_total > 0){
					//$coupon_percent = (float)$coupon->amount/100;
  					//$item_total = (float)$item_total * (1-$coupon_percent) ;
  					if(count($coupon_list['coup_percent_product']) > 0){
  						/*print_r($coupon_list['coup_percent_product']);*/
  						if($coupon_list['coup_percent_product'][$product_id] >= 0){
  							/*echo $coupon_list['coup_percent_product'][$product_id];*/
  							$coupon_percent = (float)$coupon_list['coup_percent_product'][$product_id]/100;
  							/*echo "<br/>Coupon Percentage is ".$coupon_percent;*/
  							$item_total = (float)$item_total * (1-$coupon_percent) ;
  							if($item_total < 0){
  								$item_total = 0;
  							}
  						}
  						/*echo "<br/>Item total is ".$item_total;exit;*/
  					}
  					else{
  						$coupon_percent = (float)$coupon->amount/100;
  						$item_total = (float)$item_total * (1-$coupon_percent) ;
  					}
				}

				if($item_total >= 0){
					//Calculation to find price exclude GST.Divide by 1.1
					$item_total = (float)$item_total/1.1;
					$item_total = number_format($item_total,2);
				}
  			}
  		}
  		else{
  			$item_total = $product->get_price(); 
  			if($item_total >= 0){
				//Calculation to find price exclude GST.Divide by 1.1
				$item_total = (float)$item_total/1.1;
				$item_total = number_format($item_total,2);
			}
  		}
  		//No need to add to discount percentage
  		$coupon_amount = 0;

		$csv .= $product_name . ",";
		$csv .= $item_quantity . ",";
		$csv .= $item_total . ",";
		$csv .= $coupon_amount . ",";
		$csv .= $coupon_code . ",";
		$csv .= "\n";
	}
	$csv .= "CHARGE,";
	$csv .= "1,";
	$csv .= $order->get_shipping_total() . ",";
	$csv .= ",";
	$csv .= ",";
	$csv .= "\n";
	/*$csv_handler = fopen("ftp://wordpress:shriro@1@52.63.91.214/orders/product_order_" . $order_id . ".csv", 'w');
	fwrite($csv_handler, $csv);
	fclose($csv_handler);*/

/*$local_file_path = get_stylesheet_directory()."/uploads/order_csv/product_order_" . $order_id . ".csv";*/
$wp_upload_dir =  wp_upload_dir();
$local_file_path= $wp_upload_dir['basedir']."/order_csv/product_order_" . $order_id . ".csv";
$csv_handler = fopen($local_file_path, 'w');
fwrite($csv_handler, $csv);
fclose($csv_handler);
/*AWS server login credentials-FTP server Shriro for Shopify*/
$ftp_server = "52.63.91.214";     
$ftp_user_name = "casiomusic_au";     
$ftp_user_pass = "casio123";     
// Connect to FTP Server
$conn_id = ftp_connect($ftp_server);
if($conn_id){
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	$local_filename = "product_order_" . $order_id . ".csv";
	$remote_file_path = 'orders/'.$local_filename;
	$remote_server_filelist = ftp_nlist($conn_id, 'orders/');
	/*Uploads to remote server if the same order file not exists on remote server*/
	if(!in_array($remote_file_path, $remote_server_filelist)){ 
			ftp_put($conn_id, $remote_file_path, $local_file_path, FTP_ASCII);
	}
	ftp_close($conn_id);
}
}

// add the action
add_action('woocommerce_order_status_processing', 'action_woocommerce_new_order', 10, 3);

add_filter('woocommerce_order_number', 'change_woocommerce_order_number');

function change_woocommerce_order_number($order_id) {
	$prefix = 'CASIOEMITest';
	$new_order_id = $prefix . $order_id;
	return $new_order_id;
}
 
 
 
 /*copy ehb stock avalibility module ,07/08/2020*/
 
 /*Function to get stock quantity based on state and stock code*/
function get_the_user_ip() {
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
	//check ip from share internet
	$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
	//to check ip is pass from proxy
	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	$ip = $_SERVER['REMOTE_ADDR'];
	}
	$ip = apply_filters( 'wpb_get_ip', $ip );
	//$ip ='210.84.52.95';
	return get_state_from_ip($ip);

}
function get_state_from_ip($ip=''){
	$location = file_get_contents('http://ip-api.com/json/'.$ip.'?fields=country,countryCode,region');
	$loc = json_decode($location,true);
	//echo $loc['country'] ;
	//print_r($loc);
	if($loc['country'] == 'Australia'){
	 $state = $loc['region'];
	 return $state;
	}
	else{
	  return;
	}
}
$ip_user_state = get_the_user_ip();
/*Function to get stock quantity based on state and stock code*/
 
 
 /* Display products for Shop Page*/
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
function woocommerce_template_loop_add_to_cart() {
 global $product;
 global $ip_user_state;
	$product_type = $product->get_type();
	if($product_type == 'external'){
	echo '<a href="'. home_url() .'/stockist" rel="nofollow" class="button">View Stockists</a>';
		return;
	}
	if($product_type == 'simple'){
		$stock_code = $product->get_sku();
		$stock_quantity_pronto = get_stock_quantity_pronto($stock_code,$ip_user_state);
		if($stock_quantity_pronto < MINIMUM_STOCK_QTY || empty($stock_quantity_pronto)){
			echo '<a href="'.get_permalink().'" rel="nofollow" class="button outstock_button">Out of Stock</a>';
		}
		else
		{
			woocommerce_get_template('loop/add-to-cart.php');
		}
	}
	else if($product_type == 'variable'){
		$variations = $product->get_available_variations();
		$stock_var = array();
		$is_variant_instock = false;
		if(count($variations) > 0){
			foreach($variations as $variation){
				$stock_code =	 $variation['sku']; 
				//$ip_user_state = 'VIC';
				$stock_quantity_pronto = get_stock_quantity_pronto($stock_code,$ip_user_state);
				if($stock_quantity_pronto >= MINIMUM_STOCK_QTY){
					/*echo "Stock Quantity is ".$stock_quantity_pronto;*/
					$is_variant_instock = true;
					break;	
				}
			}
			if(!$is_variant_instock){
				echo '<a href="'.get_permalink().'" rel="nofollow" class="button outstock_button">Select Option</a>';
			}
			else
			{
				woocommerce_get_template('loop/add-to-cart.php');
				
			}
		}
	}

}
}

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
	global $product;    
	$product_type = $product->product_type;  
	switch ($product_type) {
	case 'simple':
	        return __( 'Add To Cart', 'woocommerce' );
	    break;
	case 'variable':
	        return __( 'Select Option', 'woocommerce' );
	    break;
	case 'external':
	        return __( 'Read More', 'woocommerce' );
	    break;
	}
}

function get_variant_stock_check(){
	global $ip_user_state;
	//echo $variation_id = $_POST['variation_id'];
	//$product = new WC_Product($variation_id);
	//echo $stock_code = $product->get_sku();
	$stock_code = $_POST['stock_code'];
    //$ip_user_state = 'SA';
	$stock_quantity_pronto = get_stock_quantity_pronto($stock_code,$ip_user_state);
	//echo "Stock Quantity is ".$stock_quantity_pronto;
	if(!empty($stock_quantity_pronto)){
		echo '1';
	}
	else{
		echo '0';
	}
	exit;
}
add_action( 'wp_ajax_variant_stock_check', 'get_variant_stock_check');
add_action( 'wp_ajax_nopriv_variant_stock_check', 'get_variant_stock_check');

 function get_stock_quantity_pronto($stock_code ='',$state=''){
	global $wpdb;
	/*echo "SELECT `stock-quantity` as stock_quantity FROM `wpms_stock_availability` WHERE `stock-code` = '".$stock_code."' AND `state` = '".$state."'";*/
	$query = $wpdb->get_results("SELECT `stock-quantity` as stock_quantity FROM `sound_stock_availability` WHERE `stock-code` = '".$stock_code."' AND `state` = '".$state."'");
	/*Number of stock quantity based on state*/
	$stock_quantity = $query[0]->stock_quantity;
	$stock_quantity = 	$stock_quantity > 0 ? 	$stock_quantity : 0;
	return $stock_quantity;
}

