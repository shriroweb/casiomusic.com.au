<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all Stock Availability
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 
 * Template Name: Stock Availability

 */

get_header(); ?>
<style>
#line hr:not(.is-style-wide):not(.is-style-dots)::before {

    content: '';
    display: block;
    height: 1px;
    width: 100%;
    background: 

    #767676;
    margin-top: .5rem;
    margin-bottom: .5rem;

}
	
#product_images_dealer button
{
border: none;	
padding:0 2px;	
margin:2px;
}
#product_images_dealer .fa-download{
	
color:#c7b198;
	}
	.product_images_dealer {margin-top:30px; margin-bottom:30px;}
	#product_images_dealer{
	width: 17%;

float: left;

margin: 15px 15px;
	}
	#line hr{width:100%;}
	
.popupimg{max-width: 75%!important;
    margin: 0px auto;
    display: block;
    box-shadow: 0px 0px 40px #ccc;}
	
	:focus {
    outline: -webkit-focus-ring-color none 1px !important;
}
.modal-header button {
    border: none;
    padding: 4px 8px!important;
    margin: 2px;
    float: right!important;
}
.w30{width: 30%;
    float: left;}
	
/* Style the buttons */
.btn1 {
  border: none;
  outline: none;

}

/* Style the active class, and buttons on mouse-over */
.active1, .btn1:hover {

  color:#000;
  font-weight:bold;
}
	</style>
	<div id="primary" class="content-area product_images_dealer w3eden">
		<main id="main" class="site-main" role="main">
<h3>Stock</h3>
<?php
$wpdb_b = new wpdb( "ebiz", "jMZmGE#aMwFFv2cc8", "ebiz", "mysql.shriro.com.au" );
//print_r($wpdb_b);
//$stock_result=$wpdb_b->get_results( "SELECT `stock-code` as code, `stk-avail` as qty FROM `stock_avail`" );
$cat=16;
$uri_len = strlen($cat);
$categories=$wpdb_b->get_results( "SELECT `sys-tbl-alpha-1` AS sta1,`sys-tbl-code` AS stc, `sys-description` AS sd, count(*) AS products FROM `stockgrp` 
		JOIN `stockmast` ON `stockgrp`.`sys-tbl-code` =  LEFT(`stockmast`.`stock-group`,".($uri_len+1).")
		WHERE `stock-group` LIKE '".$cat."%' AND LENGTH(LEFT(`stockmast`.`stock-group`,".($uri_len+1).")) = ".($uri_len+1)."
		AND (`stk-condition-code` = '' OR `stk-condition-code` = 'A') AND `stk-stock-status` IN ('S','K') AND `stk-abc-code` IN ('D','N','X')
		GROUP BY `sys-tbl-code`, `sys-description`" );


//echo "<br> categories<br>";



//echo "<br> tree<br>";
		$where = "";
		for ($i=0;$i<$uri_len;$i++)
		{
			$where .= " OR `sys-tbl-code` = LEFT('$cat',".($uri_len-$i).") ";
		}
		
		
$tree = $wpdb->get_results( "select `sys-tbl-code` AS stc, `sys-description`AS sd FROM stockgrp WHERE `sys-tbl-code` = '$cat' ".$where.";");


		?>
		
		<?php
			
			
			
			
			
			?>
		
		
		<?php $siteurl='http://soundtechnology.com.au/'; ?>
		
		<div class="widget_contents navp col-lg-6 float-left noPadding" id="category_container">
		<?php
		if ($tree->num_rows != 0):

		    foreach ($tree AS $leaf):
			if (strlen($leaf->stc) != 1):
			    if (strlen($leaf->sd) == 4):
				$tree_array[] = "<a  id='test' onclick='stockaval($leaf->stc);'>" . $leaf->sd . "</a>";
			    else :
				$tree_array[] = "<a  id='test1' onclick='stockaval($leaf->stc);' >" . $leaf->sd . "</a>";
			    endif;
			endif;
		    endforeach;



		    $tree_text = implode(" / ", $tree_array);

		    echo "<h4>";
		    echo $tree_text;
		    echo "</h4>";
		endif;
		$sum = "";

		foreach ($categories AS $category) :
		    echo '<div class="col-lg-12  float-left">';
		    if (strlen($category->stc) != 1) :
			if ($category->products > 0) :
			    $sum = " [" . $category->products . "] ";
			endif;
		    endif;
		    if ($tree->num_rows == 3):
			echo "<a  id='test2' class='btn' onclick='stockaval($category->stc);'> " . $category->sd . " " . $sum . "</a>";
			
		    else:
			if($category->stc==161)
			{
		echo "<a class='active1 btn1' id='test".$category->stc."' onclick='stockaval($category->stc);'> " . $category->sd . " " . $sum . "</a>";
			}
			else
			{
			echo "<a class='btn1' id='test".$category->stc."' onclick='stockaval($category->stc);'> " . $category->sd . " " . $sum . "</a>";
			}
		    endif;
		    echo '</div>';
		endforeach;
		?>
	    </div>
		
		
		<div class="col-lg-6 float-left">
		

	
	 <p id="result"></p>
	 </div>
		</main><!-- #main -->
	</div><!-- #primary -->
	
	<script>
// Add active class to the current button (highlight it)
var header = document.getElementById("category_container");
var btns = header.getElementsByClassName("btn1");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active1");
  current[0].className = current[0].className.replace(" active1", "");
  this.className += " active1";
  });
}
</script>
	
	
	
	<script>
	
	
			
			function stockaval($c)
			{
				jQuery(function ($) { 
				var var_data = $c;  
				
 $.ajax({
               url: 'http://soundtechnology.com.au/wp-content/themes/soundtechnology/response_test.php',
               type: 'GET',
               data: { var_PHP_data: var_data },
               success: function(data) {
			  // alert(data);
                 $('#result').html(data);
               },
               error: function(XMLHttpRequest, textStatus, errorThrown) {
                  //case error
				 // alert(error);
                }
             });
			 
				});
			}
		</script>
		<script>
			function products($c)
			{
				jQuery(function ($) { 
				var var_data = $c;  
				
            $.ajax({
               url: 'http://soundtechnology.com.au/wp-content/themes/soundtechnology/products_test.php',
               type: 'GET',
               data: { var_PHP_data: var_data },
               success: function(data) {
			  // alert(data);
                 $('#result').html(data);
               },
               error: function(XMLHttpRequest, textStatus, errorThrown) {
                  //case error
				 // alert(error);
                }
             });
			 
				});
			}
		</script>

<?php
do_action( 'ecommerce_gem_action_sidebar' );

get_footer();
