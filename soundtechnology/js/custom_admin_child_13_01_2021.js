
jQuery(document).on('click','#freegift_approve',function(){
    	//alert('Clicked');
        var confirm_status = confirm("Are you sure to approve the cash back?");
        if(confirm_status){
        var freegift_id = jQuery(this).attr("data-freegift-id");
        var cf7_formid = jQuery(this).attr("data-cf7-formid");
		
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            data: { action: "freegift_promotion",
                    cf7_formid: cf7_formid,
                    freegift_id: freegift_id,
                    approval_status: 'Approved'
            },success: function(data){
               console.log(data);
              alert(data);
               if(data == 'Approved'){
                alert('Cash back promotion was approved successfully.');
                window.location.reload();
               }
            }
        });
        return true;
        }
    });
/*jQuery(document).on('click','#freegift_reject',function(){
        //alert('Clicked');
        var confirm_status = confirm("Are you sure to reject the free product promotion?");
        if(confirm_status){
        var freegift_id = jQuery(this).attr("data-freegift-id");
        var cf7_formid = jQuery(this).attr("data-cf7-formid");
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            data: { action: "freegift_promotion",
                    cf7_formid: cf7_formid,
                    freegift_id: freegift_id,
                    approval_status: 'Rejected'
            },success: function(data){
              //console.log(data);
              //alert(data);
               if(data == 'Rejected'){
                alert('User was rejected successfully.');
                window.location.reload();
               }
            }
        });
        return true;
        }
    });*/


/*Send Coupon Code to users registered for promotions*/
jQuery(document).on('click','#send_coupon_promotion',function(){
        //alert('Clicked');
        var name  = jQuery(this).data('first-name')+' '+jQuery(this).data('last-name');
        name = name.replace(new RegExp("\\\\", "g"), "");
        var confirm_status = confirm("Are you sure to send the coupon code to the user "+name+"?");
        if(confirm_status){
        var post_id = jQuery(this).attr("data-post-id");
        var cf7_formid = jQuery(this).attr("data-cf7-formid");
        var user_rec_id = jQuery(this).attr("data-user-recid");
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            data: { action: "sendemail_coupon_promotion",
                    post_id: post_id,
                    cf7_formid: cf7_formid,
                    user_rec_id: user_rec_id,
                    approval_status:'Approved' 
            },success: function(data){
               //console.log(data);
              //alert(data);
               if(data == 'sent_email'){
                alert('Coupon email was sent successfully.');
                window.location.reload();
               }
            }
        });
        return true;
        }
    });

/*Send Coupon Code to users registered for promotions*/


/*Stock code autopopulate for Coupon promotion page*/
jQuery(document).ready(function(){
    // simple multiple select 
    // multiple select with AJAX search
    jQuery('div[data-name="selected_product_sku"] input').attr('readonly','readonly');
    jQuery('div[data-name="products_search"] select').select2({
        ajax: {
                url: ajaxurl, // AJAX URL is predefined in WordPress admin
                dataType: 'json',
                delay: 250, // delay in ms while typing when to perform a AJAX search
                type: 'POST',
                data: function (params) {
                    return {
                        q: params.term, // search query
                        action: 'product_sku_search' // AJAX action for admin-ajax.php
                    };
                },
                processResults: function( data ) {
                var options = [];
                //console.log(data);
                if ( data ) {
 
                    // data is the array of arrays, and each of them contains ID and the Label of the option
                    jQuery.each( data, function( index, text ) { // do not forget that "index" is just auto incremented value
                        //console.log(text);
                        options.push( { id: text.product_sku, text: text.product_sku  } );
                    });
 
                }
                return {
                    results: options
                };
            },
            cache: true
        },
        minimumInputLength: 3 // the minimum of symbols to input before perform a search
    });

jQuery('div[data-name="products_search"] select').on('change', function(){
    var sku =  jQuery('div[data-name="products_search"] select').val();
    jQuery('div[data-name="selected_product_sku"] input').val(sku);
});
});
/*Stock code autopopulate for Coupon promotion page*/

/*Stock code autopopulate for Free Product promotion page*/
jQuery('div[data-name="stock_code"] input').autocomplete({
  minLength: 3,
  delay:300,
  source: function( request, response ) {
   // Fetch data
   jQuery.ajax({
    url: "/wp-admin/admin-ajax.php",
    type: 'post',
    dataType: "json",
   data: {q:request.term,'action':'product_sku_search'},
    success: function( data ) {
    var results = [];
    jQuery.each(data, function(i, item) {
        var itemToAdd = {
            value: item.product_sku,
            value1: item.product_sku
        };
        results.push(item.product_sku);
    });
    return response(results);
    }
   });
  },
  select: function (event, ui) {
   // Set selection
  jQuery(this).val(ui.item.value); // display the selected text
   return false;
  }
 });
/*Stock code autopopulate for Free Product promotion page*/

/*Approved/Rejected Details in Advanced Contact Form7 DB Promotion Records*/
jQuery(document).ready(function(){
/*Rejected Details*/
 jQuery(".reject_hide_section").hide();
 jQuery(".reject_show").click(function(){
    jQuery(this).parent().find('.reject_hide_section').show();
    jQuery(this).parent().find('.reject_hide').show();
  });
  jQuery(".reject_hide").click(function(){
    jQuery(this).parent().find('.reject_hide_section').hide();
  });
/*Rejected Details*/

/*Approved Details*/
jQuery(".approved_hide_section").hide();
 jQuery(".approved_show").click(function(){
    jQuery(this).parent().find('.approved_hide_section').show();
    jQuery(this).parent().find('.approved_hide').show();
  });
  jQuery(".approved_hide").click(function(){
    jQuery(this).parent().find('.approved_hide_section').hide();
  });
/*Approved Details*/
  });
/*Approved/Rejected Details in Advanced Contact Form7 DB Promotion Records*/












