jQuery(document).ready(function(){
  jQuery('select#country_contact').val("Australia"); 
    jQuery(document).on('change','#country_contact',function(){
    	jQuery("#query_au option:first").attr('selected','selected');
   		jQuery("#query_us option:first").attr('selected','selected');
    });
    //Here we set Australia as a default country for contact form
    jQuery(document).on('change','#query_au',function(){
    	var query_option = jQuery(this).val();
    	jQuery('#query_option').val(query_option);
    	//jQuery('#date_purchase_au').val(' ');
    });
    jQuery(document).on('change','#query_us',function(){
    	var query_option = jQuery(this).val();
    	jQuery('#query_option').val(query_option);
    	//jQuery('#date_purchase_us').val(' ');
    });
     jQuery(document).on('change','#model_contact',function(){
    	var model_option = jQuery(this).val();
    	//console.log(model_option);
    	if(model_option == "FULL RANGE"){
    		jQuery("#query_au option[value='Technical / Troubleshooting']").prop("disabled",true);
    		jQuery("#query_us option[value='Technical / Troubleshooting']").prop("disabled",true);
    		jQuery("#query_au option:first").attr('selected','selected');
    		jQuery("#query_us option:first").attr('selected','selected');
		}
    	else{
    	  jQuery("#query_au option[value='Technical / Troubleshooting']").prop("disabled",false);
    	  jQuery("#query_us option[value='Technical / Troubleshooting']").prop("disabled",false);
    	}
    });
});
 /*(21) in serial numbers are replaced using this code*/
 jQuery('#serial_number').on('change', function () {
    var serial_no = jQuery(this).val().replace(/\(.*?\)/g, '');
    serial_no = serial_no.replace(/[^0-9]/g, '');
    jQuery('#serial_number').val(serial_no);
 });
  jQuery('#serial_number_au').on('change', function () {
    jQuery('#serial_number_us').val('');
    jQuery('#serial_number_maintenance_au').val('');
    jQuery('#serial_number_maintenance_us').val('');
    var serial_no = jQuery(this).val().replace(/\(.*?\)/g, '');
    serial_no = serial_no.replace(/[^0-9]/g, '');
    jQuery('#serial_number_au').val(serial_no);
 });
 jQuery('#serial_number_us').on('change', function () {
    jQuery('#serial_number_au').val('');
    jQuery('#serial_number_maintenance_au').val('');
    jQuery('#serial_number_maintenance_us').val('');
    var serial_no = jQuery(this).val().replace(/\(.*?\)/g, '');
    serial_no = serial_no.replace(/[^0-9]/g, '');
    jQuery('#serial_number_us').val(serial_no);
 });
  jQuery('#serial_number_maintenance_au').on('change', function () {
    jQuery('#serial_number_au').val('');
    jQuery('#serial_number_us').val('');
    jQuery('#serial_number_maintenance_us').val('');
    var serial_no = jQuery(this).val().replace(/\(.*?\)/g, '');
    serial_no = serial_no.replace(/[^0-9]/g, '');
    jQuery('#serial_number_maintenance_au').val(serial_no);
 });
jQuery('#serial_number_maintenance_us').on('change', function () {
    jQuery('#serial_number_au').val('');
    jQuery('#serial_number_us').val('');
    jQuery('#serial_number_maintenance_au').val('');
    var serial_no = jQuery(this).val().replace(/\(.*?\)/g, '');
    serial_no = serial_no.replace(/[^0-9]/g, '');
    jQuery('#serial_number_maintenance_us').val(serial_no);
 });

/*jQuery popup for serial number help*/
jQuery(document).on('click','.serialnumber_click',function(){
    elementorFrontend.documentsManager.documents[15236].showModal();
});
/*jQuery popup for serial number help*/

/*Popup for confirm empty cart*/
jQuery(document).ready(function(){
    jQuery(".emptycart").confirm({title: 'Clear your cart.',
        content: 'Are you sure you want to empty your cart?',
        typeAnimated: true,
        theme: 'white',
        boxWidth: '60%',
        useBootstrap: false,
        buttons: {
        tryAgain: {
        text: 'Confirm',
        btnClass: 'btn-black',
        action: function(){
         window.location = 'cart/?empty-cart';
        }
        },
        close: function () {}
        }
    });
    /*Storelocator plus address autocomplete*/
    jQuery('#addressInput').on('change',function(){
        cslmap.searchLocations();
    });
     /*Storelocator plus address autocomplete*/
});
/*Popup for confirm empty cart*/

/*Contact Form change country unselect query options*/
jQuery(document).on('change','#country_contact',function(){
    jQuery('#query_au option:selected').prop("selected", false);
    jQuery('#query_us option:selected').prop("selected", false);
});
/*Contact Form change country unselect query options*/

/*Prevent multiple submissions with Contact Form 7*/
jQuery(document).ready(function(){
jQuery(document).on('click', '.wpcf7-submit', function(e){
    console.log("Submit Clicked");
    if( jQuery(this).siblings('.ajax-loader').hasClass('is-active') ) {
    console.log("Submit in progress");
    e.preventDefault();
    return false;
    }
}); 
});
/*Prevent multiple submissions with Contact Form 7*/
jQuery(document).ready(function(){
if(window.location.pathname == "/checkout/"){
jQuery("#order_comments").attr('maxlength','55');
jQuery('#order_comments_field').after('<span id="char_limit_noted" style="font-size:0.8rem;"></span>');
jQuery('#order_comments').keyup(function () {
  max = this.getAttribute("maxlength");
  var len = jQuery(this).val().length;
   if (len >= max) {
    jQuery('#char_limit_noted').html('You have reached the '+max+' characters limit');
   } else {
    var char = max - len;
    var char_left = char + ' characters left';
    jQuery('#char_limit_noted').html(char_left);
   }
});
}
});

