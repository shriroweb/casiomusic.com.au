/*Functions for populating suburb and postcode in 
promotion form*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#suburb").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      jQuery('#suburb').val(suburb);
      jQuery('#postcode').val(postcode);
      jQuery('#state').val(state);
  },
  onLoadEvent:function(){
    var item = jQuery("#suburb").getItemData(0); 
    if(item == '-1'){
      var error_msg = 'Please enter a valid suburb.';
      var msg ='<span role="alert" class="wpcf7-not-valid-tip">'+error_msg+'</span>';
      jQuery('#suburb_error').html(msg);
      jQuery('#suburb_error').show();
    }
    else{
      jQuery('#suburb_error').hide();
      jQuery('#postcode_error').hide();
    }
  }
}
  ,
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },

  preparePostData: function(data) {
    data.search = jQuery("#suburb").val();
    return data;
  },

  requestDelay: 200
};
jQuery("#suburb").easyAutocomplete(options);
});

jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#postcode").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      jQuery('#suburb').val(suburb);
      jQuery('#postcode').val(postcode);
      jQuery('#state').val(state);
  },
  onLoadEvent:function(){
    var item = jQuery("#postcode").getItemData(0); 
    if(item == '-1'){
      var error_msg = 'Please enter a valid postcode.';
      var msg ='<span role="alert" class="wpcf7-not-valid-tip">'+error_msg+'</span>';
      jQuery('#postcode_error').html(msg);
      jQuery('#postcode_error').show();
    }
    else{
      jQuery('#postcode_error').hide();
      jQuery('#suburb_error').hide();
    }
  }
}
  ,
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },

  preparePostData: function(data) {
    data.search = jQuery("#postcode").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#postcode").easyAutocomplete(options);

});
/*Functions for populating suburb and postcode in 
promotion form*/

/*Functions for populating suburb and postcode in woocommerce checkout page*/
/*Billing City Autopopulate*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#billing_city").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      jQuery('#billing_city').val(suburb);
      jQuery('#billing_postcode').val(postcode);
      jQuery("#billing_state").children("option[value='"+state+"']").prop('selected',true);
  }
}
,
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },

  preparePostData: function(data) {
    data.search = jQuery("#billing_city").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#billing_city").easyAutocomplete(options);

});
/*Billing City Autopopulate*/

/*Billing Postcode Autopopulate*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#billing_postcode").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      jQuery('#billing_city').val(suburb);
      jQuery('#billing_postcode').val(postcode);
      jQuery("#billing_state").children("option[value='"+state+"']").prop('selected',true);
  }
}
,
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },

  preparePostData: function(data) {
    data.search = jQuery("#billing_postcode").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#billing_postcode").easyAutocomplete(options);

});
/*Billing Postcode Autopopulate*/


/*Shipping Suburb Autopopulate*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#shipping_city").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      jQuery('#shipping_city').val(suburb);
      jQuery('#shipping_postcode').val(postcode);
      jQuery("#shipping_state").children("option[value='"+state+"']").prop('selected',true);

  }
},
  ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
  preparePostData: function(data) {
    data.search = jQuery("#shipping_city").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#shipping_city").easyAutocomplete(options);

});
/*Shipping Suburb Autopopulate*/

/*Shipping Postcode Autopopulate*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.postcode + ' , ' + element.location + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#shipping_postcode").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      jQuery('#shipping_city').val(suburb);
      jQuery('#shipping_postcode').val(postcode);
      jQuery("#shipping_state").children("option[value='"+state+"']").prop('selected',true);
    }
  },
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
preparePostData: function(data) {
    data.search = jQuery("#shipping_postcode").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#shipping_postcode").easyAutocomplete(options);

});
/*Shipping Postcode Autopopulate*/

/*Functions for populating suburb and postcode in woocommerce checkout page*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#addressInput").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      var address = suburb + ',' + postcode + ',' + state;
      jQuery('#addressInput').val(address);
    }
  },
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
preparePostData: function(data) {
    data.search = jQuery("#addressInput").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#addressInput").easyAutocomplete(options);

});


/*Landing page zip code autopopulate address*/
jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#address_input_slpw_adv").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      var address = suburb + ',' + postcode + ',' + state;
      jQuery('#address_input_slpw_adv').val(address);
    }
  },
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
preparePostData: function(data) {
    data.search = jQuery("#address_input_slpw_adv").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#address_input_slpw_adv").easyAutocomplete(options);

});
/*Landing page zip code autopopulate address*/

/*added on 14-10-2020*/

/*Australian post api for postcode search in product page*/
/*jQuery(document).ready(function(){
var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = jQuery("#post_code_stock").getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      var address = suburb + ',' + postcode + ',' + state;
      jQuery('#post_code_stock').val(address);
    }
  },
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
preparePostData: function(data) {
    data.search = jQuery("#post_code_stock").val();
    return data;
  },
  requestDelay: 200
};
jQuery("#post_code_stock").easyAutocomplete(options);

});*/
/*Australian post api for postcode search in product page*/
/*added on 14-10-2020*/

/*Added on 11-11-2020*/
/*Add custom value for input autocomplete to disable google chrome autofill*/
jQuery(document).ready(function(){
    if(window.location.pathname == "/checkout/"){
      jQuery('#billing_city').attr('autocomplete','billing_city');
      jQuery('#billing_postcode').attr('autocomplete','billing_postcode');
      jQuery('#shipping_city').attr('autocomplete','shipping_city');
      jQuery('#shipping_postcode').attr('autocomplete','shipping_postcode');
    }
});
/*Add custom value for input autocomplete to disable google chrome autofill*/
/*Added on 11-11-2020*/

jQuery('#exampleModal_Delivery').on('show.bs.modal', function (event) {
  var modal = jQuery(this);
  var bootbox_elem = modal.find('.modal-body input[name="delivery_post_code_search"]');
  var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = bootbox_elem.getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      var address = suburb + ' , ' + postcode + ' , ' + state;
      if(suburb != ''){
        jQuery('#suburb_found').val('yes');
      }
      bootbox_elem.val(address);
    }
  },
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
preparePostData: function(data) {
    data.search = bootbox_elem.val();
    return data;
  },
  requestDelay: 200
};
bootbox_elem.easyAutocomplete(options);
});


jQuery(document).on('show.bs.modal','#exampleModal_Delivery_Mob', function (event) {
  var modal = jQuery(this);
  var bootbox_elem = modal.find('.modal-body input[name="delivery_post_code_search_mob"]');
  //console.log("Length is "+ bootbox_elem.length)
  var options = {
  minCharNumber:3,
  url: function(phrase) {
    return "/wp-admin/admin-ajax.php";
  },

  getValue: function(element) {
    var item = element.location + ' , ' + element.postcode + ' , ' + element.state;  
    return item;
  },
  list: {
      maxNumberOfElements: 10,
      onSelectItemEvent: function() {
      var item = bootbox_elem.getSelectedItemData(); 
      var suburb   = item.location;
      var postcode = item.postcode;
      var state   = item.state;
      var address = suburb + ' , ' + postcode + ' , ' + state;
      if(suburb != ''){
        jQuery('#suburb_found_mob').val('yes');
      }
      bootbox_elem.val(address);
    }
  },
ajaxSettings: {
    dataType: "json",
    method: "POST",
    data: {
        action: "autocomplete_postcode"
    }
  },
preparePostData: function(data) {
    data.search = bootbox_elem.val();
    return data;
  },
  requestDelay: 200
};
bootbox_elem.easyAutocomplete(options);
});
/*Check whether a suburb/postcode is autopopulated or not*/
/*Validation for modal popup form for mobile*/
function validate_modal_form_mob(){
  if(jQuery('#suburb_found_mob').val() ==  'no'){
  bootbox.alert({
    title: "Message",
    message:"Please wait for suburb/postcode to autopopulate.",
    class : "confirmWidth"
  });
  return false;
}
var delivery_post_code_search = jQuery('#delivery_post_code_search_mob').val();
if (delivery_post_code_search.indexOf(",") == 0){
   return false;
}
var delivery_loc = delivery_post_code_search. split(',');
var suburb   = delivery_loc[0].trim();
var postcode = delivery_loc[1].trim();
var state    = delivery_loc[2].trim();
//alert(suburb);
var x = 0;
jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: {action: "suburb_modal_popup",suburb:suburb,postcode:postcode,state:state},
        async: false,
        success: function(data){
          //alert(data);
           if(data.trim() == '1'){
              //jQuery('#exampleModal_Delivery').modal('hide');
              x = 1;
            }
            else{
              var box = bootbox.alert({
              title: "Message",
              message: "Please enter a valid suburb details.",
              class : "confirmWidth",
              backdrop: true,
              centerVertical: true
              });
              x = 0;
            }
        }
    });
//console.log("x is "+x)
  if(x == 1){
    return true;
  }
  else{
    return false;
  }
}
/*Validation for modal popup form for desktop*/
function validate_modal_form(){
if(jQuery('#suburb_found').val() ==  'no'){
  bootbox.alert({
    title: "Message",
    message:"Please wait for suburb/postcode to autopopulate.",
    class : "confirmWidth"
  });
  return false;
}
var delivery_post_code_search = jQuery('#delivery_post_code_search').val();
if (delivery_post_code_search.indexOf(",") == 0){
   return false;
}
var delivery_loc = delivery_post_code_search. split(',');
var suburb   = delivery_loc[0].trim();
var postcode = delivery_loc[1].trim();
var state    = delivery_loc[2].trim();
//alert(suburb);
var x = 0;
jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: {action: "suburb_modal_popup",suburb:suburb,postcode:postcode,state:state},
        async: false,
        success: function(data){
          //alert(data.trim());
           if(data.trim() == '1'){
              //jQuery('#exampleModal_Delivery').modal('hide');
              x = 1;
            }
            else{
              var box = bootbox.alert({
              title: "Message",
              message: "Please enter a valid suburb details.",
              class : "confirmWidth",
              backdrop: true,
              centerVertical: true
              });
              x = 0;
            }
        }
    });
//console.log("x is "+x)
  if(x == 1){
    return true;
  }
  else{
    return false;
  }
}
/*Check whether a suburb/postcode is autopopulated or not*/

