
<?php
	/**
 * The template for displaying all pages.
 *
 * This is the template that displays all rsponse
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 
 * Template Name: promotions

 */

get_header(); ?>



<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main" style="margin:30px 0px;">

      <?php
 $args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'category_name' => 'promotions',
    'posts_per_page' => 5,
);

  $loop = new WP_Query( $args );
  if( $loop->have_posts() ):
  while( $loop->have_posts() ): $loop->the_post(); global $post;
    echo '<div class="portfolio">';
    echo '<h2>' . get_the_title() . '</h2>';
   if ( has_post_thumbnail() ) :
             echo the_post_thumbnail();
            endif;
    echo '<a style="margin:30px 0px 10px 0px;" class="more-link comment-reply-link" href="' . get_permalink() . '">View Details</a>';
    echo '</div>';
	echo '<hr style="background: #6c757d;">';
  endwhile;
 
  else:
  echo '<div id="promo"><h2>There are currently no promotions running for Casio EMI.<br> 
                            Please check back soon for more exciting promotions and opportunities.<br>
                            </h2></div>';
   endif;
?>

    </div><!-- #content -->
  </div><!-- #primary -->
		
		<?php
do_action( 'ecommerce_gem_action_sidebar' );

get_footer();
		
		?>
	    