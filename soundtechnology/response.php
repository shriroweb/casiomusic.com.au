<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all Stock Availability
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 
 * Template Name: Stock Availability

 */

get_header(); ?>
<style>
#line hr:not(.is-style-wide):not(.is-style-dots)::before {

    content: '';
    display: block;
    height: 1px;
    width: 100%;
    background: 

    #767676;
    margin-top: .5rem;
    margin-bottom: .5rem;

}
	
#product_images_dealer button
{
border: none;	
padding:0 2px;	
margin:2px;
}
#product_images_dealer .fa-download{
	
color:#c7b198;
	}
	.product_images_dealer {margin-top:30px; margin-bottom:30px;}
	#product_images_dealer{
	width: 17%;

float: left;

margin: 15px 15px;
	}
	#line hr{width:100%;}
	
.popupimg{max-width: 75%!important;
    margin: 0px auto;
    display: block;
    box-shadow: 0px 0px 40px #ccc;}
	
	:focus {
    outline: -webkit-focus-ring-color none 1px !important;
}
.modal-header button {
    border: none;
    padding: 4px 8px!important;
    margin: 2px;
    float: right!important;
}
	</style>
	<div id="primary" class="content-area product_images_dealer">
		<main id="main" class="site-main" role="main">
<h3>Stock</h3>
<?php
$wpdb_b = new wpdb( "ebiz", "jMZmGE#aMwFFv2cc8", "ebiz", "mysql.shriro.com.au" );
//print_r($wpdb_b);
//$stock_result=$wpdb_b->get_results( "SELECT `stock-code` as code, `stk-avail` as qty FROM `stock_avail`" );
$test = $_GET['var_PHP_data'];
isset($test)
$cat=$test;
$uri_len = strlen($cat);
$categories=$wpdb_b->get_results( "SELECT `sys-tbl-alpha-1` AS sta1,`sys-tbl-code` AS stc, `sys-description` AS sd, count(*) AS products FROM `stockgrp` 
		JOIN `stockmast` ON `stockgrp`.`sys-tbl-code` =  LEFT(`stockmast`.`stock-group`,".($uri_len+1).")
		WHERE `stock-group` LIKE '".$cat."%' AND LENGTH(LEFT(`stockmast`.`stock-group`,".($uri_len+1).")) = ".($uri_len+1)."
		AND (`stk-condition-code` = '' OR `stk-condition-code` = 'A') AND `stk-stock-status` IN ('S','K') AND `stk-abc-code` IN ('D','N','X')
		GROUP BY `sys-tbl-code`, `sys-description`" );


//echo "<br> categories<br>";



//echo "<br> tree<br>";
		$where = "";
		for ($i=0;$i<$uri_len;$i++)
		{
			$where .= " OR `sys-tbl-code` = LEFT('$cat',".($uri_len-$i).") ";
		}
		
		
$tree = $wpdb->get_results( "select `sys-tbl-code` AS stc, `sys-description`AS sd FROM stockgrp WHERE `sys-tbl-code` = '$cat' ".$where.";");


		?>
		
		<?php
			
			
			
			
			
			?>
		
		
		<?php $siteurl='http://soundtechnology.com.au/'; ?>
		
		<div class="widget_contents noPadding" id="category_container">
		<?php
		if ($tree->num_rows != 0):

		    foreach ($tree AS $leaf):
			if (strlen($leaf->stc) != 1):
			    if (strlen($leaf->sd) == 4):
				$tree_array[] = "<a onclcik='stockaval($leaf->stc);'>" . $leaf->sd . "</a>";
			    else :
				$tree_array[] = "<a onclcik='stockaval($leaf->stc);' >" . $leaf->sd . "</a>";
			    endif;
			endif;
		    endforeach;



		    $tree_text = implode(" / ", $tree_array);

		    echo "<h4>";
		    echo $tree_text;
		    echo "</h4>";
		endif;
		$sum = "";

		foreach ($categories AS $category) :
		    echo '<div class="col-md-6">';
		    if (strlen($category->stc) != 1) :
			if ($category->products > 0) :
			    $sum = " [" . $category->products . "] ";
			endif;
		    endif;
		    if ($tree->num_rows == 3):
			echo "<a onclick='stockaval($category->stc);'> " . $category->sd . " " . $sum . "</a>";
			
		    else:
			echo "<a onclick='stockaval($category->stc);'> " . $category->sd . " " . $sum . "</a>";
		    endif;
		    echo '</div>';
		endforeach;
		?>
	    </div>
		
		
		
		<!--<script>
			function stockaval($c)
			{
				
				var var_data = $c;  
				alert(var_data)
            $.ajax({
               url: 'http://soundtechnology.com.au/wp-content/themes/soundtechnology/response.php',
               type: 'GET',
               data: { var_PHP_data: var_data },
               success: function(data) {
                 $('#result').html(data);
               },
               error: function(XMLHttpRequest, textStatus, errorThrown) {
                  //case error
                }
             });
         
			}
		</script>-->
	
	
	
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'ecommerce_gem_action_sidebar' );

get_footer();
