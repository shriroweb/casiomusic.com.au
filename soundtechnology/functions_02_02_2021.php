<?php
ob_start();
/*This file is part of soundtechnology, ecommerce-gem child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/
/*Add scripts to child theme*/

/*added code on 11/01/2021 start*/

/*Add child theme scripts to admin*/
function custom_admin_scripts() {
 $custom_js_path = get_stylesheet_directory_uri().'/js/custom_admin_child.js?ver=1.0';
 wp_enqueue_script( 'custom_scripts_add',$custom_js_path,array(), time(), 'all');
}
add_action( 'admin_enqueue_scripts', 'custom_admin_scripts' );
/*added code on 11/01/2021 end*/



/*Code added on 05-01-2021*/
function custom_childtheme_scripts(){
  $custom_postcode_path = get_stylesheet_directory_uri().'/js/custom_autocomplete_postcode.js?ver=1.0';
  wp_enqueue_script( 'custom_scripts_add2',$custom_postcode_path,array(), time() , 'all');
  $custom_easyautocomplete = get_stylesheet_directory_uri().'/js/easyautocomplete/jquery.easy-autocomplete.min.js';
  wp_enqueue_script( 'custom_scripts_add3',$custom_easyautocomplete,array(), time() , 'all');
  /*Select2 jquery scripts and css*/
  $cdn_select2_css ='https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css';
  wp_enqueue_style( 'custom_css_add4',$cdn_select2_css,array(), '' , 'all');
  $cdn_select2_js = 'https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js';
    wp_enqueue_script( 'custom_scripts_add4',$cdn_select2_js,array(), '' , 'all');
    $custom_select2_css = get_stylesheet_directory_uri().'/css/select2_custom.css';
    wp_enqueue_style( 'custom_css_add5',$custom_select2_css,array(), time() , 'all');
     $custom_select2_js = get_stylesheet_directory_uri().'/js/custom_select2.js';
     wp_enqueue_script( 'custom_scripts_add5',$custom_select2_js,array(), time() , 'all');
	/*Select2 jquery scripts and css*/
	$custom_js1 = get_stylesheet_directory_uri().'/js/custom.js';
     wp_enqueue_script( 'custom_scripts_add6',$custom_js1,array(), time() , 'all');

    $custom_tooltip = get_stylesheet_directory_uri().'/css/tooltip.css';
    wp_enqueue_style( 'custom_tooltip',$custom_tooltip,array(), time() , 'all');
    /*Jquery confirm popup jquery and css styles*/
     $jquery_confirm_css = 'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css';
     wp_enqueue_style( 'confirm_popup_styles',$jquery_confirm_css,array(), '' , 'all');
     $jquery_confirm_popup = 'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js';
     wp_enqueue_script( 'confirm_popup',$jquery_confirm_popup,array(), '' , 'all');
      /*Jquery confirm popup jquery and css styles*/
}
add_action( 'wp_enqueue_scripts', 'custom_childtheme_scripts' );
function child_enqueue_styles() {
	wp_enqueue_style('meto-theme-theme-css1', get_stylesheet_directory_uri() . '/js/easyautocomplete/easy-autocomplete.min.css', array(), '', 'all');
}
add_action('wp_enqueue_scripts', 'child_enqueue_styles', 99);
 /*Remove CSS and/or JS for Select2 used by WooCommerce*/
 add_action( 'wp_enqueue_scripts', 'wsis_dequeue_stylesandscripts_select2',100 );
 function wsis_dequeue_stylesandscripts_select2() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'selectWoo' );
        wp_deregister_style( 'selectWoo' );
 
        wp_dequeue_script( 'selectWoo');
        wp_deregister_script('selectWoo');
    } 
} 
/*Code added on 05-01-2021*/
function soundtechnology_enqueue_child_styles() {
$parent_style = 'parent-style'; 
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 
		'child-style', 
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version') );
	}
add_action( 'wp_enqueue_scripts', 'soundtechnology_enqueue_child_styles' );

/*Write here your own functions */
function widget($atts) {
    
    global $wp_widget_factory;
    
    extract(shortcode_atts(array(
        'widget_name' => FALSE
    ), $atts));
    
    $widget_name = wp_specialchars($widget_name);
    
    if (!is_a($wp_widget_factory->widgets[$widget_name], 'WP_Widget')):
        $wp_class = 'WP_Widget_'.ucwords(strtolower($class));
        
        if (!is_a($wp_widget_factory->widgets[$wp_class], 'WP_Widget')):
            return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct"),'<strong>'.$class.'</strong>').'</p>';
        else:
            $class = $wp_class;
        endif;
    endif;
    
    ob_start();
    the_widget($widget_name, $instance, array('widget_id'=>'arbitrary-instance-'.$id,
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
    
}
add_shortcode('widget','widget'); 

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
return array(
'width' => 165,
'height' => 94,
'crop' => 0,
);
} );

/*cart button hide*/

/*add_filter( 'woocommerce_loop_add_to_cart_link', 'replacing_add_to_cart_button', 10, 2 );
function replacing_add_to_cart_button( $button, $product  ) {
    $button_text = __("View product", "woocommerce");
    $button = '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';

    return $button;
}

add_filter( 'woocommerce_is_purchasable', '__return_false');

add_filter( 'wc_product_sku_enabled', 'bbloomer_remove_product_page_sku' );
 
function bbloomer_remove_product_page_sku( $enabled ) {
    if ( !is_admin() && is_product() ) {
        return false;
    }
 
    return $enabled;
}*/

/*cart button hide*/


/*hide price from products start*/

/*remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_filter( 'woocommerce_variable_sale_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_get_price_html', 'businessbloomer_remove_prices', 10, 2 );
 
function businessbloomer_remove_prices( $price, $product ) {
if ( ! is_admin() ) $price = '';
return $price;
}*/

/*hide price from products end*/

add_action( 'after_setup_theme', 'my_after_setup_theme' );
function my_after_setup_theme() {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
}

add_role( 'subscriber','subscriber');
    
	
	if(current_user_can('wpsl_store_locator_manager')) {
	remove_role( 'editor');
	remove_role( 'author');
    
	}
	
	function my_login_redirect( $url, $request, $user ){
if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
if( $user->has_cap( 'administrator') or $user->has_cap( 'author')) {
$url = admin_url();
} else {
$url = home_url('/sound_login/');
}
}
return $url;
}add_filter('login_redirect', 'my_login_redirect', 10, 3 );

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
wp_redirect( home_url() );
exit();
}

// Changing "Default Sorting" to "Recommended sorting" on shop and product settings pages
function sip_update_sorting_name( $catalog_orderby ) {
$catalog_orderby = str_replace("Default sorting", "Recommended sorting", $catalog_orderby);
return $catalog_orderby;
}
add_filter( 'woocommerce_catalog_orderby', 'sip_update_sorting_name' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'sip_update_sorting_name' );


/*search limit*/

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('product'));
        $query->set('posts_per_page',4);
    }

return $query;
}

add_filter('pre_get_posts','searchfilter');

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 5; // 3 products per row
	}
}


/*allow mime type usdz*/

function mos_filter_fix_wp_check_filetype_and_ext( $data, $file, $filename, $mimes ) {
	if ( ! empty( $data['ext'] ) && ! empty( $data['type'] ) ) {
		return $data;
	}
	$registered_file_types = ['usdz' => 'model/vnd.usdz+zip|application/octet-stream|model/x-vnd.usdz+zip'];
	$filetype = wp_check_filetype( $filename, $mimes );
	if ( ! isset( $registered_file_types[ $filetype['ext'] ] ) ) {
		return $data;
	}
	return [
		'ext' => $filetype['ext'],
		'type' => $filetype['type'],
		'proper_filename' => $data['proper_filename'],
	];
}

function mos_allow_usdz( $mime_types ) {
	if ( ! in_array( 'usdz', $mime_types ) ) { 
		$mime_types['usdz'] = 'model/vnd.usdz+zip|application/octet-stream|model/x-vnd.usdz+zip';
	}
	return $mime_types;
}
add_filter( 'wp_check_filetype_and_ext', 'mos_filter_fix_wp_check_filetype_and_ext', 10, 4 );
add_filter( 'upload_mimes', 'mos_allow_usdz' );


/*allow mime type:webp*/



add_filter( 'wp_check_filetype_and_ext', 'wpse_file_and_ext_webp', 10, 4 );
function wpse_file_and_ext_webp( $types, $file, $filename, $mimes ) {
    if ( false !== strpos( $filename, '.webp' ) ) {
        $types['ext'] = 'webp';
        $types['type'] = 'image/webp';
    }

    return $types;
}

/**
 * Adds webp filetype to allowed mimes
 * 
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * 
 * @param array $mimes Mime types keyed by the file extension regex corresponding to
 *                     those types. 'swf' and 'exe' removed from full list. 'htm|html' also
 *                     removed depending on '$user' capabilities.
 *
 * @return array
 */
add_filter( 'upload_mimes', 'wpse_mime_types_webp' );
function wpse_mime_types_webp( $mimes ) {
    $mimes['webp'] = 'image/webp';

  return $mimes;
}

/*show SKU on product single page*/

/*add_action( 'woocommerce_single_product_summary', 'dev_designs_show_sku', 5 );
function dev_designs_show_sku(){
    global $product;
    echo 'SKU: ' . $product->get_sku();
}*/
/*show SKU on product single page*/


/*online store integration code*/


/**
 * Force WooCommerce terms and conditions link to open in a new page when clicked on the checkout page
 */
function shriro_woocommerce_checkout_terms_and_conditions() {
  remove_action( 'woocommerce_checkout_terms_and_conditions', 'wc_terms_and_conditions_page_content', 30 );
}
add_action( 'wp', 'shriro_woocommerce_checkout_terms_and_conditions' );


/**
 * Disable WooCommerce Ajax Cart Fragments Everywhere
 */ 
add_action( 'wp_enqueue_scripts', 'shriro_disable_woocommerce_cart_fragments', 11 );  
function shriro_disable_woocommerce_cart_fragments() { 
	 wp_dequeue_script('wc-cart-fragments'); 
}
 
 

/* Change PayPal Icon Start*/

function my_new_paypal_icon() {
    return '/wp-content/uploads/2020/08/paypal.png';
}

add_filter( 'woocommerce_paypal_icon', 'my_new_paypal_icon' );
 
 /* Change PayPal Icon End */
 
 
 /*DEFAULT SHIP TO DIFFENRENT LOCATION IS UNCHECKED*/
 add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' ); 
 /*DEFAULT SHIP TO DIFFENRENT LOCATION IS UNCHECKED*/
 
 
 /*display product variation in next line*/
 add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );
/*display product variation in next line end*/
 
 /**
 * Exclude products from a particular category on the shop page
 */
function custom_pre_get_posts_query($q) {
	$tax_query = (array) $q->get('tax_query');
	$tax_query[] = array(
		'taxonomy' => 'product_cat',
		'field' => 'slug',
		'terms' => array('discontinued'), // Don't display products in the BBQs category on the shop page.
		'operator' => 'NOT IN',
	);
	$q->set('tax_query', $tax_query);
}


/*switch billing & shipping address on checkout*/


/*function wc_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Billing details' :
            $translated_text = __( 'Shipping details', 'woocommerce' );
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );*/

/*switch billing & shipping address on checkout end*/
 

function update_pronto_stock_quantity($stock_code='',$stock_quantity=0,$warehouse_code=''){
global $wpdb;
$table_name = $wpdb->prefix."stock_availability";	
//echo "<br>Warehouse code is ".$warehouse_code;
/*General Warehouse*/
if($warehouse_code != ''){
	$wpdb->query("UPDATE ".$table_name."
	 SET `stock-quantity` = `stock-quantity` - ".$stock_quantity."  WHERE `stock-code` = '".$stock_code."'  AND `warehouse`='".$warehouse_code."'   AND `stock-quantity` > 0");
}
//echo $wpdb->last_query;
return;
}
/*Order csv created for processing order*/
function action_woocommerce_new_order($order_id) {
	global $wpdb;
	if (!$order_id) {
		return;
	}
	$order = wc_get_order($order_id);
	$is_couponcode_exists = false;
	/*echo "<pre>";
	print_r($order->get_customer_note());
	echo "</pre>";
	exit;*/
	$customer_code = 'CASIOEMI';
	$coupon_amount = 0;
	//Get state of purchase
	$state_of_purchase = $order->get_shipping_state();
	//print_r($coupon_list['coup_percent_product']);exit;
	$customer_note = $order->get_customer_note();
	//$customer_note  = str_replace('.', '', $customer_note);
	$customer_note1 = substr($customer_note, 0, 30);
	$customer_note2 = substr($customer_note, 30, 30);
	$customer_note3 = substr($customer_note, 60, 30);
	$customer_ref = $order->get_order_number();
	$sales_rep            =     'CASIOEMI';
	$delivery_first_name  =		$order->get_shipping_first_name();
	$delivery_last_name   =		$order->get_shipping_last_name();
	$delivery_address1    =		$order->get_shipping_address_1(); 
	$delivery_address2    =		$order->get_shipping_address_2();
	$delivery_suburb 	  =		$order->get_shipping_city();
	$delivery_state       =		$order->get_shipping_state();
	$delivery_postcode	  =		$order->get_shipping_postcode();
	$customer_phone       =     $order->get_billing_phone();
	$customer_email       =		$order->get_billing_email();
	$billing_firstname    =		$order->get_billing_first_name();
	$billing_lastname 	  =		$order->get_billing_last_name();
	$billing_address_1    =		$order->get_billing_address_1();
	$billing_address_2    =		$order->get_billing_address_2();
	$billing_suburb		  =		$order->get_billing_city();
	$billing_state        =		$order->get_billing_state();
	$billing_post         =		$order->get_billing_postcode();
	$sent_status          = 	'Pending';
	$woocommerce_order_id =     get_numerics($customer_ref);
	$insert_ordermast_array = array(
    'woocommerce_order_id'=>$woocommerce_order_id,
	'customer_code' => $customer_code,
	'customer_ref' => $customer_ref,
	'sales_rep' => $sales_rep,
	'not_before_date' => date('Y-m-d'),
	'delivery_firstname' => $delivery_first_name,
	'delivery_lastname' => $delivery_last_name,
	'delivery_address_1' => $delivery_address1,
	'delivery_address_2' => $delivery_address2,
	'delivery_suburb' => $delivery_suburb,
	'delivery_state' => $delivery_state,
	'delivery_post' => $delivery_postcode,
	'country' => '',
	'phone' => $customer_phone,
	'delivery_instruction_1' => $customer_note1,
	'delivery_instruction_2' => $customer_note2 ,
	'delivery_instruction_3' => $customer_note3,
	'additional_email' => $customer_email,
	'billing_firstname'=>$billing_firstname,
	'billing_lastname'=>$billing_lastname,
	'billing_address_1'=>$billing_address_1,
	'billing_address_2'=>$billing_address_2,
	'billing_suburb'=>$billing_suburb,
	'billing_state'=>$billing_state,
	'billing_post'=>$billing_post,
	'send_status'=>$sent_status,
	'send_timtestap'=>date('Y-m-d H:i:s'));
	$wpdb->insert('ordermast', $insert_ordermast_array);
	//echo $wpdb->last_query;exit;
	$line_inc = 1;
	$ordermast_id = $wpdb->insert_id;
	//Fetch Orderline Items
	foreach ($order->get_items() as $item_id => $item_data) {
		$product = $item_data->get_product();
		$product_id = $item_data->get_product_id();
		$product_name = $product->get_sku(); // Get the product name
		$item_quantity = $item_data->get_quantity(); // Get the item quantity
		//echo "Order meta is ".$item_data->get_meta('whse_code');
		$whse_code = wc_get_order_item_meta( $item_id, 'whse_code', true ); 
		$whse_state = wc_get_order_item_meta( $item_id, 'whse_state', true ); 
		/*echo "Warehouse code is ".$whse_code;
		echo "State is ".$whse_state ;
		exit;*/
		$product_stock_code = $product->get_sku();
		/*Deducting stock quantity from stock available table after purchase*/
		update_pronto_stock_quantity($product_stock_code,$item_quantity,$whse_code );
		//echo "I am here";exit;
		$item_total = 0;
		$item_total = $product->get_price(); 
		if($item_total >= 0){
		//Calculation to find price exclude GST.Divide by 1.1
		$item_total = (float)$item_total/1.1;
		$item_total = number_format($item_total,2);
		}
  		//No need to add to discount percentage
  		$coupon_amount = 0;
  		$item_total = str_replace(",", "", $item_total);
  		//echo "Item Total2 is ".$item_total;exit;

  		//No need to add to discount percentage
	$insert_orderline_array = array("ordermast_id"=>$ordermast_id,"line_id"=>$line_inc, "item_code"=>$product_stock_code , "qty"=>$item_quantity, "price"=>$item_total, "discount"=>$coupon_amount,"whse_code"=>$whse_code,"whse_state"=>$whse_state);
	$wpdb->insert('orderline', $insert_orderline_array);
	$line_inc++;
	}

	$shipping_charge =  $order->get_shipping_total();
	if ($shipping_charge>0) {
		$insert_shipping_line_array = 
			array('ordermast_id' => $ordermast_id,"line_id"=>$line_inc,'item_code' =>"CHARGE",'qty' => 1,'price' => $shipping_charge,'discount' => 0);
		$wpdb->insert('orderline', $insert_shipping_line_array);
	}
	if(!empty($ordermast_id)){
		split_orders_into_csv($ordermast_id);
	}
}
/*Code added on 20-11-2020*/
/*Split function for orders based on warehouse code*/
function split_orders_into_csv($ordermast_id=''){
	//$ordermast_id = 7;
	global $wpdb;
	$splitorders_customer_ref = array();
	if(empty($ordermast_id)){
		return;
	}
	$ftp_server = "52.63.91.214";     
	$ftp_user_name = "shopify";     
	$ftp_user_pass = "#Sdt3Obsd#8";   

	/*$ftp_server = "52.63.91.214";     
	$ftp_user_name = "ehb_user";     
	$ftp_user_pass = "ehb_user";  */
	$wp_upload_dir =  wp_upload_dir();
  	$charge_added = false;
    $csv_header = 'Customer Code,Warehouse,Customer Reference,Sales Rep Code,Not Before Date,Delivery Name,Delivery Address 1,Delivery Address 2,Delivery Suburb,Delivery State,Delivery Postcode,Delivery instructions 1,Delivery instructions 2,Delivery instructions 3,Terms Request,Additional Email Address,Despatch Email,Credit Card No,Expiry Date,Customer Name,Customer Email,Billing Address 1,Billing Address 2,Billing Suburb,Billing State,Billing Postcode,Bill to phone,Customer Phone';
    $csv_header .= "\n";
  	$sql = "SELECT * FROM `ordermast` WHERE id ='". $ordermast_id."'";
  	$row_order_header = $wpdb->get_results($sql);
  	//$csv_header = '';
  	$csv_data = '';
  	$wp_upload_dir =  wp_upload_dir();
	if($wpdb->num_rows > 0){
		$customer_ref	 =	$row_order_header[0]->customer_ref;
		$order_ref 		 =  '{{customer_ref}}';
		$woocommerce_order_id =	$row_order_header[0]->woocommerce_order_id; 
		$warehouse_code  = '{{warehouse_code}}';
		$customer_note1  = substr(trim($row_order_header[0]->delivery_instruction_1), 0, 30);
		$customer_note2  = substr(trim($row_order_header[0]->delivery_instruction_2), 0, 30);
		$customer_note3  = substr(trim($row_order_header[0]->delivery_instruction_3), 0, 30);
		$csv_header .= $row_order_header[0]->customer_code. ",";
		$csv_header .= $warehouse_code. ",";
		$csv_header .= $order_ref.",";
		$csv_header .= ",";
		$csv_header .= date("d-m-Y", strtotime($row_order_header[0]->not_before_date)) . ",";
		$csv_header .= $row_order_header[0]->delivery_firstname. " " .$row_order_header[0]->delivery_lastname. ",";
		$csv_header .= "\"".$row_order_header[0]->delivery_address_1. "\"".",";
		$csv_header .= "\"".$row_order_header[0]->delivery_address_2. "\"".",";
		$csv_header .= $row_order_header[0]->delivery_suburb. ",";
		$csv_header .= $row_order_header[0]->delivery_state. ",";
		$csv_header .= $row_order_header[0]->delivery_post. ",";
		$csv_header .= $customer_note1 . ",";
		$csv_header .= $customer_note2 . ",";
		$csv_header .= $customer_note3 . ",";
		$csv_header .= ",";
		$csv_header .= ",";
		$csv_header .= $row_order_header[0]->additional_email. ",";
		$csv_header .= ",";
		$csv_header .= ",";
		$csv_header .= $row_order_header[0]->billing_firstname. " ".$row_order_header[0]->billing_lastname . ",";
		$csv_header .=  $row_order_header[0]->additional_email.",";
		$csv_header .= "\"".$row_order_header[0]->billing_address_1. "\"".",";
		$csv_header .= "\"".$row_order_header[0]->billing_address_2. "\"".",";
		$csv_header .= $row_order_header[0]->billing_suburb. ",";
		$csv_header .= $row_order_header[0]->billing_state. ",";
		$csv_header .= $row_order_header[0]->billing_post. ",";
		$csv_header .= ",";
		$csv_header .= $row_order_header[0]->phone.",";
		$csv_header .= "\n";
		$csv_header .= "Item Code,Order Quantity,Item Price(Zero for list),Discount Percentage,Note";
		$csv_header .= "\n";
	}
	//echo $csv_header;
	$sql = "SELECT DISTINCT whse_code FROM `orderline` WHERE ordermast_id='".$ordermast_id."' AND whse_code != ''";
	$row_whse = $wpdb->get_results($sql);
	if($wpdb->num_rows > 0){
		$warehouse_code = array();
		foreach($row_whse as $whse_res){
			$warehouse_code[] =	$whse_res->whse_code;	
		}
		$warehouse_code = array_unique($warehouse_code);
		if(count($warehouse_code) > 0){
			$csv_line_item = '';
			$coupon_amount = 0;
			$coupon_code = '';
			$csv_header_data = $csv_header;
			//echo $csv_header_data;exit;
			$x = 1;
			foreach($warehouse_code as $whse_code){
			 $replace_csv_fields = '';
			 $replace_csv_value  = '';
			  $sql = "SELECT * FROM `orderline` WHERE ordermast_id='".$ordermast_id."' AND whse_code = '".$whse_code."' AND item_code != 'CHARGE' ORDER BY line_id ASC";
			  //echo $sql."<br/>";
			   $row_line_tems = $wpdb->get_results($sql);
			   if($wpdb->num_rows > 0){
				   	foreach($row_line_tems as $v){
					    $csv_line_item .= $v->item_code. ",";
						$csv_line_item .= $v->qty . ",";
						$csv_line_item .= $v->price . ",";
						$csv_line_item .= $coupon_amount . ",";
						$csv_line_item .= $coupon_code . ",";
						$csv_line_item .= "\n";
					}
					if($x == 1){
					/*Fetch only shipping charge for the order.Code logic to extract only one shipping charge,if there is split orders
					*/
					$sql_shipping = "SELECT price FROM `orderline` WHERE ordermast_id='".$ordermast_id."' AND item_code = 'CHARGE'";
					//echo "<br/>".$sql_shipping;
					$row_line_shipping = $wpdb->get_results($sql_shipping);
					if($wpdb->num_rows > 0){
							$csv_line_item .= "CHARGE,";
							$csv_line_item .= "1,";
							$csv_line_item .= $row_line_shipping[0]->price. ",";
							$csv_line_item .= ",";
							$csv_line_item .= ",";
							$csv_line_item .= "\n";
						}
					}
					if(count($warehouse_code) > 1){
						$customer_reference = $customer_ref."_".$x;
					}
					else{
						$customer_reference =	$customer_ref; 
					}
					$splitorders_customer_ref[] = $customer_reference; 
					$replace_csv_fields = array('{{customer_ref}}','{{warehouse_code}}');
					$replace_csv_value	= array($customer_reference,$whse_code);
					$csv_header_data = str_replace($replace_csv_fields,$replace_csv_value,$csv_header_data);
					$csv_data  = $csv_header_data.$csv_line_item;
					if(count($warehouse_code) > 1){
						$local_file_path= $wp_upload_dir['basedir']."/order_csv/casioemi_".$woocommerce_order_id."_".$ordermast_id."_split".$x.".csv";
					}
					else{
						$local_file_path= $wp_upload_dir['basedir']."/order_csv/casioemi_".$woocommerce_order_id."_".$ordermast_id.".csv";
					}
					$csv_handler = fopen($local_file_path, 'w');
					fwrite($csv_handler, $csv_data);
					fclose($csv_handler);
					//echo $csv_data;
					$csv_header_data = '';
					$csv_header_data = $csv_header;
					$csv_line_item = ''; 
					$csv_data = '';   
					// Connect to FTP Server
					$conn_id = ftp_connect($ftp_server);
					if($conn_id){
					$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
					ftp_pasv($conn_id, true);
					if(count($warehouse_code) > 1){
						$local_filename = "casioemi_".$woocommerce_order_id."_". $ordermast_id . "_split".$x.".csv";
					}
					else{
						$local_filename = "casioemi_".$woocommerce_order_id."_". $ordermast_id.".csv";
					}
					/*$remote_file_path = '/stage_wordpress/shriro_corporate/orders/'.$local_filename;
					$remote_server_filelist = ftp_nlist($conn_id, '/stage_wordpress/shriro_corporate/orders/');*/
					$remote_file_path = '/wordpress/casiomusic_au/orders/'.$local_filename;
					$remote_server_filelist = ftp_nlist($conn_id, '/wordpress/casiomusic_au/orders/');
					//print_r($remote_server_filelist);
					/*Uploads to remote server if the same order file not exists on remote server*/
					if(!in_array($remote_file_path, $remote_server_filelist)){ 
						//echo "Success";
						ftp_put($conn_id, $remote_file_path, $local_file_path, FTP_ASCII);
					}
					ftp_close($conn_id);
					}
					else{
						echo "Not connected";
					}
				$x++;
				}

			}
			if(count($splitorders_customer_ref) > 0)
			{
				$data = array('splitorders_customer_ref'=>implode(",",$splitorders_customer_ref));
				$where_condition = array('id'=>$ordermast_id);
				$wpdb->update('ordermast',$data,$where_condition);
			}

		}
	}
}
add_action( 'wp_ajax_custom_split_order_csv', 'split_orders_into_csv');
add_action( 'wp_ajax_nopriv_custom_split_order_csv', 'split_orders_into_csv');
/*Split function for orders based on warehouse code*/
add_action('woocommerce_order_status_processing', 'action_woocommerce_new_order', 10, 3);
/*Order csv created for processing order*/

add_filter('woocommerce_order_number', 'change_woocommerce_order_number');

function change_woocommerce_order_number($order_id) {
	$prefix = 'CASIOEMI';
	$new_order_id = $prefix . $order_id;
	return $new_order_id;
}
 
 
 
 /*copy ehb stock avalibility module ,07/08/2020*/
 /*Function to get stock quantity from Pronto table sound_stock_availability*/
  function get_stock_quantity_pronto($stock_code =''){
	global $wpdb;
	$query = $wpdb->get_results("SELECT SUM(`stock-quantity`) as stock_quantity FROM `sound_stock_availability` WHERE `stock-code` = '".$stock_code."'");
	/*Number of stock quantity based on state*/
	$stock_quantity = $query[0]->stock_quantity;
	$stock_quantity = 	$stock_quantity > 0 ? 	$stock_quantity : 0;
	return $stock_quantity;
}
 /*Function to get stock quantity from Pronto table sound_stock_availability*/
  function get_stock_quantity_whsecode($stock_code ='',$whse_code=''){
	global $wpdb;
	$query = $wpdb->get_results("SELECT `stock-quantity` as stock_quantity FROM `sound_stock_availability` WHERE `stock-code` = '".$stock_code."' AND warehouse='".$whse_code."'");
	/*Number of stock quantity based on state*/
	//echo  $wpdb->last_query;
	$stock_quantity = $query[0]->stock_quantity;
	$stock_quantity = 	$stock_quantity > 0 ? 	$stock_quantity : 0;
	return $stock_quantity;
}
 
 /*Function to get stock quantity based on state and stock code*/
function get_the_user_ip() {
	$ip = $_SERVER['REMOTE_ADDR'];
	//echo "IP is ".$ip;
	//$ip ='210.84.52.95';
	return get_state_from_ip($ip);

}
/*Function to retrieve location details from pro.ip-api.com*/
function custom_curl_request($ip=''){
	$url = 'https://pro.ip-api.com/json/'.$ip.'?key=UIpjDIC82UvnoXT&fields=country,countryCode,region';
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => $url,
	CURLOPT_USERAGENT => 'Simple cURL'
	));
	$response = curl_exec($curl);
	curl_close($curl);
	return $response;
}




function get_state_from_ip($ip=''){
	
	//$location = file_get_contents('https://pro.ip-api.com/json/'.$ip.'?key=UIpjDIC82UvnoXT&fields=country,countryCode,region');
	$location  = custom_curl_request($ip);
	$loc = json_decode($location,true);
	//echo $loc['country'] ;
	//print_r($loc);
	if($loc['country'] == 'Australia'){
	 $state = $loc['region'];
	 return $state;
	}
	else{
	  return;
	}
}
//$ip_user_state = get_the_user_ip();






//print_r($ip_user_state);
/*Function to get stock quantity based on state and stock code*/
 
 
 /* Display products for Shop Page*/
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
function woocommerce_template_loop_add_to_cart() {
 	global $product;
	$product_type = $product->get_type();
	//echo "<br/>Product type is ".$product_type;
	if($product_type == 'external'){
	echo '<a href="'. home_url() .'/stockist" rel="nofollow" class="button">View</a>';
		return;
	}
	if($product_type == 'simple'){
		$stock_code = $product->get_sku();
		$stock_quantity_pronto = get_stock_quantity_pronto($stock_code);
		/*echo "Stock Quantity is ".$stock_quantity_pronto;*/
		if($stock_quantity_pronto < MINIMUM_STOCK_QTY || empty($stock_quantity_pronto)){
			echo '<a href="'.get_permalink().'" rel="nofollow" class="button outstock_button">Out of Stock</a>';
		}
		else
		{
			woocommerce_get_template('loop/add-to-cart.php');
		}
	}
	else if($product_type == 'variable'){
		$variations = $product->get_available_variations();
		$stock_var = array();
		$is_variant_instock = false;
		if(count($variations) > 0){
			foreach($variations as $variation){
				$stock_code =	 $variation['sku']; 
				$stock_quantity_pronto = get_stock_quantity_pronto($stock_code);
				if($stock_quantity_pronto >= MINIMUM_STOCK_QTY){
					/*echo "Stock Quantity is ".$stock_quantity_pronto;*/
					$is_variant_instock = true;
					break;	
				}
			}
			if(!$is_variant_instock){
				echo '<a href="'.get_permalink().'" rel="nofollow" class="button outstock_button">Select Option</a>';
			}
			else
			{
				woocommerce_get_template('loop/add-to-cart.php');
				
			}
		}
	}

}
}

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
	global $product;    
	$product_type = $product->product_type;  
	switch ($product_type) {
	case 'simple':
	        return __( 'Add To Cart', 'woocommerce' );
	    break;
	case 'variable':
	        return __( 'Select Option', 'woocommerce' );
	    break;
	case 'variation':
	        return __( 'Select Option', 'woocommerce' );
	    break;
	case 'external':
	        return __( 'Read More', 'woocommerce' );
	    break;
	}
}

function get_variant_stock_check(){
	//global $ip_user_state;
	//$ip_user_state = get_the_user_ip();
	//echo $variation_id = $_POST['variation_id'];
	//$product = new WC_Product($variation_id);
	//echo $stock_code = $product->get_sku();
	$stock_code = $_POST['stock_code'];
    //$ip_user_state = 'SA';
	$stock_quantity_pronto = get_stock_quantity_pronto($stock_code);
	//echo "Stock Quantity is ".$stock_quantity_pronto;
	if(!empty($stock_quantity_pronto)){
		echo '1';
	}
	else{
		echo '0';
	}
	exit;
}
add_action( 'wp_ajax_variant_stock_check', 'get_variant_stock_check');
add_action( 'wp_ajax_nopriv_variant_stock_check', 'get_variant_stock_check');

/*Cron Jobs functions starts*/
/*Section to create custom shipment tracking*/
/*add_action( 'woocommerce_admin_order_data_after_order_details', 'custom_woocommerce_admin_order_data_after_order_details' );*/
function custom_woocommerce_admin_order_data_after_order_details( $order ){
?>
    <br class="clear" />
    <h4>Shipment Tracking</h4>
    <?php 
        /*
         * get all the meta data values we need
         */ 
      
    ?>
    <div class="edit_custom_field"> 
    <?php
        woocommerce_wp_textarea_input( array(
            'id' => 'tracking_company',
            'label' => 'Tracking Company:',
            'value' => $order->get_meta('tracking_company'),
            'wrapper_class' => 'form-field-wide'
        ) );

    ?>
    </div>
    <div class="edit_custom_field"> 
    <?php
        woocommerce_wp_textarea_input( array(
            'id' => 'tracking_link',
            'label' => 'Tracking Link:',
            'value' => $order->get_meta('tracking_link'),
            'wrapper_class' => 'form-field-wide'
        ) );

    ?>
    </div>
    <div class="edit_custom_field"> 
    <?php
        woocommerce_wp_textarea_input( array(
            'id' => 'tracking_number',
            'label' => 'Tracking Number:',
            'value' => $order->get_meta('tracking_number'),
            'wrapper_class' => 'form-field-wide'
        ) );

    ?>
    </div>
<?php
}

/**
 * Save the custom fields values
 */
add_action( 'woocommerce_process_shop_order_meta', 'custom_woocommerce_process_shop_order_meta' );
function custom_woocommerce_process_shop_order_meta( $order_id ){
    update_post_meta( $order_id, 'custom_field_name', wc_sanitize_textarea( $_POST[ 'custom_field_name' ] ) );
}
/*Section to create custom shipment tracking*/

/*Section to add Order status Dispatched*/
function custom_register_dispatched_status() {
 
	register_post_status( 'wc-custom-dispatched', array(
		'label'		=> 'Dispatched',
		'public'	=> true,
		'show_in_admin_status_list' => true, // show count All (12) , Completed (9) , Awaiting shipment (2) ...
		'label_count'	=> _n_noop( 'Dispatched <span class="count">(%s)</span>', 'Dispatched <span class="count">(%s)</span>' )
	) );
 
}
add_action( 'init', 'custom_register_dispatched_status' );
 
/*
 * Add registered status to list of WC Order statuses
 * @param array $wc_statuses_arr Array of all order statuses on the website
 */
function custom_add_status_dispatched( $wc_statuses_arr ) {
 	$new_statuses_arr = array();
 	// add new order status after processing
	foreach ( $wc_statuses_arr as $id => $label ) {
		$new_statuses_arr[ $id ] = $label;
 		if ( 'wc-completed' === $id ) { // after "Completed" status
			$new_statuses_arr['wc-custom-dispatched'] = 'Dispatched';
		}
	}
	return $new_statuses_arr;
}
add_filter( 'wc_order_statuses', 'custom_add_status_dispatched' );

/*Cron Jobs section*/
function get_numerics($str) {
    preg_match_all('/\d+/', $str, $matches);
    return trim($matches[0][0]);
}
/*cron jobs section for fetching connote csv file from ftp folder location and stored to database*/
function cronjob_fetch_connote_csvfiles(){
/*$subject = 'Test Email';
$headers = array('Content-Type: text/html; charset=UTF-8');
$to = 'robinv@shriro.com.au';
$message = 'This is a test email';
wp_mail($to,$subject,$message,$headers);*/
global $wpdb;
$ftp_server = "52.63.91.214";     
$ftp_user_name = "casiomusic_au";     
$ftp_user_pass = "casio123";  

//$ftp_server = "52.63.91.214";     
	//$ftp_user_name = "shopify";     
	//$ftp_user_pass = "#Sdt3Obsd#8"; 
     
// Connect to FTP Server
$conn_id = ftp_connect($ftp_server);
if($conn_id){
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	$list = ftp_nlist($conn_id, 'connote/');
		foreach ($list as $fkey => $fvalue) {
		//Initiate array
		$ordermast = $orderline = array();
		//file will be saved using this file name
		$wp_upload_dir   =  wp_upload_dir();
		$local_file_path = $wp_upload_dir['basedir'].'/connote/';
		$new_file_name   = $local_file_path.'file.csv';
		//download file to local disc
		ftp_get($conn_id, $new_file_name, $fvalue, FTP_ASCII);
		//After download the file delete it to avoid duplict
		//echo $fvalue;
		ftp_delete($conn_id,$fvalue);
		//starts reading file
		if (($handle = fopen($new_file_name, "r")) !== FALSE) {
			$line = 1;
			$orderlinenum = 0;
			$x = 1;
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    	if ($line == 2) {
		    		$ordermast['customer_ref'] = $data[0];
		    		$customer_ref = $data[0];
		    		$order_id = get_numerics($customer_ref);
		    		$order_id = intval($order_id);
		    		$ordermast['tracking_company'] = $data[1];
		    		$ordermast['tracking_link'] = $data[2];
		    		$ordermast['tracking_number'] = $data[3];
		    	}
		    	if ($line > 3) {
		    		$orderline[$orderlinenum]['ordermast_id'] = $order_id;
		    		$orderline[$orderlinenum]['itemcode'] = $data[0];
		    		$orderline[$orderlinenum]['qty'] = $data[1];
		    		$orderline[$orderlinenum]['line_id'] = $x;
		    		$x++;
		    		$orderlinenum++;
		    	}
		        $line++;
		    }
		}
		//print_r($ordermast);
		//exit;
		unlink($local_file_path.'file.csv');
		fclose($handle);
		if(count($ordermast) > 0 && count($orderline) > 0){
			$query = $wpdb->get_results("SELECT count(id) as cnt FROM fulfillment_call 
				WHERE ordermast_id ='".$order_id."' AND customer_ref='".$customer_ref."' AND synced=0");
    		$row_count = $query[0]->cnt;
    		//echo $wpdb->last_query;
    		if($row_count == 0){
    			if (strpos($ordermast['tracking_number'], 'E+')){
                	$ordermast['tracking_number'] = intval((float)$ordermast['tracking_number']);
       			}
				$insert_order_data = array('ordermast_id' => $order_id,
				'customer_ref' => $ordermast['customer_ref'],
				'tracking_company' => $ordermast['tracking_company'],
				'tracking_link' => $ordermast['tracking_link'],
				'tracking_number' => $ordermast['tracking_number'],
				'synced' => 0,
				'createdby' => date('Y-m-d H:i:s'));
				$wpdb->insert('fulfillment_call', $insert_order_data);
				foreach( $orderline as $k=>$orderline_items){
					$wpdb->insert('fulfillment_call_lines', $orderline_items);
				}
				unset($ordermast);
				unset($orderline);
			}/*Check row count > 0*/
		}
	} 
  }
}
add_action( 'wp_ajax_cron_fetch_connote', 'cronjob_fetch_connote_csvfiles');
add_action( 'wp_ajax_nopriv_cron_fetch_connote', 'cronjob_fetch_connote_csvfiles');


/*cron jobs section for connote csv file from ftp folder location and stored to database*/
function cronjob_update_order_shipment_tracking(){
  global $wpdb;
	$query = $wpdb->get_results("SELECT ordermast_id,tracking_company,tracking_link,tracking_number FROM fulfillment_call WHERE synced=0");
    if($query){
    	foreach($query as $k=>$v){
    		$order_details = wc_get_order($v->ordermast_id);
    		if($order_details){
    		$order = new WC_Order($v->ordermast_id);
    		if($order){
    			echo $url_link = str_replace("<CONNOTE>",$v->tracking_number,$v->tracking_link);
				$order->update_meta_data('tracking_company', $v->tracking_company);
				$order->update_meta_data('tracking_link', $url_link);
				$order->update_meta_data('tracking_number', $v->tracking_number);
				$order->save();
				echo "<br/>Status is ".$order->status;
				if ($order->status == 'processing') {
					/*$order->update_status('custom-dispatched', 'Order is dispatched'); */
					//$order->update_status('custom-dispatched'); 
				}
				unset($order);
				$data = array('synced'=>1);
				$where_condition = array('ordermast_id'=>$v->ordermast_id,'synced'=>0);
				$wpdb->update('fulfillment_call',$data,$where_condition);
			}
			}
		}
   }
}
add_action( 'wp_ajax_cron_update_order_shipment_tracking', 'cronjob_update_order_shipment_tracking');
add_action( 'wp_ajax_nopriv_cron_update_order_shipment_tracking', 'cronjob_update_order_shipment_tracking');

add_action("woocommerce_order_status_changed", "dispatched_status_custom_notification");
function dispatched_status_custom_notification($order_id, $checkout=null) {
   global $woocommerce;
   $order = new WC_Order( $order_id );
   echo $order->status;
   /*Custom Dispatched Order Status*/
   if($order->status == 'custom-dispatched' ) {
	$tracking_company = $order->get_meta('tracking_company');
	$tracking_link =  $order->get_meta('tracking_link');
	$tracking_number =  $order->get_meta('tracking_number');
      // Create a mailer
     $mailer = $woocommerce->mailer();
     $email_content  = '';
     $email_content = '<p>Tracking company is '.$tracking_company.'</p>';
     $email_content .= '<p>Tracking number is '.$tracking_number.'</p>';
     $email_content .= '<p><a href="'.$tracking_link.'" target="_blank">Tracking URL</a></p>';
      $message_body = __($email_content);
      $message = $mailer->wrap_message(
        // Message head and message body.
        sprintf( __( 'Order %s is Dispatched' ), $order->get_order_number() ), $message_body );

      // Cliente email, email subject and message.
     /*$mailer->send( $order->billing_email, sprintf( __( 'Order %s Dispatched' ), $order->get_order_number() ), $message );*/
     }   /*Custom Dispatched Order Status*/
   }

/*Cron Jobs functions ends*/
/*
Print load time*/

 /*echo get_num_queries();  
 echo 'queries in';
echo timer_stop(1); 
echo 'seconds.';*/







/*
Print load time*/

/*Code added on 05-01-2021*/
/*Cronjob to fetch casiomusic stock inventory*/
function sync_stock_inventory_casioemi_au_remote_ftp(){
global $wpdb;
$ftp_server = "52.63.91.214";     
$ftp_user_name = "shopify";     
$ftp_user_pass = "#Sdt3Obsd#8"; 
// Connect to FTP Server
$conn_id = ftp_connect($ftp_server);
if($conn_id){
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	$list = ftp_nlist($conn_id, 'wordpress/casiomusic_au/inventory/');
	//print_r($list);
		foreach ($list as $fkey => $fvalue) {
		//Initiate array
		//file will be saved using this file name
		$wp_upload_dir   =  wp_upload_dir();
		$local_file_path = $wp_upload_dir['basedir'].'/inventory_csv/';
		echo $new_file_name   = $local_file_path.'file_casioemi.csv';
		//download file to local disc
		ftp_get($conn_id, $new_file_name, $fvalue, FTP_ASCII);
		//After download the file delete it to avoid duplict
		//echo $fvalue;
		//starts reading file
		if (($handle = fopen($new_file_name, "r")) !== FALSE) {
			$line = 1;
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    	if($line == 1){
		    		$line++;
		    		continue;
		    	}
			    $stock_code 	= trim($data[0]);
			    $stock_quantity = trim($data[3]);
			    $warehouse 		= trim($data[4]);
			    //$state			= trim($data[3]);
			    // Insert the row into the database
			    //$warehouse = '1NW';
			    //$state     = 'NSW';
			    $query = $wpdb->get_results("SELECT count(`stock-code`) as cnt FROM ".$wpdb->prefix."stock_availability WHERE `stock-code` = '". $stock_code ."' AND warehouse='".  $warehouse."'");
			    //echo $wpdb->last_query;exit;
			    $row_cnt = $query[0]->cnt;
			    //echo "<br/>Row cnt is ". $row_cnt;
    			if($row_cnt == 0){
				    $wpdb->insert($wpdb->prefix."stock_availability", array(
				        "stock-code" => (string)$stock_code,
				        "stock-quantity" => $stock_quantity,
				        "warehouse" => $warehouse
				    ));
				    //echo "<br/>".$wpdb->last_query;
				}
				else{
					$data = array("stock-quantity"=>$stock_quantity);
					$where_condition = array("stock-code" => $stock_code,"warehouse"=>$warehouse);
					$wpdb->update($wpdb->prefix."stock_availability",$data,$where_condition);
				 	//echo "<br/>".$wpdb->last_query;

				}
			    $line++;
		    }
		}
		unlink($local_file_path.'file_casioemi.csv');
		fclose($handle);
	}
}
}
add_action( 'wp_ajax_casioemi_au_sync_stock_inventory', 'sync_stock_inventory_casioemi_au_remote_ftp');
add_action( 'wp_ajax_nopriv_casioemi_au_sync_stock_inventory', 'sync_stock_inventory_casioemi_au_remote_ftp');
/*Cronjob to fetch casiomusic stock inventory*/
/*Australian post stage api to validate suburb*/
function custom_validate_suburb($suburb='',$postcode='',$state=''){
	$postcode_details = array('suburb'=>$suburb,'state'=>$state,'postcode'=>$postcode);
	$query_data = http_build_query($postcode_details);
	$api_url = "https://digitalapi.auspost.com.au/shipping/v1/address?".$query_data;
	//echo $api_url;exit;
	//echo $api_url;
	$curl = curl_init();
	curl_setopt_array($curl, array(
	  CURLOPT_URL => $api_url,
	  CURLOPT_RETURNTRANSFER => true,
	  CURLOPT_ENCODING => "",
	  CURLOPT_MAXREDIRS => 10,
	  CURLOPT_TIMEOUT => 0,
	  CURLOPT_FOLLOWLOCATION => true,
	  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	  CURLOPT_CUSTOMREQUEST => "GET",
  	  CURLOPT_HTTPHEADER => array(
     "account-number: 0000126800",
     "Authorization: Basic ZGZiYzUyZTItYmQ0Mi00ZjQzLTkxNjItYWYxZTAzZDNiYWM1OngwNzhmNzg4NmFmMWM3YjQwNTg4"
 	 ),
	));
	$response = curl_exec($curl);
	curl_close($curl);
	$result = json_decode($response);
	$suburb_valid = $result->found;
	//if suburb exists,$suburb_valid will be true
	if($suburb_valid){
		return true;
	}
	else{
		return false;
	}
}

//Store the custom data to cart object
add_filter( 'woocommerce_add_cart_item_data', 'save_custom_product_data', 10, 2 );
function save_custom_product_data( $cart_item_data, $product_id ) {
    $bool = false;
    $data = array();
    $whse_code 	= sanitize_text_field($_POST['whse_code']);
    $whse_state = sanitize_text_field($_POST['whse_state']);
    if(!empty($whse_code)) {
        $cart_item_data['custom_data']['whse_code'] = $whse_code;
        $data['whse_code'] = $whse_code;
        $bool = true;
    }
    if(!empty($whse_state)) {
        $cart_item_data['custom_data']['whse_state'] = $whse_state;
        $data['whse_state'] = $whse_state;
        $bool = true;
    }
    if($bool) {
        // below statement make sure every add to cart action as unique line item
        /*$cart_item_data['custom_data']['unique_key'] = md5( microtime().rand() );*/
        WC()->session->set( 'custom_variations', $data );
    }
    return $cart_item_data;
}

// Displaying the custom attributes in cart and checkout items
/*add_filter( 'woocommerce_get_item_data', 'customizing_cart_item_data', 10, 2 );*/
function customizing_cart_item_data( $cart_data, $cart_item ) {
    $custom_items = array();
    if( ! empty( $cart_data ) ) $custom_items = $cart_data;

    // Get the data (custom attributes) and set them
    if( ! empty( $cart_item['custom_data']['whse_code'] )){
        $custom_items[] = array(
            'name'      => 'whse_code',
            'value'     => $cart_item['custom_data']['whse_code'],
        );
    }
    if( ! empty( $cart_item['custom_data']['whse_state'] )){
        $custom_items[] = array(
            'name'      => 'whse_state',
            'value'     => $cart_item['custom_data']['whse_state'],
        );
    }
    //print_r($custom_items);
    return $custom_items;
}
/*Store custom attributes to order meta hook*/
add_action('woocommerce_add_order_item_meta','adding_custom_data_in_order_items_meta', 10, 3 );
/*add_action('woocommerce_new_order_item','adding_custom_data_in_order_items_meta', 10, 3 );*/
function adding_custom_data_in_order_items_meta( $item_id, $values, $cart_item_key ) {

    // The corresponding Product Id for the item:
    $product_id = $values[ 'product_id' ];

   $custom_whse_code = $values['custom_data']['whse_code'];
   $custom_whse_state = $values['custom_data']['whse_state'];
    //echo "Warehouse code is ". $custom_whse_code;
    if ( !empty($custom_whse_code)) {
        wc_add_order_item_meta($item_id, 'whse_code', $custom_whse_code, true);
    }
	//echo "Warehouse state is ". $custom_whse_state;exit;
    if ( !empty($custom_whse_state) ) {
        wc_add_order_item_meta($item_id, 'whse_state', $custom_whse_state, true);
    }
    // And so on …
}
/*Store custom attributes to order meta hook*/
add_filter( 'woocommerce_order_item_get_formatted_meta_data', 'custom_order_item_get_formatted_meta_data', 10, 1 );
function custom_order_item_get_formatted_meta_data($formatted_meta){
	/*echo "<pre>";
	print_r($formatted_meta);
	echo "</pre>";*/
	$temp_metas = [];
	foreach($formatted_meta as $key => $meta) {
	    if ( isset( $meta->key ) && ! in_array( $meta->key, [
	            'whse_code',
	            'whse_state'
	        ] ) ) {
	        $temp_metas[ $key ] = $meta;
	    }
	}
	return $temp_metas;
}
/*Function to findout warehouse rule for CasioEMI based on state*/
function get_casioemi_warehouse_code_state($state=''){
	if ($state== 'NT' || $state== 'NSW' || $state== 'ACT') {
        $warehouse_code = '1NW';
    } else if ($state== 'SA' || $state== 'TAS') {
        $warehouse_code = '1NW';
    } else if ($state== 'WA') {
        $warehouse_code = '1WA';
    } else if ($state== 'QLD') {
        $warehouse_code = '1NW';
    }
    else if ($state== 'VIC'){
      $warehouse_code = '1VC';
    }
    return $warehouse_code;

}
function get_alternate_ware_house($stock_code='',$whse_code=''){
	$alternate_whse_code = array();
	$alternate_whse = '';
	if($whse_code == '1WA'){
		$alternate_whse_code = array('1NW','1VC');
	}
	else if($whse_code == '1VC'){
		$alternate_whse_code = array('1NW','1WA');
	}
	else if($whse_code == '1NW'){
		$alternate_whse_code = array('1VC','1WA');
	}
	//print_r($alternate_whse_code);
	foreach($alternate_whse_code as $v){
		$stock_quantity = get_stock_quantity_whsecode(trim($stock_code),$v);
		/*echo "<br/>Stock code is ".$stock_code;
		echo "<br/>Stock quantity is ".$stock_quantity;
		echo "<br/>Warehouse code  is ".$v;*/

		if($stock_quantity > 0){
		 	$alternate_whse = $v;
		 	//echo "Alternate warehouse code is ",$alternate_whse ;
		 	break;	
		}
	}
	return $alternate_whse;

}
function custom_product_warehouse_logic($stock_code='',$whse_state=''){
	$data = array();
	$whse_code = get_casioemi_warehouse_code_state($whse_state);
	$stock_quantity = get_stock_quantity_whsecode($stock_code,$whse_code);
	/*Check whether the stock quantity exists on alternate warehouse, if the stock quantity is not available on a particular warehouse*/
	if(empty($stock_quantity)){
		 $alternate_whse_code = get_alternate_ware_house($stock_code,$whse_code);
		 /*echo "<br/>Stock quantity is ".$stock_quantity;
		 echo "<br/>Warehouse code is ".$alternate_whse_code;
		 exit;*/
		  $whse_code = $alternate_whse_code;
	}
	$data['whse_code'] = $whse_code;
	$data['whse_state'] = $whse_state;
	return $data;
}
function update_cart_product_custom_attributes($cart_item_key,$cart_item_data='',$whse_code='',$whse_state = ''){
    $bool = false;
    $data = array();
    //echo "WHSE code is ".$whse_code;
    //echo "WHSE state is ".$whse_state;
    //print_r($cart_item_data);
    if(!empty($whse_code)) {
        $data['whse_code'] = $whse_code;
        $bool = true;
    }
    if(!empty($whse_state)) {
        $data['whse_state'] = $whse_state;
        $bool = true;
    }
    if($bool) {
       //WC()->session->set( 'custom_variations', $data );
    global $woocommerce;
    $woocommerce->cart->cart_contents[$cart_item_key]['custom_data'] = $data;
    $woocommerce->cart->set_session();   // when in ajax calls, saves it.

    }
}
function check_warehouse_code_exists($customer_state=''){
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$product = wc_get_product($cart_item["variation_id"] ? $cart_item["variation_id"] : $cart_item["product_id"]);
		$stock_code = trim($product->get_sku()); 
		$whse_data = custom_product_warehouse_logic($stock_code,$customer_state);
		update_cart_product_custom_attributes($cart_item_key,$cart_item,$whse_data['whse_code'],$whse_data['whse_state']);
	}
}
add_action('woocommerce_after_checkout_validation', 'deny_pobox_postcode');
function deny_pobox_postcode( $posted ) {
  global $woocommerce;

  $address  = ( isset( $posted['shipping_address_1'] ) ) ? $posted['shipping_address_1'] : $posted['billing_address_1'];
  $postcode = ( isset( $posted['shipping_postcode'] ) ) ? $posted['shipping_postcode'] : $posted['billing_postcode'];

  $replace  = array(" ", ".", ",");
  $address  = strtolower( str_replace( $replace, '', $address ) );
  $postcode = strtolower( str_replace( $replace, '', $postcode ) );

  if ( strstr( $address, 'pobox' ) || strstr( $postcode, 'pobox' ) ) {
    wc_add_notice( sprintf( __( "Unfortunately we do not ship to a PO Box, please enter an alternative delivery address.") ) ,'error' );
  }
}

/*Check out of stock validation*/
function validate_checkout_cart_contents($data,$errors){
	check_warehouse_code_exists(trim($data['shipping_state']));
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ){
		$error_msg = '';
		$whse_code = $cart_item['custom_data']['whse_code'];
		$user_state = $cart_item['custom_data']['whse_state'];
		$product = wc_get_product($cart_item["variation_id"] ? $cart_item["variation_id"] : $cart_item["product_id"]);
		$stock_code = $product->get_sku(); 
		$stock_quantity = get_stock_quantity_whsecode($stock_code,$whse_code);
		/*echo "<br/>WHSE code is ".$whse_code;
		echo "<br/>SKU is ".$stock_code;
		echo "<br/>Quantity is ".$stock_quantity;*/
		if($stock_quantity < MINIMUM_STOCK_QTY){
			$error_msg = $stock_code . ' is currently out of stock.Please remove that item from the cart.';
			$errors->add( 'validation', __( $error_msg));
		}
		/*Custom code added to find negative quantity*/
		$balance_qty = $stock_quantity - $cart_item['quantity'];
		//echo "<br/>Balance quantity is ".$balance_qty;
		if($balance_qty < 0){
			$error_msg_notstock = $stock_code . ' quantity available in stock is '.$stock_quantity;
			$errors->add( 'validation', __( $error_msg_notstock));
		}
    }
    //exit;
   /*Validate shipping suburb,postcode and state combination*/
    $billing_suburb = $data['billing_city'];
    $billing_postcode = $data['billing_postcode'];
	$billing_state = $data['billing_state'];
	if($billing_suburb != '' && $billing_postcode != '' && $billing_state != ''){
		$billing_suburb_valid = custom_validate_suburb($billing_suburb,$billing_postcode,$billing_state);
		if(!$billing_suburb_valid){
			$error_msg = 'Please enter a valid billing address.';
			$errors->add( 'validation', __( $error_msg));
			return;
		}
	}
	/*Validate shipping suburb,postcode and state combination*/
	$shipping_suburb = $data['shipping_city'];
    $shipping_postcode = $data['shipping_postcode'];
	$shipping_state = $data['shipping_state'];
	if($shipping_suburb != '' && $shipping_postcode != '' && $shipping_state != ''){
	$shipping_suburb_valid = custom_validate_suburb($shipping_suburb,$shipping_postcode,$shipping_state);
		if(!$shipping_suburb_valid){
			$error_msg = 'Please enter a valid shipping address.';
			$errors->add( 'validation', __( $error_msg));
			return;

		}
	}
/*echo "<pre>";
echo 'Cart Dump: ' . print_r( WC()->session->cart);
echo "</pre>";
exit;*/

}
/*Check out stock validation*/
add_action('woocommerce_after_checkout_validation', 'validate_checkout_cart_contents',10,2);
add_filter( 'woocommerce_checkout_fields' , 'alter_woocommerce_checkout_fields',100 );
function alter_woocommerce_checkout_fields( $fields ) {
    // Change the label name
    $fields['order']['order_comments']['label'] = __('Order notes Max : 55 Characters', "woocommerce");

    return $fields;
}
/* — WOOCOMMERCE: Add empty cart to cart page — */
// check for empty-cart get param to clear the cart
add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
	global $woocommerce;
	if ( isset( $_GET['empty-cart'] ) ) {
	$woocommerce->cart->empty_cart();
	}
}
/*Autopopulate suburb or postcode on checkout page*/
/*Australian postcode api code*/
function custom_aupost_postcode_api(){
	$search = htmlspecialchars($_POST['search']);
	$postcode_search = array(
		'q' => urlencode($search),
	);
	/*$api = 'https://digitalapi.auspost.com.au/';
	$auth_key = '0578121e-7aa1-4788-9b11-4673f41c307f';
	$edeliver_url = $api."postcode/search.json";*/
	$edeliver_url = AUSTRALIAN_POST_API."postcode/search.json";
	//$res = getSuburb($postcode_search);
	$first = true;
	$url = $edeliver_url;
	foreach ($postcode_search as $key => $value)
	{
	    $url .= $first ? '?' : '&';
	    $url .= "{$key}={$value}";
	    $first = false;     
	}   
	$ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
      'Auth-Key: ' . AUSTRALIAN_POST_AUTH_KEY
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $contents = curl_exec ($ch);
    curl_close ($ch);
    $res = json_decode($contents,true);
	$return_suburb = array();
	$i = 0;
	if(@count($res['localities']['locality']['location']) == 1){
	$i = 0;
	$return_suburb[$i]['location']      = $res['localities']['locality']['location'];
	$return_suburb[$i]['postcode']      = $res['localities']['locality']['postcode'];
	$return_suburb[$i]['state']         = $res['localities']['locality']['state'];

	}
	else{
		foreach($res['localities']['locality']  as $v){
		$return_suburb[$i]['location']      = $v['location'];
		$return_suburb[$i]['postcode']      = $v['postcode'];
		$return_suburb[$i]['state']         = $v['state'];
		$i++;
		}
	}
	if(empty(trim($return_suburb[0]['location']))){
		$data = false;
		echo json_encode($data);
	}
	else{
		$data = $return_suburb;
	 	echo json_encode($data);
	}
	exit;
}
add_action( 'wp_ajax_autocomplete_postcode', 'custom_aupost_postcode_api');
add_action( 'wp_ajax_nopriv_autocomplete_postcode', 'custom_aupost_postcode_api');
/*Autopopulate suburb or postcode on checkout page*/
/*Code added on 05-01-2021*/


//code added on 11-01-2021

// To Add extra column
add_action('vsz_cf7_admin_after_heading_field','vsz_cf7_admin_after_heading_field_callback',12,2);
function vsz_cf7_admin_after_heading_field_callback(){
    ?> <th class="manage-column" style="width: 200px;">Action</th><?php
}



// To Display column value
add_action('vsz_cf7_admin_after_body_field','vsz_cf7_admin_after_body_field_callback', 22, 2);
function vsz_cf7_admin_after_body_field_callback($form_id, $row_id){
	global $wpdb;
	//echo "Form id is ".$form_id;
    $row_id = (int)$row_id;
    $form_id = (int)$form_id;
  	$sql="SELECT name,value FROM ".$wpdb->prefix ."cf7_vdata_entry WHERE `cf7_id` = '".$form_id."' AND `data_id` = '".$row_id."'";
  	$result = $wpdb->get_results($sql);
  	$field_items = array();
	if($result){
		$data = array();
		$field_items = array();
		foreach ($result as $k=>$v) {
			if($v->name == 'first_name' || $v->name == 'last_name' || $v->name == 'email_address' || $v->name == 'FirstName' || $v->name == 'LastName' || $v->name == 'Email' ){
				$data = get_cf7_fieldvalue($form_id,$row_id,trim($v->name));
				foreach($data as $field=>$value){
					$field_items[$field]= $value;
				}
			 }
		}
    }
	if($field_items['first_name']){ $first_name    = addslashes(trim($field_items['first_name']));}else{ $first_name    = addslashes(trim($field_items['FirstName']));}
   if($field_items['last_name']){$last_name 	   = addslashes(trim($field_items['last_name']));}else{$last_name 	   = addslashes(trim($field_items['LastName']));}
    if($field_items['email_address']){ $email_address = addslashes(trim($field_items['email_address']));}else{ $email_address = addslashes(trim($field_items['Email']));}
   
    /*Code to check whether category is a free product promotion. 
    or not*/
    $cf7_formid =	$form_id; 
    $table_name = $wpdb->prefix ."cf7_post_mapping" ;
	$query = $wpdb->get_results("SELECT post_id FROM ".$table_name." WHERE cf7_id = '".$cf7_formid."'");
    $post_id = $query[0]->post_id;
    //$is_free_product_promo = false;
    /*Function added to get the promotion type*/
   //$promotion_type = get_post_meta($post_id, "promotion_type", $single = true);
  //  $promotion_type = trim($promotion_type);
   // if($promotion_type == "Free Product Promotion"){
   // 	$is_free_product_promo = true;
   // }
     /*Code to check whether category is a free product promotion 
     or not*/

    $table_name = $wpdb->prefix ."cf7_promotion_record_status";
    $query = $wpdb->get_results("SELECT approval_status,rejected_by,rejected_date,reason_to_reject,reject_other_reason,customer_ref,account_code  FROM ".$table_name." WHERE cf7_id = '".$form_id."' AND freegift_id = '".$row_id."'");
 	 //$query = $wpdb->get_results("SELECT approval_status,customer_ref  FROM ".$table_name." WHERE cf7_id = '".$form_id."' AND freegift_id = '".$row_id."'");
	$approval_status = $query[0]->approval_status;
 	$customer_ref 	 = $query[0]->customer_ref;
 	//$account_code    =  $query[0]->account_code;
 	/*Code to check whether is a free promotion or not*/
 	//if($is_free_product_promo){
	 	if($approval_status=='' && $cf7_formid!=5711){
	    ?>
	    <td><a href="#" id="freegift_approve" data-cf7-formid="<?php echo $form_id?>"  data-freegift-id="<?php echo $row_id; ?>">Approve</a> | <a href="#" id="freegift_reject"  data-post-id="<?php echo $post_id;?>"  data-cf7-formid="<?php echo $form_id?>"  data-user-recid="<?php echo $row_id; ?>"  data-first-name="<?php echo $first_name;?>" data-last-name="<?php echo $last_name;?>" data-email-address="<?php echo $email_address;?>"  class="open-my-dialog-reject-free">Reject</a></td>
	    <?php
	    }
	    else{
	    ?>
	    <td>
	    <?php
	    if($approval_status == 'Approved'){
	    	//if($account_code == ''){
    			//$account_code = 'ÁDVBBQ';
    		//}
	    	//$pronto_order_num = get_pronto_num($account_code,$customer_ref);
	    	//echo "Pronto order num is ".$pronto_order_num;
	    	$customer_ref_msg= '';
	    	$pronto_orderno_msg= '';
	    	//$pronto_orderno_msg = "Pronto Order No:".$pronto_order_num;
 
	    	if($customer_ref != ''){
	    		$customer_ref_msg = "<br/>Customer Ref:".$customer_ref;
	    	}
	    	//if($pronto_order_num != ''){
	    	//	$pronto_orderno_msg = "<strong>Pronto Order No: ".$pronto_order_num."</strong>";
	    	//}
	    	echo "<div style='color:#155724;font-size:14px;'>
	    	<strong>".$approval_status."<br/>Email Sent</strong>"."</div>";
	    	echo "<div id='approved_section_". $row_id ."'>";
	    	echo "<div class='approved_hide_section'>";
	    	echo "Customer Ref:".$customer_ref;
	    	echo "</div><br/>";

	    	echo "<button type='button' class='approved_show'>Show Details</button>&nbsp;";
	        echo "<button type='button' class='approved_hide' style='display:none'>Hide Details</button>";
	    	echo "</div>";
	    }
	   else if($approval_status == 'Rejected'){
	    	$rejected_by =  $query[0]->rejected_by;
	    	$reason_to_reject    =  $query[0]->reason_to_reject;
			$reject_other_reason =  nl2br(trim($query[0]->reject_other_reason));
			$approval_link = ' | <a href="#" id="freegift_approve" data-cf7-formid="'.$form_id.'"  data-freegift-id="'.$row_id.'">Approve</a>';
	    	echo "<div><span style='color:#8B0000;font-size:14px;'><strong>".$approval_status." </strong></span>".$approval_link ."<strong><br/><span style='color:#8B0000;font-size:14px;'>Email Sent</span></strong></div>";
			
	    	echo "<div id='reject_section_". $row_id ."'>";
	    	echo "<div class='reject_hide_section'>";
	    	echo "<div style='color:#8B0000;font-size:14px;'><strong>Rejected by:".$rejected_by."</strong></div>";
	    	echo "<div style='color:#8B0000;font-size:14px;'><strong>Rejected Reason:".$reason_to_reject."</strong></div>";
	    	if($reject_other_reason != ''){
	    		echo "<div style='color:#8B0000;font-size:14px;'><strong>Comments:".$reject_other_reason."</strong></div>";
	    	}
	    	echo "</div><br/>";
	    	echo "<button type='button' class='reject_show'>Show Details</button>&nbsp;";
	        echo "<button type='button' class='reject_hide' style='display:none'>Hide Details</button>";
	    	echo "</div>";

	    }
	    ?>
		</td>
	    <?php
	    }
	}
	
	
	/*Approving/Rejecting free product promotion,generate csv file and push to Pronto location*/
function custom_approve_or_reject_freegift_promotion(){
	
	global $wpdb;
	$date = new DateTime();
	$usersTimezone = 'Australia/Sydney';
	$tz = new DateTimeZone($usersTimezone);
	$date->setTimeZone($tz);
	$cf7_formid = (int)$_POST['cf7_formid'];
	$user_rec_id = $freegift_id = (int)$_POST['freegift_id'];
	$approval_status = $_POST['approval_status'];
	//print_r($_POST);exit;
	$table_name = $wpdb->prefix ."cf7_post_mapping" ;
	$query = $wpdb->get_results("SELECT post_id FROM ".$table_name." WHERE cf7_id = '".$cf7_formid."'");
 	$post_id = (int)$query[0]->post_id;
	
	$sqlemail="SELECT value FROM ".$wpdb->prefix ."cf7_vdata_entry WHERE `name` = 'Email' AND `cf7_id` = '".$cf7_formid."' AND `data_id` = '".$freegift_id."'";
	$email_result = $wpdb->get_results($sqlemail);
	$customerref=$email_result[0]->value;
//print_r($customerref);exit();
 	$custom_field_keys = get_post_custom_keys($post_id);
	//print_r($custom_field_keys);exit;
	$free_products_array = array();
	foreach ( $custom_field_keys as $key => $fieldkey )
	{
		//if (stristr($fieldkey,'free_products'))
	//	{
		    $field = get_field_object($fieldkey, $post_id);
		    $cfield_key = $field['name'];
		    $free_products_array[$cfield_key] = $field['value'];	
		//}
	}
	if(count($free_products_array) > 0){
	$free_products_basket = array();
	foreach($free_products_array as $k=>$v){
		if(is_array($v)){
			$free_products_basket[$k] = $v;
		}
	}
	}
	
	
	if($approval_status == 'Approved'){
	$sql="SELECT * FROM ".$wpdb->prefix ."cf7_vdata_entry WHERE `cf7_id` = '".$cf7_formid."' AND `data_id` = '".$freegift_id."'";
	$result = $wpdb->get_results($sql);
	
	if($result){
		$data = array();
		
		$field_items = array();
		foreach ($result as $k=>$v) {
			$data = get_cf7_fieldvalue($cf7_formid,$freegift_id,trim($v->name));
			
			foreach($data as $field=>$value){
				$field_items[$field]= $value;
			}
		 }
		
		//$promotion_code = get_post_meta($post_id, "promotion_code", $single = true);
		//$warehouse = get_post_meta($post_id, "warehouse", $single = true);
		$approval_reply_email = get_post_meta($post_id, "approval_email_for_cash_back_promotion_approval_reply_email", $single = true);
		
		$approval_response_email_title = get_post_meta($post_id, "approval_email_for_cash_back_promotion_approval_response_email_title", $single = true);
		$approval_response_email_content = get_post_meta($post_id, "approval_email_for_cash_back_promotion_approval_response_email_content", $single = true);
			
	

	
   }
}//if approval status
   if($approval_status == ''){
   	 $approval_status = 'Pending';
   } 
   //echo "Approval status is ".$approval_status;
   if($approval_status == 'Approved' || $approval_status == 'Rejected')
   {
	$table_name = $wpdb->prefix ."cf7_promotion_record_status";
	$query = $wpdb->get_results("SELECT count(id) as cnt FROM ".$table_name." WHERE cf7_id = '".$cf7_formid."' AND post_id = '".$post_id."' AND freegift_id='".$freegift_id."' AND approval_status='".$approval_status."'");
 	$rowcount = $query[0]->cnt;
 	//echo "Row cnt is ".$rowcount;
 	if($rowcount==0){
		$where_condition = 
		array('cf7_id'=>$cf7_formid,'post_id'=>$post_id,
			'freegift_id'=>$freegift_id);
		/*If pushed to ftp file server is true, then only status is changed to approved and send email to customer*/
		if($approval_status == 'Approved'){
			$wpdb->delete( $table_name, $where_condition);
			$current_user = wp_get_current_user();
	    	$approved_by = $current_user->user_login; 
			//print_r($customerref);
			//print_r($approved_by);
			//exit();
	    	//$approved_date = wp_date('Y-m-d H:i:s');
			$approved_date =date("Y-m-d H:i:s");
			$insert_data = array('cf7_id'=>$cf7_formid,'post_id'=>$post_id,
				'freegift_id'=>$freegift_id,'approval_status'=>$approval_status,'customer_ref'=>$customerref,'approved_by'=>$approved_by,'approved_date'=>$approved_date);
			$wpdb->insert($table_name, $insert_data);
			
			/*promotion_staus entry to cf7_vdata_entry */
			
			$wpdb->query("UPDATE ".$wpdb->prefix ."cf7_vdata_entry SET value = '".$approval_status."' WHERE `cf7_id` = '".$cf7_formid."'AND `data_id` = '".$freegift_id."' AND name = 'promotion_status'");
			/*promotion_staus entry to cf7_vdata_entry */
			/*Code to send approval email for the free product promotion user*/
			custom_email_freeproduct_approval($post_id,$cf7_formid,$user_rec_id);
		}
		else if($approval_status == 'Rejected'){
			$wpdb->delete( $table_name, $where_condition);
			$current_user = wp_get_current_user();
			$rejected_by = $current_user->user_login; 
			//$rejected_date = wp_date('Y-m-d H:i:s');
			$rejected_date = date('Y-m-d H:i:s');
			$reject_reason = $_POST['reject_reason'];
			$comment_other = strip_tags(trim($_POST['comment_other']));
			$insert_data = array('cf7_id'=>$cf7_formid,'post_id'=>$post_id,
				'freegift_id'=>$freegift_id,'approval_status'=>$approval_status,'rejected_by'=>$rejected_by,'reason_to_reject'=>$reject_reason,'reject_other_reason'=>$comment_other,'rejected_date'=>$rejected_date);
			$wpdb->insert($table_name, $insert_data);
			/*promotion_staus entry to cf7_vdata_entry*/
			
			$wpdb->query("UPDATE ".$wpdb->prefix ."cf7_vdata_entry SET value = '".$approval_status."' WHERE `cf7_id` = '".$cf7_formid."'AND `data_id` = '".$freegift_id."' AND name = 'promotion_status'");
			/*promotion_staus entry to cf7_vdata_entry */
			/*Code to send reject email for the rejected free product promotion user*/
			custom_email_reject_freeproduct_promotion($post_id,$cf7_formid,$user_rec_id,$reject_reason,$comment_other);
		}
		//echo $wpdb->last_query;
		echo $approval_status;
	}
	}//if Approval status is approved or rejected
	exit;

}
add_action( 'wp_ajax_freegift_promotion', 'custom_approve_or_reject_freegift_promotion');
add_action( 'wp_ajax_nopriv_freegift_promotion', 'custom_approve_or_reject_freegift_promotion');

function get_cf7_fieldvalue($cf7_formid ,$freegift_id,$field_name=''){
	global $wpdb;
	$data = array();
	
	$sql="SELECT * FROM ".$wpdb->prefix ."cf7_vdata_entry WHERE `cf7_id` = '".$cf7_formid."' AND `data_id` = '".$freegift_id."' AND name='".$field_name."'";
	$result = $wpdb->get_results($sql);
	
	$field_value = '';
	foreach ($result as $k=>$v) {
		$field_value = $v->value;
		$data[$field_name] = $field_value;
	}
	return $data; 
}
/*Section For Free Product Promotion Ends*/



/*Code to send approval email for the cashback promotion user*/
function custom_email_freeproduct_approval($post_id='',$cf7_formid='',$user_rec_id=''){
	
	
	global $wpdb;
	$sql="SELECT * FROM ".$wpdb->prefix ."cf7_vdata_entry WHERE `cf7_id` = '".$cf7_formid."' AND `data_id` = '".$user_rec_id."'";
	$result = $wpdb->get_results($sql);
	if($result){
	$data = array();
	$field_items = array();
	foreach ($result as $k=>$v) {
		if($v->name == 'first_name' || $v->name == 'last_name' || $v->name == 'email_address'|| $v->name == 'FirstName' || $v->name == 'LastName' || $v->name == 'Email'){
			$data = get_cf7_fieldvalue($cf7_formid,$user_rec_id,trim($v->name));
			foreach($data as $field=>$value){
				$field_items[$field]= $value;
			}
		}
	 }
	 if($field_items['first_name']){ $first_name    = addslashes(trim($field_items['first_name']));}else{ $first_name    = addslashes(trim($field_items['FirstName']));}
   if($field_items['last_name']){$last_name 	   = addslashes(trim($field_items['last_name']));}else{$last_name 	   = addslashes(trim($field_items['LastName']));}
    if($field_items['email_address']){ $email_address = addslashes(trim($field_items['email_address']));}else{ $email_address = addslashes(trim($field_items['Email']));}
	//$first_name    = addslashes(trim($field_items['first_name']));
    //$last_name 	   = addslashes(trim($field_items['last_name']));
    $name  = $first_name.' '.$last_name;
    $name  = ucwords($name);
    //$email_address = addslashes(trim($field_items['email_address']));
    $promotion_post_title = ucwords(get_the_title($post_id)); 
	$approval_reply_email = get_post_meta($post_id, "approval_email_for_cash_back_promotion_approval_reply_email", $single = true);
	$approval_response_email_title = get_post_meta($post_id, "approval_email_for_cash_back_promotion_approval_response_email_title", $single = true);
	$approval_response_email_content = get_post_meta($post_id, "approval_email_for_cash_back_promotion_approval_response_email_content", $single = true);
	$to 	 = trim($email_address);
	$subject = trim($approval_response_email_title);
	$message = trim($approval_response_email_content);
	$message_content = str_replace(array("{{name}}","{{promotion_post_title}}"),array($name,$promotion_post_title),$message);
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= trim($approval_reply_email);
	if($to!= '' && $subject != '' && $message!= ''){
		//echo 'to:'.$to;
		//echo 'sub:'.$subject;
		//echo 'msg:'.$message_content;
		//echo 'header:'.$headers;
		//exit();
		//wp_mail( $to, $subject, $message_content, $headers);
		$email_header = get_custom_email_header();
		$email_footer = get_custom_email_footer();
		$email_message = $email_header.$message_content.$email_footer; 
		wp_mail( $to, $subject, $email_message, $headers);
		//mail( $to, $subject, $email_message, $headers);
		$email_message = '';
	}
}
}
/*Code to send approval email for the cash back promotion user*/


/*This code is used to add jquery dialog  popup in wordpress admin footer*/ 
function insert_dialog_wpadmin(){
	//get_template_part( 'approvalcoupon', 'promotion' );
	//get_template_part( 'rejectcouponcode', 'promotion' );
	get_template_part( 'rejectfreeproduct', 'promotion' );
}
/*This code is used to add jquery dialog  popup in wordpress admin footer*/ 
add_action( 'admin_footer', 'insert_dialog_wpadmin' );

/*Code to send reject email for the rejected free product promotion user*/
function custom_email_reject_freeproduct_promotion($post_id='',$cf7_formid='',$user_rec_id='',$reject_reason='',$comment_other=''){
	global $wpdb;
	$sql="SELECT * FROM ".$wpdb->prefix ."cf7_vdata_entry WHERE `cf7_id` = '".$cf7_formid."' AND `data_id` = '".$user_rec_id."'";
	$result = $wpdb->get_results($sql);
	if($result){
	$data = array();
	$field_items = array();
	foreach ($result as $k=>$v) {
		if($v->name == 'first_name' || $v->name == 'last_name' || $v->name == 'email_address' || $v->name == 'FirstName' || $v->name == 'LastName' || $v->name == 'Email'){
			$data = get_cf7_fieldvalue($cf7_formid,$user_rec_id,trim($v->name));
			foreach($data as $field=>$value){
				$field_items[$field]= $value;
			}
		}
	 }
	 if($field_items['first_name']){ $first_name    = addslashes(trim($field_items['first_name']));}else{ $first_name    = addslashes(trim($field_items['FirstName']));}
   if($field_items['last_name']){$last_name 	   = addslashes(trim($field_items['last_name']));}else{$last_name 	   = addslashes(trim($field_items['LastName']));}
    if($field_items['email_address']){ $email_address = addslashes(trim($field_items['email_address']));}else{ $email_address = addslashes(trim($field_items['Email']));}
	
    $name  = $first_name.' '.$last_name;
    $name  = ucwords($name);
    
    //Get the promotion post title
    $promotion_post_title = ucwords(get_the_title($post_id)); 
	$reject_reply_email = get_post_meta($post_id, "reject_email_for_cash_back_promotion_reject_reply_email", $single = true);
	$reject_response_email_title = get_post_meta($post_id, "reject_email_for_cash_back_promotion_reject_response_email_title", $single = true);
	$reject_response_email_content = get_post_meta($post_id, "reject_email_for_cash_back_promotion_reject_response_email_content", $single = true);
	$to 	 = trim($email_address);
	$subject = trim($reject_response_email_title);
	$message = trim($reject_response_email_content);
	$reject_comment = '';
	if($comment_other != ''){
		$reject_comment = 'Comments: '.nl2br(stripslashes($comment_other));
	}
	$message_content = str_replace(array("{{name}}","{{promotion_post_title}}","{{reject_reason}}","{{comment_other}}"),array($name,$promotion_post_title,$reject_reason,$reject_comment),$message);
	$headers = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= trim($reject_reply_email);
	//echo $reject_reply_email;
	//print_r($headers);
	//exit();
	if($to!= '' && $subject != '' && $message!= ''){
		$email_header = get_custom_email_header();
		$email_footer = get_custom_email_footer();
		$email_message = $email_header.$message_content.$email_footer; 
		wp_mail( $to, $subject, $email_message, $headers);
		$email_message = '';
	}
}
}
/*Code to send reject email for the rejected free product promotion user*/
/*Function to get email template header*/
function get_custom_email_header(){
	$email_header = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	 <head>
	  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	  <title>CASIO Email</title>
	  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>
	';
	return $email_header;
}
/*Function to get email template header*/

/*Function to get email template footer*/
function get_custom_email_footer(){
	$email_footer = '
	</body></html>
	';
	return $email_footer;
}
/*Function to get email template footer*/




/*Low stock email notifications*/
function cronjob_low_stock_email_notification(){
global $wpdb;
$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers  .= 'Bcc: cameront@shriro.com.au'. "\r\n";
$headers  .= 'Bcc: maheshnis@shriro.com.au'. "\r\n";
$headers  .= 'Bcc: robinv@shriro.com.au'. "\r\n";


$subject = 'Casio EMI Low Stock Products';
$table_name = $wpdb->prefix.'stock_availability';
$query = $wpdb->get_results("SELECT `stock-code` as stock_code,`stock-quantity` as stock_quantity,warehouse FROM ".$table_name." WHERE `stock-quantity` < 10");
  if($query){
    $email_content = '';
    $k = 0;
    foreach($query as $k=>$v){
    	$stock_code     = trim($v->stock_code);
    	$stock_quantity = trim($v->stock_quantity);
    	$warehouse = trim($v->warehouse);
      if($k% 2 == 0){
        $bg_color = '#ffffff';
      }
      else{
        $bg_color = '#cccccc';

      }
      $email_content .= '<tr>
        <td style="background: '.$bg_color.'; text-align: center;padding:5px;">'.$stock_code .'
        </td>
        <td style="background: '.$bg_color.'; text-align: center;padding:5px;">'.$stock_quantity.'
        </td>
        <td style="background: '.$bg_color.'; text-align: center;padding:5px;">'.$warehouse.'
        </td>
      </tr>';
      $k++;
    }
$lowstock_email_template = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml">
   <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>CASIO Email</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  </head>
  <body>
<table class="body-wrap">
<tbody>
<tr>
<td></td>
<td class="container" width="600">
<table class="main" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="content-wrap">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="content-block" style="padding-top:20px;">
<p>Hi,<br/>
Please check the below Casio EMI products which are low in stock.
</p>
</td>
</tr>
<tr>
<td class="content-wrap">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<th style="background:#ccc;text-align: center;padding:5px;">
Stock code</th>
<th style="background:#ccc;text-align: center;padding:5px;">
Stock Quantity</th>
<th style="background:#ccc;text-align: center;padding:5px;">
Warehouse</th>
</tr>
'.$email_content.'
</tbody>
</table>
</td>
</tr>

<tr>
<td class="content-block"><br/>&#8212;Casio EMI Team</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</body></html>
';
//echo $lowstock_email_template;
$to = 'melissad@shriro.com.au';
//$to = 'robinv@shriro.com.au';
wp_mail($to,$subject,$lowstock_email_template,$headers);
}
exit;
}
add_action( 'wp_ajax_custom_low_stock_notification', 'cronjob_low_stock_email_notification');
add_action( 'wp_ajax_nopriv_custom_low_stock_notification', 'cronjob_low_stock_email_notification');
/*Low stock email notifications*/
/*wp_mail function from email change*/
function change_my_from_address( $original_email_address ) {
    return 'noreply@casiomusic.com.au';
}
add_filter( 'wp_mail_from', 'change_my_from_address' );
 
// Function to change sender name
function change_my_sender_name( $original_email_from ) {
    return 'Casio Music';
}
add_filter( 'wp_mail_from_name', 'change_my_sender_name' );
/*wp_mail function from email change*/

/*Get pronto order number for the Casio EMI sales order and update in the order details page*/
add_action( 'woocommerce_admin_order_data_after_order_details', 'custom_order_meta_pronto_order_num' );
 function custom_order_meta_pronto_order_num($order){
 	   global $wpdb;
	   //$accountcode = 'HESTON';
	   $accountcode = 'CASIOEMI';
	   $order_id = get_numerics($order->get_id());
	   $cust_ref = $order->get_order_number();
  	   $split_ord_customer_refs = get_post_meta($order_id, 'split_orders_customer_ref', true);
	   $pronto_order_num = get_post_meta($order_id, 'pronto_order_num', true);
	   $pronto_orders_processed = get_post_meta($order_id, 'pronto_orders_processed', true);
if( current_user_can('administrator')){
	?>
		<br class="clear" />
		<?php 
		?>
		<div class="address">
			<?php
			if(!empty($split_ord_customer_refs)){
			?>
			<p><h4>Order Customer Reference:</h4><?php echo $split_ord_customer_refs;?></p>
			<?php
			}
			if(!empty($pronto_order_num)){
			?>
				<p><h4>Pronto Order Number:</h4><?php echo $pronto_order_num;?></p>
			<?php
		    }
		    if($pronto_orders_processed == 'Yes'){
		    ?>
		    <p><h4>Pronto Orders Processed:<?php echo $pronto_orders_processed;?></h4></p>
		    <?php

		    }
		    else{
		   ?>
		      <p><h4>Pronto Orders Processed:No</h4></p>
		   <?php
			}
		   ?>
		</div>
	 
 <?php 
}
} 
 ?>
 <?php
 /*Function to retreive pronto order number for all processed orders from sales order table*/
 function custom_update_woocommerce_orders_pronto_order_num(){
 	   global $wpdb;
 	   $order_id_array = array();
 	   $sql = "SELECT woocommerce_order_id,customer_ref FROM `ordermast` WHERE `send_timtestap` >= DATE_ADD(CURDATE(), INTERVAL -5 DAY)";
 	    $order_records = $wpdb->get_results($sql);
 	    if($order_records){
	 	    $k = 0;
	 	    foreach($order_records as $k=>$v){
	 	    	$order_id_array[$k]['order_id'] = $v->woocommerce_order_id;
	 	    	$order_id_array[$k]['customer_ref'] = $v->customer_ref;
	 	    	$k++;
	 	    }
 		}
	   /*echo "<pre>";
	   print_r($order_id_array);
	   echo "</pre>";
	   exit;*/
	  $mydb = new wpdb('ebiz','jMZmGE#aMwFFv2cc8','ebiz','mysql.shriro.com.au');
 	  /*$mydb = new wpdb('testebiz_au2','T2Nmzsu8p3BvTUHG#','TEST_ebiz_AU','mysql.shriro.com.au');*/
 	  //var_dump($mydb);
 	  foreach($order_id_array as $k=>$v){
 	   $pronto_order = array();
	   //$accountcode = 'HESTON';
	   $accountcode = 'CASIOEMI';
	   $order_id = trim($v['order_id']);
	   $cust_ref_order = trim($v['customer_ref']);
	   //echo "<br/>Order id is ".$order_id ;
	   $pronto_order_num_meta = get_post_meta($order_id, 'pronto_order_num', true);
	   //echo "Pronto order meta is ".$pronto_order_num_meta;
	   $pronto_order_num = '';
	   /*Function to get pronto order number from ebiz database table*/
	   /*Code to check whether order is processed or not*/
	    $sql = "SELECT `splitorders_customer_ref` FROM `ordermast` WHERE `woocommerce_order_id`='".$order_id."'";
	    $split_orders = $wpdb->get_results($sql);
	    $splitorders_ref = $split_orders[0]->splitorders_customer_ref;
		$splitorders_customer_ref = explode(",",$splitorders_ref);
		//$splitorders_customer_ref = array('EHBAU17901','EHBAU17152');

		//print_r($splitorders_customer_ref);
		if(count($splitorders_customer_ref) > 0){
			foreach($splitorders_customer_ref  as $cust_ref){
				$sql = "SELECT `so_order_no` FROM sales_order where so_cust_code='" . strtoupper($accountcode) . "' and so_cust_reference='" . strtoupper($cust_ref) . "'";
				echo "<br/>".$sql;
				$check_sales_order = $mydb->get_results($sql);
				//print_r($check_sales_order);
				$so_order_no = $check_sales_order[0]->{'so_order_no'};
				//echo "<br/>Sales order no is ".$so_order_no;
				if(!empty($so_order_no)){
					$pronto_order[] = $cust_ref." - ".$so_order_no;
				}
				if(empty($so_order_no)){
					$sql = "SELECT `so-order-no` FROM ordamast where `so-cust-code`='" . strtoupper($accountcode) . "' and `so-cust-reference`='" . strtoupper($cust_ref) . "'";
					//echo "<br/>".$sql;
					$check_ordamast = $mydb->get_results($sql);
					$so_order_no = $check_ordamast[0]->{'so-order-no'};
					if(!empty($so_order_no)){
						$pronto_order[] = $cust_ref." - ".$so_order_no;
					}
				}
			}
		}
		/*If pronto order number is not in sales order and order mast, please check for order number in order meta which was already saved.*/
	   //print_r($pronto_order);exit;
	   if(count($pronto_order) > 0){
			$pronto_order_num = implode(" , ",$pronto_order);
			$data_po = array('pronto_order_num'=>$pronto_order_num);
			$where_condition = array('woocommerce_order_id'=>$order_id);
			$wpdb->update('ordermast',$data_po,$where_condition);
			//echo "<br/>Query is ".$wpdb->last_query;
			update_post_meta( $order_id, 'pronto_order_num', wc_clean($pronto_order_num));
				$pronto_order_num = get_post_meta( $order_id, 'pronto_order_num', true);
	   		}
	   		$sql = "SELECT `splitorders_customer_ref`,`pronto_order_num` as po_num FROM `ordermast` WHERE `woocommerce_order_id`='".$order_id."'";
	    	$row = $wpdb->get_results($sql);
	    	//print_r($row);
	    	$temp_cust_ref = $row[0]->splitorders_customer_ref;
	    	$po_num = $row[0]->po_num;
	    	$is_update_order_meta = true;
	    	echo "<br/>Temp ref is ".$temp_cust_ref;
	    	echo "<br/>PO ref is ".$po_num;
	    	if(!empty($temp_cust_ref) && !empty($po_num)){
	    		$ref_splitorder_cnt = count(explode(',',$temp_cust_ref));
	    		$ref_pronto_processed_cnt = count(explode(',',$po_num));
	    		echo "<br/>Split order count is ".$ref_splitorder_cnt ;
	    	    echo "<br/>Pronto processed order count is ".$ref_pronto_processed_cnt ;

	    	/*If processed 	pronto order count and split order processed count are same*/
	    		if($ref_splitorder_cnt == $ref_pronto_processed_cnt){
	    			echo "<br/>All orders processed";
	    			$data_po_processed = array('pronto_orders_processed'=>'Processed');
					$where_condition = array('woocommerce_order_id'=>$order_id);
					$wpdb->update('ordermast',$data_po_processed,$where_condition);
					echo "<br/>Query is ".$wpdb->last_query;
					update_post_meta( $order_id, 'pronto_orders_processed', wc_clean('Yes'));
	    		  $is_update_order_meta = false;	
	    		}
	    	}
	    	if($is_update_order_meta){
	   		if(count($splitorders_customer_ref) > 0){
		   		$split_orders_ref = implode($splitorders_customer_ref," , ");
		   		update_post_meta( $order_id, 'split_orders_customer_ref', wc_clean($split_orders_ref));
		   		$split_ord_customer_refs = get_post_meta($order_id, 'split_orders_customer_ref', true);
	   			}
	   		else{
	   			//echo "<br/>Split Orders Customers Ref";
	   			$split_ord_customer_refs = get_post_meta($order_id, 'split_orders_customer_ref', true);
	   		}
	   	}
	  }
} 
add_action( 'wp_ajax_cron_pronto_order_num_update', 'custom_update_woocommerce_orders_pronto_order_num');
add_action( 'wp_ajax_nopriv_cron_pronto_order_num_update', 'custom_update_woocommerce_orders_pronto_order_num');
/*Get all sales orders where pronto order numbers were not updated*/
function custom_get_all_processing_orders(){
	$prefix = 'CASIOEMI';
	$initial_date = date('Y-m-d', strtotime('-5 days'));
	$final_date   = date('Y-m-d', strtotime('-1 day'));
	$order_details = wc_get_orders(array(
	    'limit'=>-1,
	    'type'=> 'shop_order',
	    'status'=> array( 'wc-processing'),
	    'date_created'=> $initial_date .'...'. $final_date,
	 	'meta_key'     => 'pronto_orders_processed', // The postmeta key field
	    'meta_compare' => 'NOT EXISTS', // The comparison argument
	    )
	);
	/*echo "<pre>";
	print_r($order_details);
	echo "</pre>";*/
	$orders_list = array();
	foreach($order_details as $order){
		$order_id  = $order->get_id(); // Get the order ID
		echo "<br/>Order ID is ".$prefix .$order_id;
		$orders_list[] = '<li style="padding:5px;">'.
		$prefix .$order_id.'</l1>';
	}
if(count($orders_list) > 0){
$list_of_orders = implode("<br/>",$orders_list);
$email_content = '<table border="0" width="100%" cellspacing="0" cellpadding="0" bgcolor="#fff">
<tbody>
<tr>
<td style="padding: 10px 0 30px 0;">
<table style="border: 0px solid #000; border-collapse: collapse;" border="0" width="100%" cellspacing="0" cellpadding="0" align="center">
<tbody>
<tr>
<td style="padding: 40px 10px 30px 10px; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;" align="center" bgcolor="#000000"><span style="color: #3a3a3a; font-family: Georgia, Times New Roman, Bitstream Charter, Times, serif;"><span style="font-size: 16px; font-weight: 400;"><img class="alignnone size-full wp-image-12746" src="http://casiomusic.com.au/wp-content/uploads/2020/08/Casio-logo-White.png" alt="Casio WHITE COMBINED LOGO-01" width="196" height="61" /></td>
</tr>
<tr>
<td style="color: #000; padding: 40px 30px 40px 30px;" bgcolor="#fff">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td>
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td valign="top" width="260">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="content-wrap">
<table style="color: #000;" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="content-block" align="center" style="text-align:center;"><h2 align="center" style="text-align:center;">Casio EMI Orders</h2></td>
</tr>
<tr>
<td style="padding: 10px;"></td>
</tr>
<tr>
<td class="content-block"><p>Please check why the pronto order numbers were not updated for the following Casio EMI AU processing orders.</p></td>
</tr>
<tr>
<td class="content-block"><ul>'.$list_of_orders.'</ul></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
<td style="padding: 30px 30px 30px 30px;" bgcolor="#000000">
<table border="0" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="35%">Casio EMI Team</td>
<td style="text-decoration: none;" align="right" width="65%">
<table border="0" cellspacing="0" cellpadding="0" bgcolor="#000000">
<tbody>
<tr>
<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold; text-decoration: none; padding: 0px;"></td>
<td style="font-size: 0; line-height: 0; text-decoration: none; padding: 0px;" width="20"></td>
<td style="font-family: Arial, sans-serif; font-size: 12px; color: #fff;">Follow <a style="color: #fff;" href="#">Casio EMI</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>';
$message = $email_content;

$to      = 'melissad@shriro.com.au';
$subject = 'CasioEMI Pronto Order Number-Not Updated';
$headers[] = 'From: No-Reply<noreply@everdurebyheston.com.au>';
$headers[] = 'Content-Type: text/html; charset=UTF-8';
$headers[] = 'Cc: cameront@shriro.com.au';
$headers[] = 'Bcc: robinv@shriro.com.au';

wp_mail( $to, $subject, $message, $headers );
}
}
add_action( 'wp_ajax_custom_order_processed_pronto_check', 'custom_get_all_processing_orders');
add_action( 'wp_ajax_nopriv_custom_order_processed_pronto_check', 'custom_get_all_processing_orders');
/*Get all sales orders where pronto order numbers were not updated*/

?>

<?php 
/* code added on 22/01/2021*/
/*return to home */

add_filter( 'woocommerce_return_to_shop_redirect', 'st_woocommerce_shop_url' );
/**
 * Redirect WooCommerce Shop URL
 */

function st_woocommerce_shop_url(){

return site_url() . '/';

}
/*chnage the text from return to shop to return to home */

add_filter( 'gettext', 'change_woocommerce_return_to_shop_text', 20, 3 );
function change_woocommerce_return_to_shop_text( $translated_text, $text, $domain ) {
       switch ( $translated_text ) {
                      case 'Return to shop' :
   $translated_text = __( 'Return to Home', 'woocommerce' );
   break;
  }
 return $translated_text; 

}

?>