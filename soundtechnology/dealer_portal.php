<?php
/**
 * The template for Dealers
 *
 * This is the template is for dealers to login 
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 
 * Template Name: Dealer Portal

 */

get_header(); ?>

	<div id="primary" class="content-area product_images_dealer">
		<main id="main" class="site-main m30tb" role="main">
<h3>Please sign in</h3>


<?php echo do_shortcode('[wppb-login]');?>
<?php
if ( is_user_logged_in() ) {
   
} else {
    echo '<a href="/forget-password">Forget your password?</a><br>';
}
?>

<a href="/dealer-register">Dealer to register?</a>

		
		
		
	
	
	
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'ecommerce_gem_action_sidebar' );

get_footer();
