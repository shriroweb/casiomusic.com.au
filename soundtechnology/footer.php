<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eCommerce_Gem
 */

	/**
	 * Hook - ecommerce_gem_after_content.
	 *
	 * @hooked ecommerce_gem_after_content_action - 10
	 */
	do_action( 'ecommerce_gem_after_content' );

?>

	<?php get_template_part( 'template-parts/footer-widgets' ); ?>
<!---->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="site-footer-wrap">
		
				<?php 

				$copyright_text = ecommerce_gem_get_option( 'copyright_text' ); 

				if ( ! empty( $copyright_text ) ) : ?>

					<div class="copyright">

						<?php echo wp_kses_data( $copyright_text ); ?>

					</div><!-- .copyright -->

					<?php 

				endif; 

				//do_action( 'ecommerce_gem_credit' ); 

				?>
			</div>
		</div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
    WebFontConfig = {
        google: { families: [ 'Noto+Serif:400,400italic,700,700italic' ] }
    };

    (function() {
        var wf = document.createElement('script');
        wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
<script>
jQuery( function( $ ) {
	$( document ).ready(function() {
		$( ".woocommerce-checkout-review-order-table" ).hide();
		$( "#order_review_heading" ).click( function() {
			$(this).toggleClass( "hideorders" ).next().slideToggle( "normal" );
			//$(".woocommerce-checkout-review-order-table").slideToggle( "normal" );
			return false;
		} );
	} );
} );
</script>
<script>
!function (w, d, t) {
w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i+)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i"?sdkid="e"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)};

ttq.load('C65JM9GU4DKUD0OQI140');
ttq.page();
}(window, document, 'ttq');
</script>

</body>
</html>
