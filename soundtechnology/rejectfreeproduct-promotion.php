<script>
/*Modal popup for wordpress to reject free product promotion*/
jQuery(document).ready(function(){
(function ($) {
  // initalise the dialog
  jQuery('#my-dialog-reject-free').dialog({
    title: 'Reject Cash Back Promotion',
    dialogClass: 'wp-dialog',
    autoOpen: false,
    draggable: false,
    width: 'auto',
    modal: true,
    resizable: false,
    closeOnEscape: true,
    position: {
      my: "center",
      at: "center",
      of: window
    },
    open: function () {
      // close dialog by clicking the overlay behind it
      jQuery('.ui-widget-overlay').bind('click', function(){
        jQuery('#my-dialog-reject-free').dialog('close');
      })
    },
    create: function () {
      // style fix for WordPress admin
      jQuery('.ui-dialog-titlebar-close').addClass('ui-button');
    },
  });

  // bind a button or a link to open the dialog
  jQuery('a.open-my-dialog-reject-free').click(function(e) {
    e.preventDefault();
    jQuery('#my-dialog-reject-free').dialog('open');
      var cf7_id = $(this).data('cf7-formid');
      var user_rec_id  = $(this).data('user-recid');
      var post_id  = $(this).data('post-id');
      var name  = $(this).data('first-name')+' '+$(this).data('last-name');
      name = name.replace(new RegExp("\\\\", "g"), "");
      var email_address  = $(this).data('email-address');
        jQuery('#post_id_free_ele_reject').html('<input id="post_id_free_reject" name="post_id_free_reject" value="'+post_id+'" type="hidden">');
        jQuery('#cf7_id_free_ele_reject').html('<input id="cf7_id_free_reject" name="cf7_id_free_reject" value="'+cf7_id+'" type="hidden">');
        jQuery('#user_rec_id_free_ele_reject').html('<input id="user_rec_id_free_reject" name="user_rec_id_free_reject" value="'+user_rec_id+'" type="hidden">');
        jQuery('#name_id_free_ele_reject').html(name);
        jQuery('#email_id_free_ele_reject').html(email_address);
  });
})(jQuery);
});
/*Modal popup for wordpress admin to reject free product promotion*/

jQuery(document).on('click','#reject_free_promotion',function(){
        //alert('Clicked');
        var reject_option = jQuery("#reject_reason_free option:selected").val();
        var comment_other = jQuery('#comment_other_free').val();
        comment_other = comment_other.replace(/<\/?.+?>/ig, '');
        if(reject_option == ''){
          alert("Please choose a valid reject reason");
          return false;
        }
        var name  = jQuery('#name_id_free_ele_reject').text();
        name = name.replace(new RegExp("\\\\", "g"), "");
        var confirm_status = 
        confirm("Are you sure to reject this user "+name+"?");
        if(confirm_status){
        var post_id = jQuery('#post_id_free_reject').val();
        var cf7_formid = jQuery('#cf7_id_free_reject').val();
        var user_rec_id = jQuery('#user_rec_id_free_reject').val();
		//alert(post_id);
		//alert(cf7_formid);
		//alert(user_rec_id);
		//console.log(data);
              //alert(data);
        //var reject_reason = jQuery.trim(jQuery("#reject_reason").val());
        /*Remove html tags from string*/
        //reject_reason = reject_reason.replace(/<\/?.+?>/ig, '');
        jQuery.ajax({
            type: 'POST',
            url: "/wp-admin/admin-ajax.php",
            data: { action: "freegift_promotion",
                    post_id: post_id,
                    cf7_formid: cf7_formid,
                    freegift_id: user_rec_id,
                    reject_reason:reject_option,
                    comment_other:comment_other,
                    approval_status:'Rejected' 
            },success: function(data){
               //console.log(data);
             // alert(data);
               if(data.trim() == 'Rejected'){
                alert('User was rejected successfully.');
                window.location.reload();
               }
            }
        });
        return true;
        }
    });
jQuery(document).ready(function(){
    jQuery('#reject_reason_free').change(function() {
     var reject_option = jQuery("#reject_reason_free option:selected").text();
     jQuery('#comment_section_free').hide();
     if(reject_option == 'Other'){
        jQuery('#comment_section_free').show();
     }
  });  
});
</script>

<div id="my-dialog-reject-free" class="hidden" style="max-width:800px">
  <h3>Please click on Reject Cash Back for the user registered for this promotion.</h3>
<div class="container">
  <div class="main">
  <div id="dialog-reject" title="Reject this promotion">
      <div id="post_id_free_ele_reject">
      </div>
      <div id="cf7_id_free_ele_reject">
      </div>
      <div id="user_rec_id_free_ele_reject">
      </div>
      <div id="email_ele_free_reject">
      </div>
      <br/>
      <div class="table-inner-structure">
    <table class="fixed" cellspacing="0">
        <thead>
        </thead>
        <tbody>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr class="alternate">
                <td width="30%" class="column-columnname">
                  <label for="Name">Name:</label></td>
                <td width="2%"></td>
                <td class="column-columnname" id="name_id_free_ele_reject"></td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr>
                <td width="30%" class="column-columnname"><label for="Email">Email:</label></td>
                <td width="2%"></td>
                <td class="column-columnname" id="email_id_free_ele_reject"></td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr>
                <td width="30%" class="column-columnname"><label for="reject_reason_free">Reason for rejection:</label></td>
                <td width="2%"></td>
                <td class="column-columnname">
                  <select name="reject_reason_free" id="reject_reason_free">
                    <option value="">Please select reason</option>
                    <option value="Missing Proof of Purchase">Missing Proof of Purchase</option>
                    <option value="Outside Promotional Period">Outside Promotional Period</option>
                    <option value="Missing Information Fields">Missing Information Fields</option>
                    <option value="Other">Other</option>
                  </select>
                </td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr id="comment_section_free" style="display:none;">
                <td width="30%" class="column-columnname"><label for="comment">Comment:</label></td>
                <td width="2%"></td>
                <td class="column-columnname">
                  <textarea name="comment_other_free" id="comment_other_free" rows="2" cols="40"></textarea></td>
            </tr>
            <tr>
              <td colspan="3" height="10"></td>
            </tr>
            <tr>
              <td colspan="3" height="20"></td>
            </tr>
            <tr>
              <td colspan="3" height="40"></td>
            </tr>
            <tr>
                <td width="40%" class="column-columnname"></td>
                <td width="2%"></td>
                <td><input type="button" name="reject_free_promotion" id="reject_free_promotion" value="Reject Free Product Promotion" class="button button-primary button-large">
                </td>
            </tr>
        </tbody>
    </table>
  </div>
    </div>
  </div>
</div>
</div>