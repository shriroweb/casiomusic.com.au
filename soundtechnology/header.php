<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eCommerce_Gem
 */

?>
<?php
	/**
	 * Hook - ecommerce_gem_doctype.
	 *
	 * @hooked ecommerce_gem_doctype_action - 10
	 */
	do_action( 'ecommerce_gem_doctype' );
?>




<head>

<!-- End Tiktok Pixel Code -->
<script> !function (w, d, t) { w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)}; ttq.load('C65JM9GU4DKUD0OQI140'); ttq.page(); }(window, document, 'ttq'); </script>
<!-- End Tiktok Pixel Code -->
	<?php
	/**
	 * Hook - ecommerce_gem_head.
	 *
	 * @hooked ecommerce_gem_head_action - 10
	 */
	do_action( 'ecommerce_gem_head' );
	
	wp_head(); ?>
	<meta name="facebook-domain-verification" content="bi0jh9ayzq4m6zkpb0fg6vj90hsdbk" />
	<link href="https://fonts.googleapis.com/css?family=Oswald|Droid+Sans:400,700" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,200,300,400,500,600,700,800,900" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
		
		<?php $pageid=get_the_ID() ;/*if($pageid!=5681){*/?>
		<!-- Facebook Pixel Code -->
	<script>
	    !function (f, b, e, v, n, t, s)
	    {
		if (f.fbq)
		    return;
		n = f.fbq = function () {
		    n.callMethod ?
			    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq)
		    f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
	    }(window, document, 'script',
		    'https://connect.facebook.net/en_US/fbevents.js');
	    fbq('init', '1762853627078000');
	    fbq('track', 'PageView');
	</script>
    <noscript><img height="1" width="1" style="display:none"
		   src="https://www.facebook.com/tr?id=1762853627078000&ev=PageView&noscript=1"
		   /></noscript>
 <!-- End Facebook Pixel Code -->
<?php /*}*/ ?>

<!--<script>
    (function (i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function () {
	    (i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o),
		m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-10599113-1', 'auto');
    ga('send', 'pageview');

</script>-->
<script async src="//tag.benchplatform.com/benchmarketingsmarttag/get?bb83497d54bf238ab76045b8752b73066f6d14311f5e297c3b9f49580207c334"></script>
		
		
</head>

<body <?php body_class(); ?>>

	<div id="page" class="site">
		<div id="fb-root"></div> 
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v11.0" nonce="9IMYP7oP"></script> 
		<?php
		/**
		 * Hook - ecommerce_gem_top_header.
		 *
		 * @hooked ecommerce_gem_top_header_action - 10
		 */
		do_action( 'ecommerce_gem_top_header' );

		/**
		* Hook - winsone_before_header.
		*
		* @hooked ecommerce_gem_before_header_action - 10
		*/
		do_action( 'ecommerce_gem_before_header' );

		/**
		* Hook - ecommerce_gem_header.
		*
		* @hooked ecommerce_gem_header_action - 10
		*/
		do_action( 'ecommerce_gem_header' );

		/**
		* Hook - ecommerce_gem_after_header.
		*
		* @hooked ecommerce_gem_after_header_action - 10
		*/
		do_action( 'ecommerce_gem_after_header' );

		/**
		* Hook - ecommerce_gem_main_content.
		*
		* @hooked ecommerce_gem_main_content_for_slider - 5
		* @hooked ecommerce_gem_main_content_for_breadcrumb - 7
		* @hooked ecommerce_gem_main_content_for_home_widgets - 9
		*/
		do_action( 'ecommerce_gem_main_content' );

		/**
		* Hook - ecommerce_gem_before_content.
		*
		* @hooked ecommerce_gem_before_content_action - 10
		*/
		do_action( 'ecommerce_gem_before_content' );