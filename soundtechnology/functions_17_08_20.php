<?php
/*This file is part of soundtechnology, ecommerce-gem child theme.

All functions of this file will be loaded before of parent theme functions.
Learn more at https://codex.wordpress.org/Child_Themes.

Note: this function loads the parent stylesheet before, then child theme stylesheet
(leave it in place unless you know what you are doing.)
*/

function soundtechnology_enqueue_child_styles() {
$parent_style = 'parent-style'; 
	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 
		'child-style', 
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get('Version') );
	}
add_action( 'wp_enqueue_scripts', 'soundtechnology_enqueue_child_styles' );

/*Write here your own functions */
function widget($atts) {
    
    global $wp_widget_factory;
    
    extract(shortcode_atts(array(
        'widget_name' => FALSE
    ), $atts));
    
    $widget_name = wp_specialchars($widget_name);
    
    if (!is_a($wp_widget_factory->widgets[$widget_name], 'WP_Widget')):
        $wp_class = 'WP_Widget_'.ucwords(strtolower($class));
        
        if (!is_a($wp_widget_factory->widgets[$wp_class], 'WP_Widget')):
            return '<p>'.sprintf(__("%s: Widget class not found. Make sure this widget exists and the class name is correct"),'<strong>'.$class.'</strong>').'</p>';
        else:
            $class = $wp_class;
        endif;
    endif;
    
    ob_start();
    the_widget($widget_name, $instance, array('widget_id'=>'arbitrary-instance-'.$id,
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
    
}
add_shortcode('widget','widget'); 

add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
return array(
'width' => 165,
'height' => 94,
'crop' => 0,
);
} );

/*cart button hide*/

/*add_filter( 'woocommerce_loop_add_to_cart_link', 'replacing_add_to_cart_button', 10, 2 );
function replacing_add_to_cart_button( $button, $product  ) {
    $button_text = __("View product", "woocommerce");
    $button = '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';

    return $button;
}

add_filter( 'woocommerce_is_purchasable', '__return_false');

add_filter( 'wc_product_sku_enabled', 'bbloomer_remove_product_page_sku' );
 
function bbloomer_remove_product_page_sku( $enabled ) {
    if ( !is_admin() && is_product() ) {
        return false;
    }
 
    return $enabled;
}*/

/*cart button hide*/


/*hide price from products start*/

/*remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
add_filter( 'woocommerce_variable_sale_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_variable_price_html', 'businessbloomer_remove_prices', 10, 2 );
add_filter( 'woocommerce_get_price_html', 'businessbloomer_remove_prices', 10, 2 );
 
function businessbloomer_remove_prices( $price, $product ) {
if ( ! is_admin() ) $price = '';
return $price;
}*/

/*hide price from products end*/

add_action( 'after_setup_theme', 'my_after_setup_theme' );
function my_after_setup_theme() {
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
}

add_role( 'subscriber','subscriber');
    
	
	if(current_user_can('wpsl_store_locator_manager')) {
	remove_role( 'editor');
	remove_role( 'author');
    
	}
	
	function my_login_redirect( $url, $request, $user ){
if( $user && is_object( $user ) && is_a( $user, 'WP_User' ) ) {
if( $user->has_cap( 'administrator') or $user->has_cap( 'author')) {
$url = admin_url();
} else {
$url = home_url('/sound_login/');
}
}
return $url;
}add_filter('login_redirect', 'my_login_redirect', 10, 3 );

add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
wp_redirect( home_url() );
exit();
}

// Changing "Default Sorting" to "Recommended sorting" on shop and product settings pages
function sip_update_sorting_name( $catalog_orderby ) {
$catalog_orderby = str_replace("Default sorting", "Recommended sorting", $catalog_orderby);
return $catalog_orderby;
}
add_filter( 'woocommerce_catalog_orderby', 'sip_update_sorting_name' );
add_filter( 'woocommerce_default_catalog_orderby_options', 'sip_update_sorting_name' );


/*search limit*/

function searchfilter($query) {

    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('product'));
        $query->set('posts_per_page',4);
    }

return $query;
}

add_filter('pre_get_posts','searchfilter');

/**
 * Change number or products per row to 3
 */
add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
	function loop_columns() {
		return 5; // 3 products per row
	}
}


/*allow mime type usdz*/

function mos_filter_fix_wp_check_filetype_and_ext( $data, $file, $filename, $mimes ) {
	if ( ! empty( $data['ext'] ) && ! empty( $data['type'] ) ) {
		return $data;
	}
	$registered_file_types = ['usdz' => 'model/vnd.usdz+zip|application/octet-stream|model/x-vnd.usdz+zip'];
	$filetype = wp_check_filetype( $filename, $mimes );
	if ( ! isset( $registered_file_types[ $filetype['ext'] ] ) ) {
		return $data;
	}
	return [
		'ext' => $filetype['ext'],
		'type' => $filetype['type'],
		'proper_filename' => $data['proper_filename'],
	];
}

function mos_allow_usdz( $mime_types ) {
	if ( ! in_array( 'usdz', $mime_types ) ) { 
		$mime_types['usdz'] = 'model/vnd.usdz+zip|application/octet-stream|model/x-vnd.usdz+zip';
	}
	return $mime_types;
}
add_filter( 'wp_check_filetype_and_ext', 'mos_filter_fix_wp_check_filetype_and_ext', 10, 4 );
add_filter( 'upload_mimes', 'mos_allow_usdz' );


/*allow mime type:webp*/



add_filter( 'wp_check_filetype_and_ext', 'wpse_file_and_ext_webp', 10, 4 );
function wpse_file_and_ext_webp( $types, $file, $filename, $mimes ) {
    if ( false !== strpos( $filename, '.webp' ) ) {
        $types['ext'] = 'webp';
        $types['type'] = 'image/webp';
    }

    return $types;
}

/**
 * Adds webp filetype to allowed mimes
 * 
 * @see https://codex.wordpress.org/Plugin_API/Filter_Reference/upload_mimes
 * 
 * @param array $mimes Mime types keyed by the file extension regex corresponding to
 *                     those types. 'swf' and 'exe' removed from full list. 'htm|html' also
 *                     removed depending on '$user' capabilities.
 *
 * @return array
 */
add_filter( 'upload_mimes', 'wpse_mime_types_webp' );
function wpse_mime_types_webp( $mimes ) {
    $mimes['webp'] = 'image/webp';

  return $mimes;
}

/*show SKU on product single page*/

/*add_action( 'woocommerce_single_product_summary', 'dev_designs_show_sku', 5 );
function dev_designs_show_sku(){
    global $product;
    echo 'SKU: ' . $product->get_sku();
}*/
/*show SKU on product single page*/


/*online store integration code*/


/**
 * Force WooCommerce terms and conditions link to open in a new page when clicked on the checkout page
 */
function shriro_woocommerce_checkout_terms_and_conditions() {
  remove_action( 'woocommerce_checkout_terms_and_conditions', 'wc_terms_and_conditions_page_content', 30 );
}
add_action( 'wp', 'shriro_woocommerce_checkout_terms_and_conditions' );


/**
 * Disable WooCommerce Ajax Cart Fragments Everywhere
 */ 
add_action( 'wp_enqueue_scripts', 'shriro_disable_woocommerce_cart_fragments', 11 );  
function shriro_disable_woocommerce_cart_fragments() { 
	 wp_dequeue_script('wc-cart-fragments'); 
}
 
 

/* Change PayPal Icon Start*/

function my_new_paypal_icon() {
    return '/wp-content/uploads/2020/08/paypal.png';
}

add_filter( 'woocommerce_paypal_icon', 'my_new_paypal_icon' );
 
 /* Change PayPal Icon End */
 
 
 /*DEFAULT SHIP TO DIFFENRENT LOCATION IS UNCHECKED*/
 add_filter( 'woocommerce_ship_to_different_address_checked', '__return_false' ); 
 /*DEFAULT SHIP TO DIFFENRENT LOCATION IS UNCHECKED*/
 
 
 /*display product variation in next line*/
 add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_false' );
add_filter( 'woocommerce_is_attribute_in_product_name', '__return_false' );
/*display product variation in next line end*/
 
 /**
 * Exclude products from a particular category on the shop page
 */
function custom_pre_get_posts_query($q) {
	$tax_query = (array) $q->get('tax_query');
	$tax_query[] = array(
		'taxonomy' => 'product_cat',
		'field' => 'slug',
		'terms' => array('discontinued'), // Don't display products in the BBQs category on the shop page.
		'operator' => 'NOT IN',
	);
	$q->set('tax_query', $tax_query);
}


/*switch billing & shipping address on checkout*/


/*function wc_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Billing details' :
            $translated_text = __( 'Shipping details', 'woocommerce' );
            break;
    }
    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );*/

/*switch billing & shipping address on checkout end*/
 

 /*copied code from ehb for ecommerce */
 
 function action_woocommerce_new_order($order_id) {
	global $wpdb;
	if (!$order_id) {
		return;
	}
	$order = wc_get_order($order_id);
	if ($order->get_shipping_state() == 'NT' || $order->get_shipping_state() == 'NSW' || $order->get_shipping_state() == 'ACT') {
		$warehouse_code = '1NW';
	} else if ($order->get_shipping_state() == 'VIC' || $order->get_shipping_state() == 'SA' || $order->get_shipping_state() == 'TAS') {
		$warehouse_code = '1NW';
	} else if ($order->get_shipping_state() == 'WA') {
		$warehouse_code = '1WA';
	} else if ($order->get_shipping_state() == 'QLD') {
		$warehouse_code = '1NW';
	}

	$is_couponcode_exists = false;
	if ($order->get_coupon_codes()) {
		$is_couponcode_exists = true;
		$customer_code = 'CASIOEMI';
		foreach ($order->get_used_coupons() as $coupon_code) {
			$coupon_post_obj = get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
			$coupon_id = $coupon_post_obj->ID;
			// Get an instance of WC_Coupon object in an array(necessary to use WC_Coupon methods)
			$coupon = new WC_Coupon($coupon_id);
			/*echo "<pre>";
			$discount_type = 'fixed_cart'; // Type: fixed_cart, percent, fixed_product, percent_product
			print_r($coupon->get_discount_type());
			echo "</pre>";
			exit;*/
			$discount_type = $coupon->get_discount_type();
			$coupon_amount = $coupon->get_amount();
			//$coupon_code = $coupon->get_code();
			$coupon_code = '';
			 ## Filtering with your coupon custom types
	    	if($coupon->is_type( 'fixed_product' ) && $discount_type == 'fixed_product'){
		    	$product_ids = $coupon->get_product_ids();
	        	$coupon_total_amount = $coupon->get_amount();
	        	if(count($product_ids) > 0){
	        		foreach($product_ids as $pid){
	        			$coupon_list['coup_fixed_product'][$pid] = $coupon_total_amount;
	        		}
	        	}
	   	 	}
	   	 	else if($coupon->is_type( 'fixed_cart' ) && $discount_type == 'fixed_cart'){
		    	$product_ids = $coupon->get_product_ids();
	        	$coupon_total_amount = $coupon->get_amount();
	        	if(count($product_ids) > 0){
	        		foreach($product_ids as $pid){
	        			$coupon_list['coup_fixed_cart'][$pid] = $coupon_total_amount;
	        		}
	        	}
	   	 	}
	   	 	 else if($coupon->is_type( 'percent' ) && $discount_type == 'percent'){
		    	$product_ids = $coupon->get_product_ids();
	        	$coupon_total_amount = $coupon->get_amount();
	        	if(count($product_ids) > 0){
	        	foreach($product_ids as $pid){
	        			$coupon_list['coup_percent_product'][$pid] = $coupon_total_amount;
	        		}
	        	}
	   	 	}

		}
	} else {
		$customer_code = 'CASIOEMI';
		$coupon_amount = 0;
	}

	//print_r($coupon_list['coup_percent_product']);exit;

	$customer_note = $order->get_customer_note();
	$customer_note1 = substr($customer_note, 0, 30);
	$customer_note2 = substr($customer_note, 30, 60);
	$customer_note3 = substr($customer_note, 60, 90);

	$csv = 'Customer Code,Warehouse,Customer Reference,Sales Rep Code,Not Before Date,Delivery Name,Delivery Address 1,Delivery Address 2,Delivery Suburb,Delivery State,Delivery Postcode,Delivery instructions 1,Delivery instructions 2,Delivery instructions 3,Terms Request,Additional Email Address,Web Reference,Credit Card No,Expiry Date,Customer Name,Customer Email,Billing Address 1,Billing Address 2,Billing Suburb,Billing State,Billing Postcode,Customer Phone,Flag';
	$csv .= "\n";
	$csv .= $customer_code . ",";
	$csv .= $warehouse_code . ",";
	$csv .= $order->get_order_number() . ",";
	$csv .= ",";
	$csv .= date("d-m-Y", strtotime($order->get_date_created())) . ",";
	$csv .= $order->get_shipping_first_name() . " " . $order->get_shipping_last_name() . ",";
	$csv .= $order->get_shipping_address_1() . ",";
	$csv .= $order->get_shipping_address_2() . ",";
	$csv .= $order->get_shipping_city() . ",";
	$csv .= $order->get_shipping_state() . ",";
	$csv .= $order->get_shipping_postcode() . ",";
	$csv .= $customer_note1 . ",";
	$csv .= $customer_note2 . ",";
	$csv .= $customer_note3 . ",";
	$csv .= ",";
	$csv .= ",";
	$csv .= $order->get_order_number() . ",";
	$csv .= ",";
	$csv .= ",";
	$csv .= $order->get_billing_first_name() . " " . $order->get_billing_last_name() . ",";
	$csv .= $order->get_billing_email() . ",";
	$csv .= $order->get_billing_address_1() . ",";
	$csv .= $order->get_billing_address_2() . ",";
	$csv .= $order->get_billing_city() . ",";
	$csv .= $order->get_billing_state() . ",";
	$csv .= $order->get_billing_postcode() . ",";
	$csv .= $order->get_billing_phone() . ",";
	$csv .= "D,";
	$csv .= "\n";
	$csv .= "Item Code,Order Quantity,Item Price(Zero for list),Discount Percentage,Note";
	$csv .= "\n";

	foreach ($order->get_items() as $item_id => $item_data) {
		$product = $item_data->get_product();
		$product_id = $item_data->get_product_id();
		$product_name = $product->get_sku(); // Get the product name
		$item_quantity = $item_data->get_quantity(); // Get the item quantity
		/*$item_total = $product->get_price();*/ // Get the item line total
		/*Doddy has to sync this table from Pronto*/
		/*Fetch price from pronto table wpms_quote_margin_cost if coupon code exists*/
		$item_total = 0;
		if($is_couponcode_exists){
			//$coupon_amount = 0;
  			$table_name = $wpdb->prefix."quote_margin_cost";
  			/*echo "SELECT * FROM $table_name WHERE `stock-code`='".$product_name."' 
  			AND `whse-code`='".$warehouse_code."'";exit;*/
  			$whse_res = $wpdb->get_results( "SELECT * FROM $table_name WHERE `stock-code`='".$product_name."' 
  			AND `whse-code`='".$warehouse_code."'" );
  			if($whse_res){
	  			foreach($whse_res as $row_whse){
	  				$item_total =	$row_whse->{'whse-avg-cost'};	
	  			}
  			}
  			/*If not able to find that product on quote_margin table*/
  			else{
  				/*echo "Discount type is ".$discount_type;exit;*/
  				$item_total = $product->get_price(); 	
  				/*Discount amount for a fixed product*/
  				if($discount_type == 'fixed_product' && $item_total > 0){
  					if(count($coupon_list['coup_fixed_product']) > 0){
  						if($coupon_list['coup_fixed_product'][$product_id] >= 0){
  							$item_total = $item_total - $coupon_list['coup_fixed_product'][$product_id];
  							if($item_total < 0){
  								$item_total = 0;
  							}
  						}
  					}
				}
				else if($discount_type == 'fixed_cart' && $item_total > 0){
  					if(count($coupon_list['coup_fixed_cart']) > 0){
  						if($coupon_list['coup_fixed_cart'][$product_id] >= 0){
  							$item_total = $item_total - $coupon_list['coup_fixed_cart'][$product_id];
  							if($item_total < 0){
  								$item_total = 0;
  							}
  						}
  					}
				}
				/*Discount amount for a fixed product*/

				/*Discount percentage applied for each item*/
				else if($discount_type == 'percent' && $item_total > 0){
					//$coupon_percent = (float)$coupon->amount/100;
  					//$item_total = (float)$item_total * (1-$coupon_percent) ;
  					if(count($coupon_list['coup_percent_product']) > 0){
  						/*print_r($coupon_list['coup_percent_product']);*/
  						if($coupon_list['coup_percent_product'][$product_id] >= 0){
  							/*echo $coupon_list['coup_percent_product'][$product_id];*/
  							$coupon_percent = (float)$coupon_list['coup_percent_product'][$product_id]/100;
  							/*echo "<br/>Coupon Percentage is ".$coupon_percent;*/
  							$item_total = (float)$item_total * (1-$coupon_percent) ;
  							if($item_total < 0){
  								$item_total = 0;
  							}
  						}
  						/*echo "<br/>Item total is ".$item_total;exit;*/
  					}
  					else{
  						$coupon_percent = (float)$coupon->amount/100;
  						$item_total = (float)$item_total * (1-$coupon_percent) ;
  					}
				}

				if($item_total >= 0){
					//Calculation to find price exclude GST.Divide by 1.1
					$item_total = (float)$item_total/1.1;
					$item_total = number_format($item_total,2);
				}
  			}
  		}
  		else{
  			$item_total = $product->get_price(); 
  			if($item_total >= 0){
				//Calculation to find price exclude GST.Divide by 1.1
				$item_total = (float)$item_total/1.1;
				$item_total = number_format($item_total,2);
			}
  		}
  		//No need to add to discount percentage
  		$coupon_amount = 0;

		$csv .= $product_name . ",";
		$csv .= $item_quantity . ",";
		$csv .= $item_total . ",";
		$csv .= $coupon_amount . ",";
		$csv .= $coupon_code . ",";
		$csv .= "\n";
	}
	$csv .= "CHARGE,";
	$csv .= "1,";
	$csv .= $order->get_shipping_total() . ",";
	$csv .= ",";
	$csv .= ",";
	$csv .= "\n";
	/*$csv_handler = fopen("ftp://wordpress:shriro@1@52.63.91.214/orders/product_order_" . $order_id . ".csv", 'w');
	fwrite($csv_handler, $csv);
	fclose($csv_handler);*/

/*$local_file_path = get_stylesheet_directory()."/uploads/order_csv/product_order_" . $order_id . ".csv";*/
$wp_upload_dir =  wp_upload_dir();
$local_file_path= $wp_upload_dir['basedir']."/order_csv/product_order_" . $order_id . ".csv";
$csv_handler = fopen($local_file_path, 'w');
fwrite($csv_handler, $csv);
fclose($csv_handler);
/*AWS server login credentials-FTP server Shriro for Shopify*/
$ftp_server = "52.63.91.214";     
$ftp_user_name = "casiomusic_au";     
$ftp_user_pass = "casio123";     
// Connect to FTP Server
$conn_id = ftp_connect($ftp_server);
if($conn_id){
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	$local_filename = "product_order_" . $order_id . ".csv";
	$remote_file_path = 'orders/'.$local_filename;
	$remote_server_filelist = ftp_nlist($conn_id, 'orders/');
	/*Uploads to remote server if the same order file not exists on remote server*/
	if(!in_array($remote_file_path, $remote_server_filelist)){ 
			ftp_put($conn_id, $remote_file_path, $local_file_path, FTP_ASCII);
	}
	ftp_close($conn_id);
}
}

// add the action
add_action('woocommerce_order_status_processing', 'action_woocommerce_new_order', 10, 3);

add_filter('woocommerce_order_number', 'change_woocommerce_order_number');

function change_woocommerce_order_number($order_id) {
	$prefix = 'CASIOEMITest';
	$new_order_id = $prefix . $order_id;
	return $new_order_id;
}
 
 
 
 /*copy ehb stock avalibility module ,07/08/2020*/
 /*Function to get stock quantity from Pronto table sound_stock_availability*/
  function get_stock_quantity_pronto($stock_code ='',$state=''){
	global $wpdb;
	/*echo "SELECT `stock-quantity` as stock_quantity FROM `wpms_stock_availability` WHERE `stock-code` = '".$stock_code."' AND `state` = '".$state."'";*/
	$query = $wpdb->get_results("SELECT `stock-quantity` as stock_quantity FROM `sound_stock_availability` WHERE `stock-code` = '".$stock_code."' AND `state` = '".$state."'");
	/*Number of stock quantity based on state*/
	$stock_quantity = $query[0]->stock_quantity;
	$stock_quantity = 	$stock_quantity > 0 ? 	$stock_quantity : 0;
	return $stock_quantity;
}
 
 /*Function to get stock quantity based on state and stock code*/
function get_the_user_ip() {
	if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
	//check ip from share internet
	$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
	//to check ip is pass from proxy
	$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
	$ip = $_SERVER['REMOTE_ADDR'];
	}
	$ip = apply_filters( 'wpb_get_ip', $ip );
	//$ip ='210.84.52.95';
	return get_state_from_ip($ip);

}
/*Function to retrieve location details from pro.ip-api.com*/
function custom_curl_request($ip=''){
	$url = 'https://pro.ip-api.com/json/'.$ip.'?key=UIpjDIC82UvnoXT&fields=country,countryCode,region';
	$curl = curl_init();
	curl_setopt_array($curl, array(
	CURLOPT_RETURNTRANSFER => 1,
	CURLOPT_URL => $url,
	CURLOPT_USERAGENT => 'Simple cURL'
	));
	$response = curl_exec($curl);
	curl_close($curl);
	return $response;
}




function get_state_from_ip($ip=''){
	
	//$location = file_get_contents('https://pro.ip-api.com/json/'.$ip.'?key=UIpjDIC82UvnoXT&fields=country,countryCode,region');
	$location  = custom_curl_request($ip);
	$loc = json_decode($location,true);
	//echo $loc['country'] ;
	//print_r($loc);
	if($loc['country'] == 'Australia'){
	 $state = $loc['region'];
	 return $state;
	}
	else{
	  return;
	}
}
$ip_user_state = get_the_user_ip();






//print_r($ip_user_state);
/*Function to get stock quantity based on state and stock code*/
 
 
 /* Display products for Shop Page*/
if (!function_exists('woocommerce_template_loop_add_to_cart')) {
function woocommerce_template_loop_add_to_cart() {
 global $product;
 $ip_user_state = get_the_user_ip();
	$product_type = $product->get_type();
	if($product_type == 'external'){
	echo '<a href="'. home_url() .'/stockist" rel="nofollow" class="button">View Stockists</a>';
		return;
	}
	if($product_type == 'simple'){
		$stock_code = $product->get_sku();
		$stock_quantity_pronto = get_stock_quantity_pronto($stock_code,$ip_user_state);
		if($stock_quantity_pronto < MINIMUM_STOCK_QTY || empty($stock_quantity_pronto)){
			echo '<a href="'.get_permalink().'" rel="nofollow" class="button outstock_button">Out of Stock</a>';
		}
		else
		{
			woocommerce_get_template('loop/add-to-cart.php');
		}
	}
	else if($product_type == 'variable'){
		$variations = $product->get_available_variations();
		$stock_var = array();
		$is_variant_instock = false;
		if(count($variations) > 0){
			foreach($variations as $variation){
				$stock_code =	 $variation['sku']; 
				//$ip_user_state = 'VIC';
				$stock_quantity_pronto = get_stock_quantity_pronto($stock_code,$ip_user_state);
				if($stock_quantity_pronto >= MINIMUM_STOCK_QTY){
					/*echo "Stock Quantity is ".$stock_quantity_pronto;*/
					$is_variant_instock = true;
					break;	
				}
			}
			if(!$is_variant_instock){
				echo '<a href="'.get_permalink().'" rel="nofollow" class="button outstock_button">Select Option</a>';
			}
			else
			{
				woocommerce_get_template('loop/add-to-cart.php');
				
			}
		}
	}

}
}

add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
	global $product;    
	$product_type = $product->product_type;  
	switch ($product_type) {
	case 'simple':
	        return __( 'Add To Cart', 'woocommerce' );
	    break;
	case 'variable':
	        return __( 'Select Option', 'woocommerce' );
	    break;
	case 'external':
	        return __( 'Read More', 'woocommerce' );
	    break;
	}
}

function get_variant_stock_check(){
	global $ip_user_state;
	//echo $variation_id = $_POST['variation_id'];
	//$product = new WC_Product($variation_id);
	//echo $stock_code = $product->get_sku();
	$stock_code = $_POST['stock_code'];
    //$ip_user_state = 'SA';
	$stock_quantity_pronto = get_stock_quantity_pronto($stock_code,$ip_user_state);
	//echo "Stock Quantity is ".$stock_quantity_pronto;
	if(!empty($stock_quantity_pronto)){
		echo '1';
	}
	else{
		echo '0';
	}
	exit;
}
add_action( 'wp_ajax_variant_stock_check', 'get_variant_stock_check');
add_action( 'wp_ajax_nopriv_variant_stock_check', 'get_variant_stock_check');

/*Cron Jobs functions starts*/
/*Section to create custom shipment tracking*/
add_action( 'woocommerce_admin_order_data_after_order_details', 'custom_woocommerce_admin_order_data_after_order_details' );
function custom_woocommerce_admin_order_data_after_order_details( $order ){
?>
    <br class="clear" />
    <h4>Shipment Tracking</h4>
    <?php 
        /*
         * get all the meta data values we need
         */ 
      
    ?>
    <div class="edit_custom_field"> 
    <?php
        woocommerce_wp_textarea_input( array(
            'id' => 'tracking_company',
            'label' => 'Tracking Company:',
            'value' => $order->get_meta('tracking_company'),
            'wrapper_class' => 'form-field-wide'
        ) );

    ?>
    </div>
    <div class="edit_custom_field"> 
    <?php
        woocommerce_wp_textarea_input( array(
            'id' => 'tracking_link',
            'label' => 'Tracking Link:',
            'value' => $order->get_meta('tracking_link'),
            'wrapper_class' => 'form-field-wide'
        ) );

    ?>
    </div>
    <div class="edit_custom_field"> 
    <?php
        woocommerce_wp_textarea_input( array(
            'id' => 'tracking_number',
            'label' => 'Tracking Number:',
            'value' => $order->get_meta('tracking_number'),
            'wrapper_class' => 'form-field-wide'
        ) );

    ?>
    </div>
<?php
}

/**
 * Save the custom fields values
 */
add_action( 'woocommerce_process_shop_order_meta', 'custom_woocommerce_process_shop_order_meta' );
function custom_woocommerce_process_shop_order_meta( $order_id ){
    update_post_meta( $order_id, 'custom_field_name', wc_sanitize_textarea( $_POST[ 'custom_field_name' ] ) );
}
/*Section to create custom shipment tracking*/

/*Section to add Order status Dispatched*/
function custom_register_dispatched_status() {
 
	register_post_status( 'wc-custom-dispatched', array(
		'label'		=> 'Dispatched',
		'public'	=> true,
		'show_in_admin_status_list' => true, // show count All (12) , Completed (9) , Awaiting shipment (2) ...
		'label_count'	=> _n_noop( 'Dispatched <span class="count">(%s)</span>', 'Dispatched <span class="count">(%s)</span>' )
	) );
 
}
add_action( 'init', 'custom_register_dispatched_status' );
 
/*
 * Add registered status to list of WC Order statuses
 * @param array $wc_statuses_arr Array of all order statuses on the website
 */
function custom_add_status_dispatched( $wc_statuses_arr ) {
 	$new_statuses_arr = array();
 	// add new order status after processing
	foreach ( $wc_statuses_arr as $id => $label ) {
		$new_statuses_arr[ $id ] = $label;
 		if ( 'wc-completed' === $id ) { // after "Completed" status
			$new_statuses_arr['wc-custom-dispatched'] = 'Dispatched';
		}
	}
	return $new_statuses_arr;
}
add_filter( 'wc_order_statuses', 'custom_add_status_dispatched' );
function custom_register_delivered_status() {
 	register_post_status( 'wc-custom-delivered', array(
		'label'		=> 'Delivered',
		'public'	=> true,
		'show_in_admin_status_list' => true, // show count All (12) , Completed (9) , Awaiting shipment (2) ...
		'label_count'	=> _n_noop( 'Delivered <span class="count">(%s)</span>', 'Delivered <span class="count">(%s)</span>' )
	) );
 
}
/*Section to add Order status Dispatched*/
add_action( 'init', 'custom_register_delivered_status' ); 
/*
 * Add registered status to list of WC Order statuses
 * @param array $wc_statuses_arr Array of all order statuses on the website
 */
function custom_add_status_delivered( $wc_statuses_arr ) {
 	$new_statuses_arr = array();
 	// add new order status after processing
	foreach ( $wc_statuses_arr as $id => $label ) {
		$new_statuses_arr[ $id ] = $label;
 		if ( 'wc-completed' === $id ) { // after "Completed" status
			$new_statuses_arr['wc-custom-delivered'] = 'Delivered';
		}
	}
 	return $new_statuses_arr;
 }
add_filter( 'wc_order_statuses', 'custom_add_status_delivered' );
/*Section to add Order status Delivered*/

/*Cron Jobs section*/
function get_numerics($str) {
    preg_match_all('/\d+/', $str, $matches);
    return trim($matches[0][0]);
}
/*cron jobs section for fetching connote csv file from ftp folder location and stored to database*/
function cronjob_fetch_connote_csvfiles(){
/*$subject = 'Test Email';
$headers = array('Content-Type: text/html; charset=UTF-8');
$to = 'robinv@shriro.com.au';
$message = 'This is a test email';
wp_mail($to,$subject,$message,$headers);*/
global $wpdb;
$ftp_server = "52.63.91.214";     
$ftp_user_name = "casiomusic_au";     
$ftp_user_pass = "casio123";       
// Connect to FTP Server
$conn_id = ftp_connect($ftp_server);
if($conn_id){
	$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
	$list = ftp_nlist($conn_id, 'connote/');
		foreach ($list as $fkey => $fvalue) {
		//Initiate array
		$ordermast = $orderline = array();
		//file will be saved using this file name
		$wp_upload_dir   =  wp_upload_dir();
		$local_file_path = $wp_upload_dir['basedir'].'/connote/';
		$new_file_name   = $local_file_path.'file.csv';
		//download file to local disc
		ftp_get($conn_id, $new_file_name, $fvalue, FTP_ASCII);
		//After download the file delete it to avoid duplict
		//echo $fvalue;
		ftp_delete($conn_id,$fvalue);
		//starts reading file
		if (($handle = fopen($new_file_name, "r")) !== FALSE) {
			$line = 1;
			$orderlinenum = 0;
			$x = 1;
		    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
		    	if ($line == 2) {
		    		$ordermast['customer_ref'] = $data[0];
		    		$customer_ref = $data[0];
		    		$order_id = get_numerics($customer_ref);
		    		$order_id = intval($order_id);
		    		$ordermast['tracking_company'] = $data[1];
		    		$ordermast['tracking_link'] = $data[2];
		    		$ordermast['tracking_number'] = $data[3];
		    	}
		    	if ($line > 3) {
		    		$orderline[$orderlinenum]['ordermast_id'] = $order_id;
		    		$orderline[$orderlinenum]['itemcode'] = $data[0];
		    		$orderline[$orderlinenum]['qty'] = $data[1];
		    		$orderline[$orderlinenum]['line_id'] = $x;
		    		$x++;
		    		$orderlinenum++;
		    	}
		        $line++;
		    }
		}
		//print_r($ordermast);
		//exit;
		unlink($local_file_path.'file.csv');
		fclose($handle);
		if(count($ordermast) > 0 && count($orderline) > 0){
			$query = $wpdb->get_results("SELECT count(id) as cnt FROM fulfillment_call 
				WHERE ordermast_id ='".$order_id."' AND customer_ref='".$customer_ref."' AND synced=0");
    		$row_count = $query[0]->cnt;
    		//echo $wpdb->last_query;
    		if($row_count == 0){
    			if (strpos($ordermast['tracking_number'], 'E+')){
                	$ordermast['tracking_number'] = intval((float)$ordermast['tracking_number']);
       			}
				$insert_order_data = array('ordermast_id' => $order_id,
				'customer_ref' => $ordermast['customer_ref'],
				'tracking_company' => $ordermast['tracking_company'],
				'tracking_link' => $ordermast['tracking_link'],
				'tracking_number' => $ordermast['tracking_number'],
				'synced' => 0,
				'createdby' => date('Y-m-d H:i:s'));
				$wpdb->insert('fulfillment_call', $insert_order_data);
				foreach( $orderline as $k=>$orderline_items){
					$wpdb->insert('fulfillment_call_lines', $orderline_items);
				}
				unset($ordermast);
				unset($orderline);
			}/*Check row count > 0*/
		}
	} 
  }
}
add_action( 'wp_ajax_cron_fetch_connote', 'cronjob_fetch_connote_csvfiles');
add_action( 'wp_ajax_nopriv_cron_fetch_connote', 'cronjob_fetch_connote_csvfiles');


/*cron jobs section for connote csv file from ftp folder location and stored to database*/
function cronjob_update_order_shipment_tracking(){
  global $wpdb;
	$query = $wpdb->get_results("SELECT ordermast_id,tracking_company,tracking_link,tracking_number FROM fulfillment_call WHERE synced=0");
    if($query){
    	foreach($query as $k=>$v){
    		$order_details = wc_get_order($v->ordermast_id);
    		if($order_details){
    		$order = new WC_Order($v->ordermast_id);
    		if($order){
    			echo $url_link = str_replace("<CONNOTE>",$v->tracking_number,$v->tracking_link);
				$order->update_meta_data('tracking_company', $v->tracking_company);
				$order->update_meta_data('tracking_link', $url_link);
				$order->update_meta_data('tracking_number', $v->tracking_number);
				$order->save();
				echo "<br/>Status is ".$order->status;
				if ($order->status == 'processing') {
					/*$order->update_status('custom-dispatched', 'Order is dispatched'); */
					$order->update_status('custom-dispatched'); 
				}
				unset($order);
				$data = array('synced'=>1);
				$where_condition = array('ordermast_id'=>$v->ordermast_id,'synced'=>0);
				$wpdb->update('fulfillment_call',$data,$where_condition);
			}
			}
		}
   }
}
add_action( 'wp_ajax_cron_update_order_shipment_tracking', 'cronjob_update_order_shipment_tracking');
add_action( 'wp_ajax_nopriv_cron_update_order_shipment_tracking', 'cronjob_update_order_shipment_tracking');

add_action("woocommerce_order_status_changed", "dispatched_status_custom_notification");
function dispatched_status_custom_notification($order_id, $checkout=null) {
   global $woocommerce;
   $order = new WC_Order( $order_id );
   echo $order->status;
   /*Custom Dispatched Order Status*/
   if($order->status == 'custom-dispatched' ) {
	$tracking_company = $order->get_meta('tracking_company');
	$tracking_link =  $order->get_meta('tracking_link');
	$tracking_number =  $order->get_meta('tracking_number');
      // Create a mailer
     $mailer = $woocommerce->mailer();
     $email_content  = '';
     $email_content = '<p>Tracking company is '.$tracking_company.'</p>';
     $email_content .= '<p>Tracking number is '.$tracking_number.'</p>';
     $email_content .= '<p><a href="'.$tracking_link.'" target="_blank">Tracking URL</a></p>';
      $message_body = __($email_content);
      $message = $mailer->wrap_message(
        // Message head and message body.
        sprintf( __( 'Order %s is Dispatched' ), $order->get_order_number() ), $message_body );

      // Cliente email, email subject and message.
     $mailer->send( $order->billing_email, sprintf( __( 'Order %s Dispatched' ), $order->get_order_number() ), $message );
     }   /*Custom Dispatched Order Status*/
    /*Custom Delivered Order Status*/
    else if($order->status == 'custom-delivered' ) {
	$tracking_company = $order->get_meta('tracking_company');
	$tracking_link =  $order->get_meta('tracking_link');
	$tracking_number =  $order->get_meta('tracking_number');
      // Create a mailer
     $mailer = $woocommerce->mailer();
     $email_content  = '';
     $email_content = '<p>Tracking company is '.$tracking_company.'</p>';
     $email_content .= '<p>Tracking number is '.$tracking_number.'</p>';
     $email_content .= '<p><a href="'.$tracking_link.'" target="_blank">Tracking URL</a></p>';
      $message_body = __($email_content);
      $message = $mailer->wrap_message(
        // Message head and message body.
        sprintf( __( 'Order %s is Delivered' ), $order->get_order_number() ), $message_body );

      // Cliente email, email subject and message.
     $mailer->send( $order->billing_email, sprintf( __( 'Order %s Delivered' ), $order->get_order_number() ), $message );
     }
     /*Custom Delivered Order Status*/
   }


/*change join waitlist text to notify me end*/
/*Cron job to send emails to users who are registered for 
out of stock items*/
function cronjob_run_scheduled_tasks(){
global $wpdb;
$subject = 'A product you are waiting for is back in stock';
$headers = array('Content-Type: text/html; charset=UTF-8');
$table_name = 'outofstock_waitlist';
$query = $wpdb->get_results("SELECT count(id) as cnt FROM ".$table_name." WHERE email_status = 'Not Sent'");
    $row_count = $query[0]->cnt;
    if($row_count  > 0){
      $query = $wpdb->get_results("SELECT product_name,stock_code,email,state FROM ".$table_name." WHERE email_status = 'Not Sent'");
      foreach($query as $k=>$v){
      	$product_name = strtoupper($v->product_name);
      	$stock_code   = $v->stock_code;
      	$email        = $v->email;
      	$state        = $v->state;
      	$stock_quantity = get_stock_quantity_pronto($stock_code,$state);
      	if($stock_quantity >= MINIMUM_STOCK_QTY){
      		$product_url = home_url().'/product/'.$stock_code;
      		$backtostock_email_template = '
<table class="body-wrap">
<tbody>
<tr>
<td></td>
<td class="container" width="600">
<div class="content">
<table class="main" width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td style="background: #000; text-align: center;padding:5px;">
<h1>'.$product_name.' is now back in stock at Everdure by Heston Blumenthal</h1><br/><br/>
</td>
</tr>
<tr>
<td class="content-wrap">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr>
<td class="content-block" style="padding-top:20px;">Hi There,<br/></td>
</tr>
<tr>
<td class="content-block">
<br/>
'.$product_name.' is now back in stock at Everdure by Heston Blumenthal. You have been sent this email because your email address was registered on a waitlist for this product.
If you would like to purchase '.$product_name.' please visit the following link: '.$product_url.'
<br/>
</td>
</tr>
<tr>
<td class="content-block"><br/>— Everdure by Heston Team</td>
</tr>
</tbody>
</table>';
//echo $backtostock_email_template;
$to = $email;
wp_mail($to,$subject,$backtostock_email_template,$headers);
$where_condition = array('stock_code'=>$stock_code,'state'=>$state,'email'=>$email);
$sent_date = wp_date('Y-m-d H:i:s');
$data = array('email_status'=>'Sent','sent_date'=>$sent_date);
$wpdb->update($table_name,$data,$where_condition);
      	}
      }
    }
   exit;
}
/*Enable this functions if you want back to stock emails*/
/*add_action( 'wp_ajax_custom_cron_job', 'cronjob_run_scheduled_tasks');
add_action( 'wp_ajax_nopriv_custom_cron_job', 'cronjob_run_scheduled_tasks');*/
/*Cron job to send emails to users who are registered for 
out of stock items*/

/*Cron Jobs functions ends*/


/*
Print load time*/

 /*echo get_num_queries();  
 echo 'queries in';
echo timer_stop(1); 
echo 'seconds.';*/







/*
Print load time*/