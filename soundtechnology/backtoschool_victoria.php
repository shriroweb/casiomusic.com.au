<?php
 /* Template Name: backtoschool_victoria */ 

//get_header(); 


?>

<!DOCTYPE html> 
<link rel='stylesheet' id='parent-style-css'  href='https://casiomusic.com.au/wp-content/themes/ecommerce-gem/style.css?ver=5.2.9' type='text/css' media='all' />
<link rel='stylesheet' id='child-style-css'  href='https://casiomusic.com.au/wp-content/themes/soundtechnology/style.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='dnd-upload-cf7-css'  href='https://casiomusic.com.au/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/assets/css/dnd-upload-cf7.css?ver=1.3.5.8' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='https://casiomusic.com.au/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-animations-css'  href='https://casiomusic.com.au/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=2.8.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-frontend-css'  href='https://casiomusic.com.au/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.8.4' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-5-all-css'  href='https://casiomusic.com.au/wp-content/plugins/elementor/assets/lib/font-awesome/css/all.min.css?ver=2.8.4' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-4-shim-css'  href='https://casiomusic.com.au/wp-content/plugins/elementor/assets/lib/font-awesome/css/v4-shims.min.css?ver=2.8.4' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-global-css'  href='https://casiomusic.com.au/wp-content/uploads/elementor/css/global.css?ver=1583245164' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-5681-css'  href='https://casiomusic.com.au/wp-content/uploads/elementor/css/post-5681.css?ver=1610425679' type='text/css' media='all' />
<!--<link rel='stylesheet' id='elementor-post-5681-css'  href='https://casiomusic.com.au/wp-content/plugins/wp-content-copy-protector/css/style.css?ver=1.222' type='text/css' media='all' />
<link rel='stylesheet' id='elementor-post-5681-css'  href='https://casiomusic.com.au/wp-content/plugins/wp-content-copy-protector/start-page-assests/css?ver=16179797' type='text/css' media='all' />-->
<style type="text/css" id="wp-custom-css">
			.wpcf7 input.wpcf7-submit {
    width: auto;
    border: none;
}
a:visited {
    color: #333;
}

button, html input[type="button"], input[type="reset"], input[type="submit"] {
    -webkit-appearance: button;
    cursor: pointer;
    border: none;
}

.m30tb{
    margin-top: 30px;
    margin-bottom: 30px;
}

input[type="submit"]
{    background: #c7b198;
}
#primary {
    width: 100%;
    padding-left: 15px;
    padding-right: 15px;
    float: left;
}




#wpcp-error-message {
direction: ltr;
text-align: center;
transition: opacity 900ms ease 0s;
z-index: 99999999;
}
.hideme {
opacity:0;
visibility: hidden;
}
.showme {
opacity:1;
visibility: visible;
}
.msgmsg-box-wpcp {
border:1px solid #f5aca6;
border-radius: 10px;
color: #555;
font-family: Tahoma;
font-size: 11px;
margin: 10px;
padding: 10px 36px;
position: fixed;
width: 255px;
top: 50%;
left: 50%;
margin-top: -10px;
margin-left: -130px;
-webkit-box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
-moz-box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
box-shadow: 0px 0px 34px 2px rgba(242,191,191,1);
}
.msgmsg-box-wpcp span {
font-weight:bold;
text-transform:uppercase;
}
.warning-wpcp {
background:#ffecec url('https://casiomusic.com.au/wp-content/plugins/wp-content-copy-protector/images/warning.png') no-repeat 10px 50%;
}


		</style>
	<script type='text/javascript' src='https://casiomusic.com.au/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://casiomusic.com.au/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<div id="content" class="site-content">
  <div class="container">
    <div class="inner-wrapper">
	<div id="primary" class="content-area m30tb">
		<main id="main" class="site-main" role="main">
		<div class="elementor-widget-container">
					<div class="elementor-image" style="margin-bottom: 20px;">
										<img title="" alt="" data-src="https://casiomusic.com.au/wp-content/uploads/2021/01/CASIO0030-BACK_TO_SCHOOL_CASHBACK_WEB_BANNER_V2A2-.jpg" class=" lazyloaded" src="https://casiomusic.com.au/wp-content/uploads/2021/01/CASIO0030-BACK_TO_SCHOOL_CASHBACK_WEB_BANNER_V2A2-.jpg"><noscript><img src="https://casiomusic.com.au/wp-content/uploads/2021/01/CASIO0030-BACK_TO_SCHOOL_CASHBACK_WEB_BANNER_V2A2-.jpg" title="" alt="" /></noscript>											</div>
				</div>
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-large">Back to School Casio Cashback, 2021</h2>		</div>

			<?php
			while ( have_posts() ) : the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	</div><!-- inner wrapper -->
	</div><!-- container -->
	
	</div><!-- #content -->
	<div id="wpcp-error-message" class="msgmsg-box-wpcp hideme"><span>error: </span>Content is protected !!</div>
	<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/casiomusic.com.au\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='https://casiomusic.com.au/wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6'></script>
<script type='text/javascript' src='https://casiomusic.com.au/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/assets/js/codedropz-uploader-min.js?ver=1.3.5.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var dnd_cf7_uploader = {"ajax_url":"https:\/\/casiomusic.com.au\/wp-admin\/admin-ajax.php","ajax_nonce":"ed76b09e41","drag_n_drop_upload":{"tag":"h3","text":"Drag & Drop Files Here","or_separator":"or","browse":"Browse Files","server_max_error":"The uploaded file exceeds the maximum upload size of your server.","large_file":"Uploaded file is too large","inavalid_type":"Uploaded file is not allowed for file type","max_file_limit":"Note : Some of the files are not uploaded ( Only %count% files allowed )","required":"This field is required.","delete":{"text":"deleting","title":"Remove"}},"dnd_text_counter":"of","disable_btn":""};
/* ]]> */
</script>
<script type='text/javascript' src='https://casiomusic.com.au/wp-content/plugins/drag-and-drop-multiple-file-upload-contact-form-7/assets/js/dnd-upload-cf7.js?ver=1.3.5.8'></script>
	<script type='text/javascript' src='https://casiomusic.com.au/wp-content/plugins/date-time-picker-field/assets/js/vendor/moment/moment.js?ver=5.2.9'></script>
<script type='text/javascript' src='https://casiomusic.com.au/wp-content/plugins/date-time-picker-field/assets/js/vendor/datetimepicker/jquery.datetimepicker.full.min.js?ver=5.2.9'></script>
	<script type="text/javascript">
window.addEventListener("load", function(event) {
jQuery(".cfx_form_main,.wpcf7-form,.wpforms-form,.gform_wrapper form").each(function(){
var form=jQuery(this); 
var screen_width=""; var screen_height="";
 if(screen_width == ""){
 if(screen){
   screen_width=screen.width;  
 }else{
     screen_width=jQuery(window).width();
 }    }  
  if(screen_height == ""){
 if(screen){
   screen_height=screen.height;  
 }else{
     screen_height=jQuery(window).height();
 }    }
form.append('<input type="hidden" name="vx_width" value="'+screen_width+'">');
form.append('<input type="hidden" name="vx_height" value="'+screen_height+'">');
form.append('<input type="hidden" name="vx_url" value="'+window.location.href+'">');  
}); 

});
</script>
<script type='text/javascript' src='https://www.google.com/recaptcha/api.js?render=6LdG2TMaAAAAAM8kTBWbA7gzViKk5AB7ANLmDX3A&#038;ver=3.0'></script>
	<script type="text/javascript">
( function( grecaptcha, sitekey, actions ) {

	var wpcf7recaptcha = {

		execute: function( action ) {
			grecaptcha.execute(
				sitekey,
				{ action: action }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		},

		executeOnHomepage: function() {
			wpcf7recaptcha.execute( actions[ 'homepage' ] );
		},

		executeOnContactform: function() {
			wpcf7recaptcha.execute( actions[ 'contactform' ] );
		},

	};

	grecaptcha.ready(
		wpcf7recaptcha.executeOnHomepage
	);

	document.addEventListener( 'change',
		wpcf7recaptcha.executeOnContactform, false
	);

	document.addEventListener( 'wpcf7submit',
		wpcf7recaptcha.executeOnHomepage, false
	);

} )(
	grecaptcha,
	'6LdG2TMaAAAAAM8kTBWbA7gzViKk5AB7ANLmDX3A',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
<!--<script type='text/javascript' src='https://casiomusic.com.au/wp-content/plugins/wp-content-copy-protector/js/simpletabs_1.3.js'></script>-->
<script>
var timeout_result;
function show_wpcp_message(smessage)
{
if (smessage !== "")
{
var smessage_text = '<span>Alert: </span>'+smessage;
document.getElementById("wpcp-error-message").innerHTML = smessage_text;
document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp showme";
clearTimeout(timeout_result);
timeout_result = setTimeout(hide_message, 3000);
}
}
function hide_message()
{
document.getElementById("wpcp-error-message").className = "msgmsg-box-wpcp warning-wpcp hideme";
}
</script>
<style>
@media print {
body * {display: none !important;}
body:after {
content: "You are not allowed to print preview this page, Thank you"; }
}
</style>

<script id="wpcp_disable_selection" type="text/javascript">
var image_save_msg='You are not allowed to save images!';
var no_menu_msg='Context Menu disabled!';
var smessage = "Content is protected !!";
function disableEnterKey(e)
{
var elemtype = e.target.tagName;
elemtype = elemtype.toUpperCase();
if (elemtype == "TEXT" || elemtype == "TEXTAREA" || elemtype == "INPUT" || elemtype == "PASSWORD" || elemtype == "SELECT" || elemtype == "OPTION" || elemtype == "EMBED")
{
elemtype = 'TEXT';
}
if (e.ctrlKey){
var key;
if(window.event)
key = window.event.keyCode;     //IE
else
key = e.which;     //firefox (97)
//if (key != 17) alert(key);
if (elemtype!= 'TEXT' && (key == 97 || key == 65 || key == 67 || key == 99 || key == 88 || key == 120 || key == 26 || key == 85  || key == 86 || key == 83 || key == 43 || key == 73))
{
if(wccp_free_iscontenteditable(e)) return true;
show_wpcp_message('You are not allowed to copy content or view source');
return false;
}else
return true;
}
}
/*For contenteditable tags*/
function wccp_free_iscontenteditable(e)
{
var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
var target = e.target || e.srcElement;
var elemtype = e.target.nodeName;
elemtype = elemtype.toUpperCase();
var iscontenteditable = "false";
if(typeof target.getAttribute!="undefined" ) iscontenteditable = target.getAttribute("contenteditable"); // Return true or false as string
var iscontenteditable2 = false;
if(typeof target.isContentEditable!="undefined" ) iscontenteditable2 = target.isContentEditable; // Return true or false as boolean
if(target.parentElement.isContentEditable) iscontenteditable2 = true;
if (iscontenteditable == "true" || iscontenteditable2 == true)
{
if(typeof target.style!="undefined" ) target.style.cursor = "text";
return true;
}
}
////////////////////////////////////
function disable_copy(e)
{	
var e = e || window.event; // also there is no e.target property in IE. instead IE uses window.event.srcElement
var elemtype = e.target.tagName;
elemtype = elemtype.toUpperCase();
if (elemtype == "TEXT" || elemtype == "TEXTAREA" || elemtype == "INPUT" || elemtype == "PASSWORD" || elemtype == "SELECT" || elemtype == "OPTION" || elemtype == "EMBED")
{
elemtype = 'TEXT';
}
if(wccp_free_iscontenteditable(e)) return true;
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
var checker_IMG = '';
if (elemtype == "IMG" && checker_IMG == 'checked' && e.detail >= 2) {show_wpcp_message(alertMsg_IMG);return false;}
if (elemtype != "TEXT")
{
if (smessage !== "" && e.detail == 2)
show_wpcp_message(smessage);
if (isSafari)
return true;
else
return false;
}	
}
//////////////////////////////////////////
function disable_copy_ie()
{
var e = e || window.event;
var elemtype = window.event.srcElement.nodeName;
elemtype = elemtype.toUpperCase();
if(wccp_free_iscontenteditable(e)) return true;
if (elemtype == "IMG") {show_wpcp_message(alertMsg_IMG);return false;}
if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "OPTION" && elemtype != "EMBED")
{
return false;
}
}	
function reEnable()
{
return true;
}
document.onkeydown = disableEnterKey;
document.onselectstart = disable_copy_ie;
if(navigator.userAgent.indexOf('MSIE')==-1)
{
document.onmousedown = disable_copy;
document.onclick = reEnable;
}
function disableSelection(target)
{
//For IE This code will work
if (typeof target.onselectstart!="undefined")
target.onselectstart = disable_copy_ie;
//For Firefox This code will work
else if (typeof target.style.MozUserSelect!="undefined")
{target.style.MozUserSelect="none";}
//All other  (ie: Opera) This code will work
else
target.onmousedown=function(){return false}
target.style.cursor = "default";
}
//Calling the JS function directly just after body load
window.onload = function(){disableSelection(document.body);};
//////////////////special for safari Start////////////////
var onlongtouch;
var timer;
var touchduration = 1000; //length of time we want the user to touch before we do something
var elemtype = "";
function touchstart(e) {
var e = e || window.event;
// also there is no e.target property in IE.
// instead IE uses window.event.srcElement
var target = e.target || e.srcElement;
elemtype = window.event.srcElement.nodeName;
elemtype = elemtype.toUpperCase();
if(!wccp_pro_is_passive()) e.preventDefault();
if (!timer) {
timer = setTimeout(onlongtouch, touchduration);
}
}
function touchend() {
//stops short touches from firing the event
if (timer) {
clearTimeout(timer);
timer = null;
}
onlongtouch();
}
onlongtouch = function(e) { //this will clear the current selection if anything selected
if (elemtype != "TEXT" && elemtype != "TEXTAREA" && elemtype != "INPUT" && elemtype != "PASSWORD" && elemtype != "SELECT" && elemtype != "EMBED" && elemtype != "OPTION")	
{
if (window.getSelection) {
if (window.getSelection().empty) {  // Chrome
window.getSelection().empty();
} else if (window.getSelection().removeAllRanges) {  // Firefox
window.getSelection().removeAllRanges();
}
} else if (document.selection) {  // IE?
document.selection.empty();
}
return false;
}
};
document.addEventListener("DOMContentLoaded", function(event) { 
window.addEventListener("touchstart", touchstart, false);
window.addEventListener("touchend", touchend, false);
});
function wccp_pro_is_passive() {
var cold = false,
hike = function() {};
try {
const object1 = {};
var aid = Object.defineProperty(object1, 'passive', {
get() {cold = true}
});
window.addEventListener('test', hike, aid);
window.removeEventListener('test', hike, aid);
} catch (e) {}
return cold;
}
/*special for safari End*/
</script>
<script id="wpcp_disable_Right_Click" type="text/javascript">
document.ondragstart = function() { return false;}
function nocontext(e) {
return false;
}
document.oncontextmenu = nocontext;
</script>

<?php
//do_action( 'ecommerce_gem_action_sidebar' );

//get_footer();

