<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package eCommerce_Gem
 */

	/**
	 * Hook - ecommerce_gem_after_content.
	 *
	 * @hooked ecommerce_gem_after_content_action - 10
	 */
	do_action( 'ecommerce_gem_after_content' );

?>

	<?php get_template_part( 'template-parts/footer-widgets' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="site-footer-wrap">
		
				<?php 

				$copyright_text = ecommerce_gem_get_option( 'copyright_text' ); 

				if ( ! empty( $copyright_text ) ) : ?>

					<div class="copyright">

						<?php echo wp_kses_data( $copyright_text ); ?>

					</div><!-- .copyright -->

					<?php 

				endif; 

				//do_action( 'ecommerce_gem_credit' ); 

				?>
			</div>
		</div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<!-- Facebook Pixel Code -->
	<script>
	    !function (f, b, e, v, n, t, s)
	    {
		if (f.fbq)
		    return;
		n = f.fbq = function () {
		    n.callMethod ?
			    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
		};
		if (!f._fbq)
		    f._fbq = n;
		n.push = n;
		n.loaded = !0;
		n.version = '2.0';
		n.queue = [];
		t = b.createElement(e);
		t.async = !0;
		t.src = v;
		s = b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t, s)
	    }(window, document, 'script',
		    'https://connect.facebook.net/en_US/fbevents.js');
	    fbq('init', '1762853627078000');
	    fbq('track', 'PageView');
	</script>
    <noscript><img height="1" width="1" style="display:none"
		   src="https://www.facebook.com/tr?id=1762853627078000&ev=PageView&noscript=1"
		   /></noscript>
    <!-- End Facebook Pixel Code -->


<script>
    (function (i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function () {
	    (i[r].q = i[r].q || []).push(arguments)
	}, i[r].l = 1 * new Date();
	a = s.createElement(o),
		m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-10599113-1', 'auto');
    ga('send', 'pageview');

</script>
<script async src="//tag.benchplatform.com/benchmarketingsmarttag/get?bb83497d54bf238ab76045b8752b73066f6d14311f5e297c3b9f49580207c334"></script>

</body>
</html>
