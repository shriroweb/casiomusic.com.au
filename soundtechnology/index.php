<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 */

get_header(); ?>



	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<!-- Home Slider -->
<?php 
echo do_shortcode('[smartslider3 slider=2]');
?>
<!-- ./Home Slider -->

<!-- ./Home New Section -->
<?php dynamic_sidebar( 'custom-sidebar-88' ); ?>

<!-- ./Home New Section -->

<?php dynamic_sidebar( 'home-page-widget-area' ); ?>
				


		</main><!-- #main -->
	</div><!-- #primary -->

<?php


 // End if show home content.

get_footer();
