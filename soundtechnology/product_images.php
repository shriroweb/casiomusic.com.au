<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all products images
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package eCommerce_Gem
 
 * Template Name: Product Images

 */

get_header(); ?>
<style>
#line hr:not(.is-style-wide):not(.is-style-dots)::before {

    content: '';
    display: block;
    height: 1px;
    width: 100%;
    background: 

    #767676;
    margin-top: .5rem;
    margin-bottom: .5rem;

}
	
#product_images_dealer button
{
border: none;	
padding:0 2px;	
margin:2px;
}
#product_images_dealer .fa-download{
	
color:#c7b198;
	}
	.product_images_dealer {margin-top:30px; margin-bottom:30px;}
	#product_images_dealer{
	width: 17%;

float: left;

margin: 15px 15px;
	}
	#line hr{width:100%;}
	
.popupimg{max-width: 75%!important;
    margin: 0px auto;
    display: block;
    box-shadow: 0px 0px 40px #ccc;}
	
	:focus {
    outline: -webkit-focus-ring-color none 1px !important;
}
.modal-header button {
    border: none;
    padding: 4px 8px!important;
    margin: 2px;
    float: right!important;
}
	</style>
	<div id="primary" class="content-area product_images_dealer">
		<main id="main" class="site-main" role="main">
<h3>Product Images</h3>


		<?php 
				global $wpdb;
				$result = $wpdb->get_results( "SELECT * FROM sound_posts WHERE post_status = 'publish' AND post_type LIKE 'product%'" );
				foreach ( $result as $res ) 
{
	echo $res->post_title."<br>";
	$p_id=$res->ID;
	

$term_list = wp_get_post_terms($p_id,'product_cat',array('fields'=>'ids'));
$cat_id = (int)$term_list[0];
//echo get_term_link ($cat_id, 'product_cat');
// Get array of URL 'scheme', 'host', and 'path'.
$url_parse = wp_parse_url(get_term_link ($cat_id, 'product_cat'));
 
// Output URL's path.
//echo $url_parse['path'];

// product category

$str = $url_parse['path'];
//echo $str . "<br>";
$url_parse['path']=trim($str,"/");
$categoryname=trim(str_replace("product-category","",$url_parse['path']),"/");
//echo ucfirst(str_replace("-"," ",$categoryname));

//categories list



  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args );
 foreach ($all_categories as $cat) {
    if($cat->category_parent == 0) {
        $category_id = $cat->term_id;       
      //  echo '<br /><a href="'. get_term_link($cat->slug, 'product_cat') .'">'. $cat->name .'</a>';

        $args2 = array(
                'taxonomy'     => $taxonomy,
                'child_of'     => 0,
                'parent'       => $category_id,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
        );
        $sub_cats = get_categories( $args2 );
        if($sub_cats) {
            foreach($sub_cats as $sub_category) {
               // echo  $sub_category->name ;
            }   
        }
    }       
}



//categories list



// product category

	//--main image	
	
	$result1 = $wpdb->get_results( "SELECT meta_value FROM sound_postmeta WHERE meta_key ='_thumbnail_id' AND post_id =$p_id");
	foreach ( $result1 as $res1 ) {
	
		
		$mv=$res1->meta_value;
		$result2 = $wpdb->get_results( "SELECT * FROM sound_postmeta WHERE meta_key ='_wp_attached_file' AND post_id =$mv");
		foreach ( $result2 as $res2 ) {
		
		
	$upload_dir = wp_upload_dir(); // Array of key => value pairs
/*
    $upload_dir now contains something like the following (if successful)
    Array (
        [path] => C:\path\to\wordpress\wp-content\uploads\2010\05
        [url] => http://example.com/wp-content/uploads/2010/05
        [subdir] => /2010/05
        [basedir] => C:\path\to\wordpress\wp-content\uploads
        [baseurl] => http://example.com/wp-content/uploads
        [error] =>
    )
    // Descriptions
    [path] - base directory and sub directory or full path to upload directory.
    [url] - base url and sub directory or absolute URL to upload directory.
    [subdir] - sub directory if uploads use year/month folders option is on.
    [basedir] - path without subdir.
    [baseurl] - URL path without subdir.
    [error] - set to false.
*/
 

 
$upload_baseurl = ( $upload_dir['baseurl'] );

 
// Now echo the final result
 
 
 ?>
 <div class="woocommerce columns-4 col-sm-4 col-lg-4 col-xs-12"  id="product_images_dealer">
 <ul class="products columns-4">
 <li>
 <div class="product-thumb-wrap yith-enabled">
 
 <?php
  echo "<img src='$upload_baseurl/$res2->meta_value' class='attachment-woocommerce_thumbnail size-woocommerce_thumbnail'>";
  
  ?>
  <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $res2->meta_id; ?>">
<?php echo "<i class='fa fa-eye' aria-hidden='true'></i>";?>
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter<?php echo $res2->meta_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;position: absolute;z-index: 9999;top: 10%;right: 8%;">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
         <!--<h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <?php
 echo "<img src='$upload_baseurl/$res2->meta_value' class='attachment-woocommerce_thumbnail popupimg' >";
 ?>
      </div>
   
    </div>
  </div>
</div>
 
 
 
 
 <?php
 
 echo "<a href='$upload_baseurl/$res2->meta_value' download><i class='fa fa-download'></i>
</a>";
	?>
	
	</div>
	</li>
	</ul>
	</div>
	<?php
		}
		
		}
		
	//--end of main image	
	
	
	
	//--gallery images
	
	$result1 = $wpdb->get_results( "SELECT meta_value FROM sound_postmeta WHERE meta_key ='_product_image_gallery' AND post_id =$p_id");
	foreach ( $result1 as $res1 ) {
	
		
		$mv=$res1->meta_value;
		$result2 = $wpdb->get_results( "SELECT * FROM sound_postmeta WHERE meta_key ='_wp_attached_file' AND post_id IN($mv)");
		foreach ( $result2 as $res2 ) {
		
		
	$upload_dir = wp_upload_dir(); // Array of key => value pairs
/*
    $upload_dir now contains something like the following (if successful)
    Array (
        [path] => C:\path\to\wordpress\wp-content\uploads\2010\05
        [url] => http://example.com/wp-content/uploads/2010/05
        [subdir] => /2010/05
        [basedir] => C:\path\to\wordpress\wp-content\uploads
        [baseurl] => http://example.com/wp-content/uploads
        [error] =>
    )
    // Descriptions
    [path] - base directory and sub directory or full path to upload directory.
    [url] - base url and sub directory or absolute URL to upload directory.
    [subdir] - sub directory if uploads use year/month folders option is on.
    [basedir] - path without subdir.
    [baseurl] - URL path without subdir.
    [error] - set to false.
*/
 

 
$upload_baseurl = ( $upload_dir['baseurl'] );

 
// Now echo the final result
?>
<div class="woocommerce columns-4 col-sm-4 col-lg-4 col-xs-12"  id="product_images_dealer">
 <ul class="products columns-4">
 <li>
 <div class="product-thumb-wrap yith-enabled">
 <?php
 echo "<img src='$upload_baseurl/$res2->meta_value' class='attachment-woocommerce_thumbnail size-woocommerce_thumbnail'>";
 ?>
 
 <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter<?php echo $res2->meta_id; ?>">
<?php echo "<i class='fa fa-eye' aria-hidden='true'></i>";?>
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter<?php echo $res2->meta_id;?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="display: none;position: absolute;z-index: 9999;top: 10%;right: 8%;">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!--<h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>-->
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <?php
 echo "<img src='$upload_baseurl/$res2->meta_value' class='attachment-woocommerce_thumbnail popupimg' >";
 ?>
      </div>
   
    </div>
  </div>
</div>
 
 
 
 <?php
 echo "<a href='$upload_baseurl/$res2->meta_value' download><i class='fa fa-download'></i>
</a>";
	?>
	</div>
	</li>
	</ul>
	
	</div>
	
	<?php
		}
		
		}
		
	//--end of gallery images	
	
	echo "<div id='line'><div style='clear:both;'></div><hr></div>";
	
}

?>
	
	
	
	
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
do_action( 'ecommerce_gem_action_sidebar' );

get_footer();
